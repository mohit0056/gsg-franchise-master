package com.crm.akscrm.model;

public class GetAttendanceHistory {

    private String id;

    private String user_id;

    private String punchintime;

    private String punchinlocation;

    private String punchouttime;

    private String punchoutlocation;

    private String created_at;

    public void setId(String id){
        this.id = id;
    }
    public String getId(){
        return this.id;
    }
    public void setUser_id(String user_id){
        this.user_id = user_id;
    }
    public String getUser_id(){
        return this.user_id;
    }
    public void setPunchintime(String punchintime){
        this.punchintime = punchintime;
    }
    public String getPunchintime(){
        return this.punchintime;
    }
    public void setPunchinlocation(String punchinlocation){
        this.punchinlocation = punchinlocation;
    }
    public String getPunchinlocation(){
        return this.punchinlocation;
    }
    public void setPunchouttime(String punchouttime){
        this.punchouttime = punchouttime;
    }
    public String getPunchouttime(){
        return this.punchouttime;
    }
    public void setPunchoutlocation(String punchoutlocation){
        this.punchoutlocation = punchoutlocation;
    }
    public String getPunchoutlocation(){
        return this.punchoutlocation;
    }
    public void setCreated_at(String created_at){
        this.created_at = created_at;
    }
    public String getCreated_at(){
        return this.created_at;
    }
}