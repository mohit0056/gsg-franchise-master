package com.crm.akscrm.model;

public class GetTargetData {

    private String id;

    private String user_id;

    private String monthcompleted;

    private String monthremaining;

    private String monthtargetAmount;

    private String monthdate;

    private String mvonetarget;

    private String mvonecomplete;

    private String mvoneremaining;

    private String mvtwotarget;

    private String mvtwocomplete;

    private String mvtworemaining;

    private String mvthreetarget;

    private String mvthreecomplete;

    private String mvthreeremaining;

    private String mvfourtarget;

    private String mvfourcomplete;

    private String mvfourremaining;

    public void setId(String id){
        this.id = id;
    }
    public String getId(){
        return this.id;
    }
    public void setUser_id(String user_id){
        this.user_id = user_id;
    }
    public String getUser_id(){
        return this.user_id;
    }
    public void setMonthcompleted(String monthcompleted){
        this.monthcompleted = monthcompleted;
    }
    public String getMonthcompleted(){
        return this.monthcompleted;
    }
    public void setMonthremaining(String monthremaining){
        this.monthremaining = monthremaining;
    }
    public String getMonthremaining(){
        return this.monthremaining;
    }
    public void setMonthtargetAmount(String monthtargetAmount){
        this.monthtargetAmount = monthtargetAmount;
    }
    public String getMonthtargetAmount(){
        return this.monthtargetAmount;
    }
    public void setMonthdate(String monthdate){
        this.monthdate = monthdate;
    }
    public String getMonthdate(){
        return this.monthdate;
    }
    public void setMvonetarget(String mvonetarget){
        this.mvonetarget = mvonetarget;
    }
    public String getMvonetarget(){
        return this.mvonetarget;
    }
    public void setMvonecomplete(String mvonecomplete){
        this.mvonecomplete = mvonecomplete;
    }
    public String getMvonecomplete(){
        return this.mvonecomplete;
    }
    public void setMvoneremaining(String mvoneremaining){
        this.mvoneremaining = mvoneremaining;
    }
    public String getMvoneremaining(){
        return this.mvoneremaining;
    }
    public void setMvtwotarget(String mvtwotarget){
        this.mvtwotarget = mvtwotarget;
    }
    public String getMvtwotarget(){
        return this.mvtwotarget;
    }
    public void setMvtwocomplete(String mvtwocomplete){
        this.mvtwocomplete = mvtwocomplete;
    }
    public String getMvtwocomplete(){
        return this.mvtwocomplete;
    }
    public void setMvtworemaining(String mvtworemaining){
        this.mvtworemaining = mvtworemaining;
    }
    public String getMvtworemaining(){
        return this.mvtworemaining;
    }
    public void setMvthreetarget(String mvthreetarget){
        this.mvthreetarget = mvthreetarget;
    }
    public String getMvthreetarget(){
        return this.mvthreetarget;
    }
    public void setMvthreecomplete(String mvthreecomplete){
        this.mvthreecomplete = mvthreecomplete;
    }
    public String getMvthreecomplete(){
        return this.mvthreecomplete;
    }
    public void setMvthreeremaining(String mvthreeremaining){
        this.mvthreeremaining = mvthreeremaining;
    }
    public String getMvthreeremaining(){
        return this.mvthreeremaining;
    }
    public void setMvfourtarget(String mvfourtarget){
        this.mvfourtarget = mvfourtarget;
    }
    public String getMvfourtarget(){
        return this.mvfourtarget;
    }
    public void setMvfourcomplete(String mvfourcomplete){
        this.mvfourcomplete = mvfourcomplete;
    }
    public String getMvfourcomplete(){
        return this.mvfourcomplete;
    }
    public void setMvfourremaining(String mvfourremaining){
        this.mvfourremaining = mvfourremaining;
    }
    public String getMvfourremaining(){
        return this.mvfourremaining;
    }
}