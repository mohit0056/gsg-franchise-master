package com.crm.akscrm;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crm.akscrm.utill.ApiURL;
import com.crm.akscrm.utill.NetworkCall;
import com.crm.akscrm.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class leaves extends AppCompatActivity implements NetworkCall.MyNetworkCallBack{
    Progress progress;
     TextView totalLeave1,completedLeave,total,remain,complete_leave_txt,remaining_txt,total_sick,complete_sick,remaining_sick,total_unpaid,complete_unpaid,remaining_unpaid,total_paid,complete_paid,remain_paid;
String userid,earning_id,earningleaves,earningcompleted,earningremaining,str_total_sick,str_complete_sick,str_remaining_sick,str_total_unpaid,str_complete_unpaid,str_remaining_unpaid,str_total_paid,str_complete_paid,
        str_remain_paid;
    SharedPreferences mSharedPreference;
    NetworkCall networkCall;
    ProgressBar simpleProgressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaves);

        progress = new Progress(leaves.this);
        networkCall = new NetworkCall(leaves.this, leaves.this);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        userid = (mSharedPreference.getString("id", ""));

        simpleProgressBar = findViewById(R.id.simpleProgressBar);


        totalLeave1=findViewById(R.id.totalLeave1);
        remain=findViewById(R.id.remain);
        total=findViewById(R.id.total);
        complete_leave_txt=findViewById(R.id.complete_leave_txt);
        remaining_txt=findViewById(R.id.remaining_txt);
        total_sick=findViewById(R.id.total_sick);
        complete_sick=findViewById(R.id.complete_sick);
        completedLeave=findViewById(R.id.completedLeave);
        remaining_sick=findViewById(R.id.remaining_sick);
        total_unpaid=findViewById(R.id.total_unpaid);
        complete_unpaid=findViewById(R.id.complete_unpaid);
        remaining_unpaid=findViewById(R.id.remaining_unpaid);
        total_paid=findViewById(R.id.total_paid);
        complete_paid=findViewById(R.id.complete_paid);
        remain_paid=findViewById(R.id.remain_paid);

        getData();

    }

    private void getData() {

        networkCall.NetworkAPICall(ApiURL.getLeaveData, true);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void back(View view) {
        super.onBackPressed();

    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.getLeaveData:
                ion = (Builders.Any.B) Ion.with(leaves.this)
                        .load("POST", ApiURL.getLeaveData)
                        .setHeader("token", "zsd16xzv3jsytnp87tk7ygv73k8zmr0ekh6ly7mxaeyeh46oe8")
                        .setBodyParameter("user_id", userid)


                ;
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {

        switch (apitype) {

            case ApiURL.getLeaveData:

                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");

                    if (succes.equals("true")) {

                       // JSONArray Earning = jsonObject.getJSONArray("Earningleave");
                        JSONArray unpaid = jsonObject.getJSONArray("unpaid");
//                        JSONArray sick = jsonObject.getJSONArray("SickLeave");
//                        JSONArray Paid = jsonObject.getJSONArray("Paidleave");

//                        for (int i = 0; i < Earning.length(); i++) {
//                            JSONObject Earning2 = Earning.getJSONObject(0);
//
//                            earningleaves = Earning2.getString("earningleaves");
//                            earningcompleted = Earning2.getString("earning completed");
//                            earningremaining = Earning2.getString("earning remaining");
//
//                            totalLeave1.setText(earningleaves);
//                            complete_leave_txt.setText(earningcompleted+" "+"Leave");
//                            remaining_txt.setText(earningremaining+" "+"Leave");
//                        }

                        for (int i = 0; i < unpaid.length(); i++) {
                            JSONObject json_unpaid = unpaid.getJSONObject(0);

                            str_total_unpaid = json_unpaid.getString("unpaid_leave");
                            str_complete_unpaid = json_unpaid.getString("unpaid_completed");
                            str_remaining_unpaid = json_unpaid.getString("unpaid_remaining");


                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putString("unpaid_leave", str_total_unpaid);
                            editor.putString("unpaid_completed", str_complete_unpaid);
                            editor.putString("unpaid_remaining", str_remaining_unpaid);

                            editor.commit();


                            total_unpaid.setText(str_total_unpaid);
                            total.setText(str_total_unpaid+" "+"Leaves Per Year");
                            complete_unpaid.setText(str_complete_unpaid+" "+"Leave");
                            completedLeave.setText(str_complete_unpaid+" "+"Leave");
                            remaining_unpaid.setText(str_remaining_unpaid+" "+"Leave");
                            remain.setText(str_remaining_unpaid+" "+"Leave");

                            simpleProgressBar.setMax((int) Double.parseDouble(str_total_unpaid));
                            simpleProgressBar.setProgress((int) Double.parseDouble(str_complete_unpaid));
                        }
//                        for (int i = 0; i < sick.length(); i++) {
//                            JSONObject json_completed = sick.getJSONObject(0);
//
//                            str_total_sick = json_completed.getString("sick leaves");
//                            str_complete_sick = json_completed.getString("sick completed");
//                            str_remaining_sick = json_completed.getString("sick remaining");
//
//                            total_sick.setText(str_total_sick);
//                            complete_sick.setText(str_complete_sick+" "+"Leave");
//                            remaining_sick.setText(str_remaining_sick+" "+"Leave");
//                        }
//                        for (int i = 0; i < Paid.length(); i++) {
//                            JSONObject json_paid = Paid.getJSONObject(0);
//
//                            str_total_paid = json_paid.getString("paid leaves");
//                            str_complete_paid = json_paid.getString("paid completed");
//                            str_remain_paid = json_paid.getString("paid remaining");
//
//                            total_paid.setText(str_total_paid);
//                            complete_paid.setText(str_complete_paid+" "+"Leave");
//                            remain_paid.setText(str_remain_paid+" "+"Leave");
//                        }
                    } else {

                        String status_fail = jsonObject.getString("success");
                        if (status_fail.equals("false")) {
                            Toast.makeText(leaves.this, msg.toString(), Toast.LENGTH_SHORT).show();

                        }

                    }
                } catch (JSONException e1) {


                    Toast.makeText(leaves.this, jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();

                }
                break;
        }


    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        Toast.makeText(leaves.this, jsonstring, Toast.LENGTH_SHORT).show();

    }
}