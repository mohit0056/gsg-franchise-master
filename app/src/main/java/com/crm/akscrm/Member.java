package com.crm.akscrm;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Member extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member);
    }

    public void addMember(View view) {

        Intent intent = new Intent(Member.this, AddMember.class);
        startActivity(intent);
    }

    public void back(View view) {

        super.onBackPressed();
    }

    public void memberDetail(View view) {

        Intent intent = new Intent(Member.this, MemberDetail.class);
        startActivity(intent);
    }
}