package com.crm.akscrm;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class LeaveRequest extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave_request);
    }

    public void reqLeave(View view) {
        Intent intent = new Intent(LeaveRequest.this,RequestLeave.class);
        startActivity(intent);

    }

    public void leaveApprovalStatus(View view) {
        Intent intent = new Intent(LeaveRequest.this,LeaveApprovalStatus.class);
        startActivity(intent);

    }

    public void back(View view) {
        super.onBackPressed();

    }
}