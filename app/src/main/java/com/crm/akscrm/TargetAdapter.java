package com.crm.akscrm;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.crm.akscrm.AllTargets.Monthly;
import com.crm.akscrm.AllTargets.Quarterly;
import com.crm.akscrm.AllTargets.Yearly;

import java.time.Month;

class TargetAdapter extends FragmentPagerAdapter {
    Context context;
    int totalTabs;

    public TargetAdapter(Context c, FragmentManager fm, int totalTabs) {
        super(fm);
        context = c;
        this.totalTabs = totalTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Monthly MonthlyFragment = new Monthly();
                return MonthlyFragment;
            case 1:
                Quarterly QuarterlyFragment = new Quarterly();
                return QuarterlyFragment;
            case 2:
                Yearly YearlyFragment = new Yearly();
                return YearlyFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return totalTabs;
    }
}