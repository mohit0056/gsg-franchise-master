package com.crm.akscrm;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;

import com.crm.akscrm.Adapter.MemberDetailAdapter;
import com.google.android.material.tabs.TabLayout;

public class MemberDetail extends AppCompatActivity {
    ViewPager viewPager;
    TabLayout tabLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_detail);

        viewPager = findViewById(R.id.viewPager);


        tabLayout = findViewById(R.id.tabLayout);

        tabLayout.addTab(tabLayout.newTab().setText("Dealer"));
        tabLayout.addTab(tabLayout.newTab().setText("Distributor"));
        tabLayout.addTab(tabLayout.newTab().setText("Retailer"));
        tabLayout.addTab(tabLayout.newTab().setText("Architect"));
        tabLayout.addTab(tabLayout.newTab().setText("Builder"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final MemberDetailAdapter adapter = new MemberDetailAdapter(this, getSupportFragmentManager(),
                tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

    }

    public void back(View view) {
        super.onBackPressed();
    }
}