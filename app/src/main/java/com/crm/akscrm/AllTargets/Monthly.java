package com.crm.akscrm.AllTargets;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crm.akscrm.Adapter.LeaveApprovalStatusAdapter;
import com.crm.akscrm.Adapter.TargetData;
import com.crm.akscrm.AllTargets.Monthly;
import com.crm.akscrm.LeaveApprovalStatus;
import com.crm.akscrm.R;
import com.crm.akscrm.model.GetLeaveApproveStatus;
import com.crm.akscrm.model.GetTargetData;
import com.crm.akscrm.utill.ApiURL;
import com.crm.akscrm.utill.NetworkCall;
import com.crm.akscrm.utill.Progress;
import com.google.gson.Gson;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.crm.akscrm.utill.ApiURL.getTargetData;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Monthly#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Monthly extends Fragment implements NetworkCall.MyNetworkCallBack, TargetData.ReturnView{
    Progress progress;
    NetworkCall networkCall;
    SharedPreferences mSharedPreference;
    RecyclerView recyclerView;
    RelativeLayout norecord;

    ArrayList<GetTargetData> arrGetTargetData = new ArrayList<>();
    TargetData TargetDataAdapter;
    GetTargetData model_GetTargetData;
    String userid;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public Monthly() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Monthly.
     */
    // TODO: Rename and change types and number of parameters
    public static Monthly newInstance(String param1, String param2) {
        Monthly fragment = new Monthly();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_monthly, container, false);

        progress = new Progress(getContext());
        networkCall = new NetworkCall(Monthly.this, getContext());
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getContext());
        userid = (mSharedPreference.getString("id", ""));
        recyclerView = v.findViewById(R.id.leavestatusApprovalRecycler);
        norecord = v.findViewById(R.id.relativeData);

        getTargetData();



   return v;
    }

    private void getTargetData() {

        networkCall.NetworkAPICall(getTargetData, true);

    }


    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case getTargetData:
                ion = (Builders.Any.B) Ion.with(getContext())
                        .load("POST", getTargetData)
                        .setHeader("token", "zsd16xzv3jsytnp87tk7ygv73k8zmr0ekh6ly7mxaeyeh46oe8")
                        .setBodyParameter("user_id", userid);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case getTargetData:
                try {
                    JSONObject getApprovalStatus = new JSONObject(jsonstring.toString());
                    String status = getApprovalStatus.getString("success");
                    if (status.equals("true")) {

                        JSONArray getApprovalStatusList = getApprovalStatus.getJSONArray("data");

                        for (int i = 0; i < getApprovalStatusList.length(); i++) {
                            model_GetTargetData = new Gson().fromJson(getApprovalStatusList.optJSONObject(i).toString(), GetTargetData.class);
                            arrGetTargetData.add(model_GetTargetData);


                        }

                        TargetDataAdapter  = new TargetData(arrGetTargetData, getContext(), R.layout.layout_target_fragment, this::getAdapterView, 1);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setAdapter(TargetDataAdapter);
                    } else {

                        Toast.makeText(getContext(), "Data not found", Toast.LENGTH_SHORT).show();
                        progress.dismiss();

                    }
                } catch (JSONException e1) {

                    norecord.setVisibility(View.VISIBLE);
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }



    @Override
    public void getAdapterView(View view, List objects, int position, int from) {
        model_GetTargetData = arrGetTargetData.get(position);

        TextView targetAmount, date, remaining, completed, v1Target, v1complete, v1remaining,v2Target, v2complete, v2remaining,v3Target, v3complete, v3remaining,v4Target, v4complete, v4remaining;

        date = view.findViewById(R.id.date);

        String myperiod="", mydateFrom="", mydateTo="", mytype="", myreasons="", mydate="", mystatus="";


//        myperiod = model_GetLeaveApproveStatus.getPeriod();
//        mydateFrom = model_GetLeaveApproveStatus.getDateFrom();
//        mydateTo = model_GetLeaveApproveStatus.getDateTo();
//        mytype = model_GetLeaveApproveStatus.getType();
//        myreasons = model_GetLeaveApproveStatus.getReason();
//        mydate = model_GetLeaveApproveStatus.getDate();
//        mystatus = model_GetLeaveApproveStatus.getStatus();
//
//
//        period.setText(myperiod);
//        dateFrom.setText(mydateFrom);
//        dateTo.setText(mydateTo);
//        type.setText(mytype);
//        reasons.setText(myreasons);
//        date.setText(mydate);
//        status.setText(mystatus);

    }

}