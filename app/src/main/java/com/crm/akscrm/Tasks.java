package com.crm.akscrm;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.tasks.Task;

public class Tasks extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasks);
    }

    public void submitTask(View view) {

        Intent intent = new Intent(Tasks.this,SubmitTask.class);
        startActivity(intent);
    }

    public void TaskReports(View view) {
        Intent intent = new Intent(Tasks.this,TasksReports.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void back(View view) {
        super.onBackPressed();

    }
}