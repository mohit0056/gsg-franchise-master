package com.crm.akscrm.utill;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

public class Helper {
    private static Progress progress;

    private static AlertDialog.Builder builder;

    public static final Pattern EMAIL_PATTERN = Pattern.compile("^([_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{1,25}))?$");

    public static void showShortToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static boolean isConnectingToInternet(Context _context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = connectivityManager.getActiveNetworkInfo();
        if (ni != null && ni.isAvailable() && ni.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    public static String setTheDate(String date) {
        long msTime = Long.valueOf(date);
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(msTime * 1000);
        Date d = cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String converted_date = sdf.format(d);
        return converted_date;
    }


//    public static int subtractDays(String currentDate, String beforeDate) {
//        LocalDate startDay = new LocalDate(Integer.parseInt(beforeDate.substring(0, 4)), Integer.parseInt(beforeDate.substring(5, 7)), Integer.parseInt(beforeDate.substring(8, beforeDate.length())));
//        LocalDate endDay = new LocalDate(Integer.parseInt(currentDate.substring(0, 4)), Integer.parseInt(currentDate.substring(5, 7)), Integer.parseInt(currentDate.substring(8, currentDate.length())));
//        Log.d(TAG, "subtractDays: " + startDay + "," + endDay);
//        return Days.daysBetween(startDay, endDay).getDays();
//
//    }

    public static void deleteFileFromMediaStore(final ContentResolver contentResolver, final File file) {
        String canonicalPath;
        try {
            canonicalPath = file.getCanonicalPath();
        } catch (IOException e) {
            canonicalPath = file.getAbsolutePath();
        }
        final Uri uri = MediaStore.Files.getContentUri("external");
        final int result = contentResolver.delete(uri,
                MediaStore.Files.FileColumns.DATA + "=?",
                new String[]{canonicalPath});
        if (result == 0) {
            final String absolutePath = file.getAbsolutePath();
            if (!absolutePath.equals(canonicalPath)) {
                contentResolver.delete(uri, MediaStore.Files.FileColumns.DATA + "=?", new String[]{absolutePath});
            }
        }
    }

    public static void showProgress(Context context) {
        try {
            progress = new Progress(context);
            progress.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void cancleProgerss() {
        if (progress.isShowing()) {
            progress.dismiss();
        }
    }

    public static void showAlertDialog(Activity activity, String title, String msg) {
        builder = new AlertDialog.Builder(activity);
        builder.setMessage(msg)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.setTitle(title);
        alert.show();
    }
}
