package com.crm.akscrm;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crm.akscrm.Adapter.TaskAdapter;
import com.crm.akscrm.model.GetTask;
import com.crm.akscrm.utill.ApiURL;
import com.crm.akscrm.utill.NetworkCall;
import com.crm.akscrm.utill.Progress;
import com.google.gson.Gson;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TasksReports extends AppCompatActivity implements NetworkCall.MyNetworkCallBack, TaskAdapter.ReturnView {
    Progress progress;
    NetworkCall networkCall;
    SharedPreferences mSharedPreference;
    RecyclerView recyclerView;
    ArrayList<GetTask> arrGetTask = new ArrayList<>();
    TaskAdapter TaskAdapter_adapter;
    GetTask model_GetTask;
    String userid;
RelativeLayout norecord;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasks_reports);

        progress = new Progress(TasksReports.this);
        networkCall = new NetworkCall(TasksReports.this, TasksReports.this);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        userid = (mSharedPreference.getString("id", ""));
        recyclerView = findViewById(R.id.taskRecycler);
        norecord = findViewById(R.id.relativeData);

        getTaskReport();

    }

    private void getTaskReport() {

        networkCall.NetworkAPICall(ApiURL.getTask, true);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void back(View view) {
        super.onBackPressed();

    }


    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.getTask:
                ion = (Builders.Any.B) Ion.with(TasksReports.this)
                        .load("POST", ApiURL.getTask)
                        .setHeader("token", "zsd16xzv3jsytnp87tk7ygv73k8zmr0ekh6ly7mxaeyeh46oe8")
                        .setBodyParameter("user_id", userid);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.getTask:
                try {
                    JSONObject getTask = new JSONObject(jsonstring.toString());
                    String status = getTask.getString("success");
                    String msg = getTask.getString("message");

                    if (status.equals("true")) {

                        JSONArray getTaskList = getTask.getJSONArray("data");

                        for (int i = 0; i < getTaskList.length(); i++) {
                            model_GetTask = new Gson().fromJson(getTaskList.optJSONObject(i).toString(), GetTask.class);
                            arrGetTask.add(model_GetTask);


                        }

                        TaskAdapter_adapter = new TaskAdapter(arrGetTask, TasksReports.this, R.layout.layout_tasks_report, this, 1);
                        recyclerView.setLayoutManager(new LinearLayoutManager(TasksReports.this));
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setAdapter(TaskAdapter_adapter);
                    } else {

                        Toast.makeText(TasksReports.this, "Data not found", Toast.LENGTH_SHORT).show();
                        progress.dismiss();

                    }
                } catch (JSONException e1) {
               //     Toast.makeText(TasksReports.this, "Data not found", Toast.LENGTH_SHORT).show();
               norecord.setVisibility(View.VISIBLE);
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }

    @Override
    public void getAdapterView(View view, List objects, int position, int from) {
        model_GetTask = arrGetTask.get(position);

        TextView category, time, Description, date;

        category = view.findViewById(R.id.distributor);
        time = view.findViewById(R.id.time);
        Description = view.findViewById(R.id.description);
        date = view.findViewById(R.id.date);

        String mcategory = "", mtime = "", mDescription = "", mdate = "";


        mdate = model_GetTask.getDate();
        mtime = model_GetTask.getTime();
        mcategory = model_GetTask.getCategory();
        mDescription = model_GetTask.getDescription();


        category.setText(mcategory);
        time.setText(mtime);
        Description.setText(mDescription);
        date.setText(mdate);


    }

}