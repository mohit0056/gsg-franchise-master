package com.crm.akscrm;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crm.akscrm.utill.ApiURL;
import com.crm.akscrm.utill.NetworkCall;
import com.crm.akscrm.utill.Progress;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class SubmitExpense extends AppCompatActivity implements NetworkCall.MyNetworkCallBack {
    private static final int pic_id = 123;
    private static final int REQUEST_CODE_DOC = 1212;
    // Define the button and imageview type variable
    private static final int PICK_FROM_GALLERY = 1;
    final int RESULT_LOAD_IMG = 1000;
    TextView travelCam, foodCam, hotelCam, tworeading1Cam, tworeading2Cam, fourreading1Cam, fourreading2Cam, otherCam;
    String CAMERA_PERMISSION = android.Manifest.permission.CAMERA;
    int clickImage;
    String strSpinnerType;
    RelativeLayout submit;
    Boolean cancel;
    SimpleDateFormat sdf;
    RelativeLayout twoWheelerLayout,fourWheelerLayout;
    Progress progress;
    Spinner sp1;
    String[] wheeler;
    SharedPreferences mSharedPreference;
    NetworkCall networkCall;
    Uri contentURI1;
    EditText EDTtravel, EDTfood, EDThotel, EDTtwoReading1, EDTtwoReading2, EDTfourReading1, EDTfourReading2, EDTother, EDTdate;
    String READ_EXTERNAL_STORAGE_PERMISSION = android.Manifest.permission.READ_EXTERNAL_STORAGE;
    TextView pic1, pic2, pic3, twopicR1, twopicR2, fourpicR1, fourpicR2, pic6;
    String strpic1, userid, strpic2,strpic21,strpic22,strpic41,strpic42 ,strpic3, strpic4, strpic5, strpic6, strEDTtravel, strEDTdate, strEDTfood, strEDThotel, strEDTReading1, strEDTReading2, strEDTother,strEDTtwoReadig1,strEDTtwoReadig2,strEDTfourReadig1,strEDTfourReadig2;
    ;
    String WRITE_EXTERNAL_STORAGE_PERMISSION = android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_expense);
        progress = new Progress(SubmitExpense.this);
        networkCall = new NetworkCall(SubmitExpense.this, SubmitExpense.this);

        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        userid = (mSharedPreference.getString("id", ""));
        EDTtravel = findViewById(R.id.EDTtravel);
        EDTfood = findViewById(R.id.EDTfood);
        EDThotel = findViewById(R.id.EDThotel);
        EDTtwoReading1 = findViewById(R.id.twoedtR1);
        EDTfourReading1 = findViewById(R.id.fouredtR1);
        EDTtwoReading2 = findViewById(R.id.twoedtR2);
        EDTfourReading2 = findViewById(R.id.fouredtR2);

        EDTother = findViewById(R.id.EDTother);

        twoWheelerLayout = findViewById(R.id.twoWheelerLayout);
        fourWheelerLayout = findViewById(R.id.fourWheelerLayout);

        pic1 = findViewById(R.id.pic1);
        pic2 = findViewById(R.id.pic2);
        pic3 = findViewById(R.id.pic3);
        twopicR1 = findViewById(R.id.twopicR1);
        twopicR2 = findViewById(R.id.twopicR2);
        fourpicR1 = findViewById(R.id.fourpicR1);
        fourpicR2 = findViewById(R.id.fourpicR2);
        pic6 = findViewById(R.id.pic6);

        travelCam = findViewById(R.id.travelCam);
        foodCam = findViewById(R.id.foodCam);
        hotelCam = findViewById(R.id.hotelCam);
        tworeading1Cam = findViewById(R.id.tworeading1Cam);
        tworeading2Cam = findViewById(R.id.tworeading2Cam);
        fourreading1Cam = findViewById(R.id.fourreading1Cam);
        fourreading2Cam = findViewById(R.id.fourreading2Cam);
        otherCam = findViewById(R.id.otherCam);
        sp1 = (Spinner) findViewById(R.id.wheeler_spinner);

        wheeler = getResources().getStringArray(R.array.WheelerSpiner);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, wheeler);
        sp1.setAdapter(adapter);


        sp1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strSpinnerType = String.valueOf(wheeler[i]);
                if (strSpinnerType.equals("Four-Wheeler")) {
//
                    twoWheelerLayout.setVisibility(View.INVISIBLE);
                    fourWheelerLayout.setVisibility(View.VISIBLE);
//


                }else  if (strSpinnerType.equals("Two-Wheeler")) {
//
                    twoWheelerLayout.setVisibility(View.VISIBLE);
                    fourWheelerLayout.setVisibility(View.INVISIBLE);
//


                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        travelCam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (ActivityCompat.checkSelfPermission(SubmitExpense.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(SubmitExpense.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                    } else {
                        clickImage = 1;
                        loadImagefromGallery();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

        foodCam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (ActivityCompat.checkSelfPermission(SubmitExpense.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(SubmitExpense.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                    } else {
                        clickImage = 2;
                        loadImagefromGallery();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

        hotelCam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (ActivityCompat.checkSelfPermission(SubmitExpense.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(SubmitExpense.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                    } else {
                        clickImage = 3;
                        loadImagefromGallery();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

        twopicR1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (ActivityCompat.checkSelfPermission(SubmitExpense.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(SubmitExpense.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                    } else {
                        clickImage = 4;
                        loadImagefromGallery();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

        twopicR2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (ActivityCompat.checkSelfPermission(SubmitExpense.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(SubmitExpense.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                    } else {
                        clickImage = 5;
                        loadImagefromGallery();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

        fourpicR1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (ActivityCompat.checkSelfPermission(SubmitExpense.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(SubmitExpense.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                    } else {
                        clickImage = 6;
                        loadImagefromGallery();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

        fourpicR2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (ActivityCompat.checkSelfPermission(SubmitExpense.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(SubmitExpense.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                    } else {
                        clickImage = 7;
                        loadImagefromGallery();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

        otherCam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (ActivityCompat.checkSelfPermission(SubmitExpense.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(SubmitExpense.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                    } else {
                        clickImage = 8;
                        loadImagefromGallery();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });
        EDTdate = findViewById(R.id.mydate);
        EDTdate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(
                        SubmitExpense.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker,
                                          int selectedyear, int selectedmonth,
                                          int selectedday) {

                        mcurrentDate.set(Calendar.YEAR, selectedyear);
                        mcurrentDate.set(Calendar.MONTH, selectedmonth);
                        mcurrentDate.set(Calendar.DAY_OF_MONTH,
                                selectedday);
                        sdf = new SimpleDateFormat(
                                getResources().getString(
                                        R.string.datecardformat),
                                Locale.US);

                        EDTdate.setText(sdf.format(mcurrentDate
                                .getTime()));
                    }
                }, mYear, mMonth, mDay);

//mdob = dob.getText().toString();
                mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker.show();

            }
        });

        submit = findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strEDThotel = EDThotel.getText().toString().trim();
                strEDTfood = EDTfood.getText().toString().trim();
                strEDTother = EDTother.getText().toString().trim();
                strEDTtravel = EDTtravel.getText().toString().trim();
                strEDTtwoReadig1 = EDTtwoReading1.getText().toString().trim();
                strEDTtwoReadig2 = EDTtwoReading2.getText().toString().trim();
                strEDTfourReadig1 = EDTfourReading1.getText().toString().trim();
                strEDTfourReadig2 = EDTfourReading2.getText().toString().trim();
                strEDTdate = EDTdate.getText().toString().trim();
                EDThotel.setError(null);
                EDTfood.setError(null);
                EDTother.setError(null);
                EDTtravel.setError(null);
                EDTtwoReading1.setError(null);
                EDTtwoReading2.setError(null);
                EDTfourReading1.setError(null);
                EDTfourReading2.setError(null);

                if (TextUtils.isEmpty(strEDTtravel)) {
                    EDTtravel.setError("This Field is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(strEDTdate)) {
                    EDTdate.setError("This Field is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(strEDTfood)) {
                    EDTfood.setError("This Field is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(strEDThotel)) {
                    EDThotel.setError("This Field is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(strEDTReading1)) {
                    EDTtwoReading1.setError("This Field is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(strEDTtwoReadig2)) {
                    EDTtwoReading2.setError("This Field is required");
                    cancel = true;
                }else if (TextUtils.isEmpty(strEDTfourReadig1)) {
                    EDTfourReading1.setError("This Field is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(strEDTfourReadig2)) {
                    EDTfourReading2.setError("This Field is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(strEDTother)) {
                    EDTother.setError("This Field is required");
                    cancel = true;
                }

//                else if (strpic1.equals("")) {
//                    Toast.makeText(SubmitExpense.this, "This Field is Required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                } else if (strpic2.equals("")) {
//                    Toast.makeText(SubmitExpense.this, "This Field is Required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                } else if (strpic3.equals("")) {
//                    Toast.makeText(SubmitExpense.this, "This Field is Required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                } else if (strpic4.equals("")) {
//                    Toast.makeText(SubmitExpense.this, "This Field is Required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                } else if (strpic5.equals("")) {
//                    Toast.makeText(SubmitExpense.this, "This Field is Required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                } else if (strpic6.equals("")) {
//                    Toast.makeText(SubmitExpense.this, "This Field is Required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }


                else {


                    submitExpenses();

                }


            }
        });

    }


    private void loadImagefromGallery() {
        if (clickImage == 1) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        } else if (clickImage == 2) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        } else if (clickImage == 3) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        } else if (clickImage == 4) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        } else if (clickImage == 5) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        } else if (clickImage == 6) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        }else if (clickImage == 7) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        } else if (clickImage == 8) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (clickImage) {
            case 1:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI1 = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            strpic1 = getRealPathFromURI(selectedImage);
                            File File_data_1 = new File(strpic1);
                            String id_proof_name = File_data_1.getName();
                            pic1.setText(id_proof_name);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
            case 2:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI1 = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            strpic2 = getRealPathFromURI(selectedImage);
                            File File_data_1 = new File(strpic2);
                            String address_proof_name = File_data_1.getName();
                            pic2.setText(address_proof_name);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
            case 3:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI1 = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            strpic3 = getRealPathFromURI(selectedImage);
                            File File_data_1 = new File(strpic3);
                            String identity_name = File_data_1.getName();
                            pic3.setText(identity_name);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;


            case 4:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI1 = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            strpic21 = getRealPathFromURI(selectedImage);
                            File File_data_1 = new File(strpic21);
                            String sales_tax_name = File_data_1.getName();
                            twopicR1.setText(sales_tax_name);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                    break;
                }
            case 5:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI1 = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            strpic22 = getRealPathFromURI(selectedImage);
                            File File_data_1 = new File(strpic22);
                            String project_report_name = File_data_1.getName();
                            twopicR2.setText(project_report_name);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
            case 6:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI1 = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            strpic41 = getRealPathFromURI(selectedImage);
                            File File_data_1 = new File(strpic41);
                            String sales_tax_name = File_data_1.getName();
                            fourpicR1.setText(sales_tax_name);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                    break;
                }
            case 7:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI1 = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            strpic42 = getRealPathFromURI(selectedImage);
                            File File_data_1 = new File(strpic42);
                            String project_report_name = File_data_1.getName();
                            fourpicR2.setText(project_report_name);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;

            case 8:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI1 = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            strpic6 = getRealPathFromURI(selectedImage);
                            File File_data_1 = new File(strpic6);
                            String party_guarantee_name = File_data_1.getName();
                            pic6.setText(party_guarantee_name);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;

        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void back(View view) {
        super.onBackPressed();

    }

    private void submitExpenses() {

        networkCall.NetworkAPICall("https://www.daviteks.com/crm-APIs/userExpense", true);

    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case "https://www.daviteks.com/crm-APIs/userExpense":
                ion = (Builders.Any.B) Ion.with(SubmitExpense.this)
                        .load("POST", "https://www.daviteks.com/crm-APIs/userExpense")
                        .setHeader("token", "zsd16xzv3jsytnp87tk7ygv73k8zmr0ekh6ly7mxaeyeh46oe8")
                        .setMultipartParameter("user_id", userid)
                        .setMultipartParameter("travel_amount", strEDTtravel)
                        .setMultipartFile("travel_image", new File(strpic1))

                        .setMultipartParameter("food_amount", strEDTfood)
                        .setMultipartFile("food_image", new File(strpic2))

                        .setMultipartParameter("hotel_amount", strEDThotel)
                        .setMultipartFile("hotel_image", new File(strpic3))

                        .setMultipartParameter("twoWheeler", strEDTReading1)
                        .setMultipartFile("twoWheeler_image", new File(strpic4))

                        .setMultipartParameter("fourWheeler", strEDTReading2)
                        .setMultipartFile("fourWheeler_image", new File(strpic5))

                        .setMultipartParameter("date", strEDTdate)
                        .setMultipartParameter("other", strEDTother)
                        .setMultipartFile("other_image", new File(strpic6))


                ;
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case "https://www.daviteks.com/crm-APIs/userExpense":

                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");

                    if (succes.equals("true")) {
                        Toast.makeText(SubmitExpense.this, msg, Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(SubmitExpense.this, Expenses.class);
                        startActivity(intent);
                        SubmitExpense.this.finish();
                    } else {

                        String status_fail = jsonObject.getString("success");
                        if (status_fail.equals("false")) {
                            Toast.makeText(SubmitExpense.this, msg.toString(), Toast.LENGTH_SHORT).show();

                        }

                    }
                } catch (JSONException e1) {

                    Toast.makeText(SubmitExpense.this, e1.toString(), Toast.LENGTH_SHORT).show();


                }
                break;
        }


    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        Toast.makeText(SubmitExpense.this, jsonstring.toString(), Toast.LENGTH_SHORT).show();

    }
}