package com.crm.akscrm;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crm.akscrm.Adapter.AttendanceAdapter;
import com.crm.akscrm.Adapter.TaskAdapter;
import com.crm.akscrm.model.GetAttendanceHistory;
import com.crm.akscrm.model.GetTask;
import com.crm.akscrm.utill.ApiURL;
import com.crm.akscrm.utill.NetworkCall;
import com.crm.akscrm.utill.Progress;
import com.google.gson.Gson;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AttendanceHistory extends AppCompatActivity implements NetworkCall.MyNetworkCallBack, AttendanceAdapter.ReturnView{
    Progress progress;
    NetworkCall networkCall;
    SharedPreferences mSharedPreference;
    RecyclerView recyclerView;
    ArrayList<GetAttendanceHistory> arrGetAttendanceHistory = new ArrayList<>();
    AttendanceAdapter attendance_adapter;
    GetAttendanceHistory getAttendanceHistory;
    String userid;
    RelativeLayout norecord;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_history);

        progress = new Progress(AttendanceHistory.this);
        networkCall = new NetworkCall(AttendanceHistory.this, AttendanceHistory.this);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        userid = (mSharedPreference.getString("id", ""));
        recyclerView = findViewById(R.id.taskRecycler);
        norecord = findViewById(R.id.relativeData);

        getAttendanceHistory();

    }

    private void getAttendanceHistory() {
        networkCall.NetworkAPICall(ApiURL.attendanceHistory, true);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void back(View view) {
        super.onBackPressed();

    }

    @Override
    public void getAdapterView(View view, List objects, int position, int from) {

        getAttendanceHistory = arrGetAttendanceHistory.get(position);

        TextView loginTime,loginLocation,logoutTime,logoutLocation;

        loginTime = view.findViewById(R.id.loginTime);
        loginLocation = view.findViewById(R.id.loginlocation);
        logoutTime = view.findViewById(R.id.logoutTime);
        logoutLocation = view.findViewById(R.id.logoutLocation);

        String mloginTime = "", mloginLocation = "", mlogoutTime = "", mlogoutLocation = "";


        mloginTime = getAttendanceHistory.getPunchintime();
        mloginLocation = getAttendanceHistory.getPunchinlocation();
        mlogoutTime = getAttendanceHistory.getPunchouttime();
        mlogoutLocation = getAttendanceHistory.getPunchoutlocation();


        loginTime.setText(mloginTime);
        loginLocation.setText(mloginLocation);
        logoutTime.setText(mlogoutTime);
        logoutLocation.setText(mlogoutLocation);


    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.attendanceHistory:
                ion = (Builders.Any.B) Ion.with(AttendanceHistory.this)
                        .load("POST", ApiURL.attendanceHistory)
                        .setHeader("token", "zsd16xzv3jsytnp87tk7ygv73k8zmr0ekh6ly7mxaeyeh46oe8")
                        .setBodyParameter("user_id", userid);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.attendanceHistory:
                try {
                    JSONObject getTask = new JSONObject(jsonstring.toString());
                    String status = getTask.getString("success");
                    String msg = getTask.getString("message");

                    if (status.equals("true")) {

                        JSONArray getTaskList = getTask.getJSONArray("data");

                        for (int i = 0; i < getTaskList.length(); i++) {
                            getAttendanceHistory = new Gson().fromJson(getTaskList.optJSONObject(i).toString(), GetAttendanceHistory.class);
                            arrGetAttendanceHistory.add(getAttendanceHistory);


                        }

                        attendance_adapter= new AttendanceAdapter(arrGetAttendanceHistory, AttendanceHistory.this, R.layout.layout_attendance_history, this, 1);
                        recyclerView.setLayoutManager(new LinearLayoutManager(AttendanceHistory.this));
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setAdapter(attendance_adapter);
                    } else {

                        Toast.makeText(AttendanceHistory.this, "Data not found", Toast.LENGTH_SHORT).show();
                        progress.dismiss();

                    }
                } catch (JSONException e1) {
                    //     Toast.makeText(AttendanceHistory.this, "Data not found", Toast.LENGTH_SHORT).show();
                    norecord.setVisibility(View.VISIBLE);
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }
}