package com.crm.akscrm;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.andreseko.SweetAlert.SweetAlertDialog;
import com.crm.akscrm.utill.ApiURL;
import com.crm.akscrm.utill.NetworkCall;
import com.crm.akscrm.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Login extends AppCompatActivity implements NetworkCall.MyNetworkCallBack {

    EditText emailorphone, password;
    RelativeLayout register;
    String myemailphone, mypassword, userid, name, status;
    final String regexStr = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";
    Boolean cancel;
    Progress progress;
    Boolean isLogin;
    static final String pref_name = "Scratch";
    SharedPreferences mSharedPreference;
    NetworkCall networkCall;

    SweetAlertDialog sweetAlertDialog;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mSharedPreference = getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        userid = (mSharedPreference.getString("id", ""));


        isLogin = mSharedPreference.getBoolean("login", false);
        if (isLogin) {
            Intent intent = new Intent(Login.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }

        progress = new Progress(Login.this);
        networkCall = new NetworkCall(Login.this, Login.this);

        emailorphone = findViewById(R.id.emailormobileEDT);
        password = findViewById(R.id.passordEDT);
        register = findViewById(R.id.signin);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                myemailphone = emailorphone.getText().toString().trim();

                mypassword = password.getText().toString().trim();


                emailorphone.setError(null);
                password.setError(null);


                if (TextUtils.isEmpty(myemailphone)) {
                    emailorphone.setError("This Field is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(mypassword)) {
                    password.setError("Password is required");
                    cancel = true;
                } else if (mypassword.length() < 6) {
                    password.setError("Password should be of min. 6 characters");
                    cancel = true;
                }
//
                else {

                    Loginnow();


                }

            }


        });

    }

    private void Loginnow() {

        networkCall.NetworkAPICall(ApiURL.user_login, true);


    }

    public void register(View view) {

        Intent intent = new Intent(Login.this, Register.class);
        startActivity(intent);
        Login.this.finish();

    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.user_login:
                ion = (Builders.Any.B) Ion.with(Login.this)
                        .load("POST", ApiURL.user_login)
                        .setHeader("token", "zsd16xzv3jsytnp87tk7ygv73k8zmr0ekh6ly7mxaeyeh46oe8")
                        .setBodyParameter("email/mobile", myemailphone)
                        .setBodyParameter("password", mypassword)

                ;
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.user_login:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String status = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");


                    if (status.equals("true")) {

                        JSONArray jsonObject1 = jsonObject.getJSONArray("data");

                        for (int i = 0; i < jsonObject1.length(); i++) {
                            JSONObject Jasonobject2 = jsonObject1.getJSONObject(0);
                            userid = Jasonobject2.getString("id");
                            name = Jasonobject2.getString("name");
                            status = Jasonobject2.getString("status");

                        }
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("id", userid);
                        editor.putString("name", name);

                        editor.commit();
                        mSharedPreference.edit().putBoolean("login", true).commit();

                        //Toast.makeText(this, "Successfully Logged-In", Toast.LENGTH_SHORT).show();
                        if (status.equals("Pending")) {

                            sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
                            sweetAlertDialog.setTitleText("Pending");
                            sweetAlertDialog.setContentText(msg);
                            sweetAlertDialog.show();

                            Button btn = (Button) sweetAlertDialog.findViewById(R.id.confirm_button);
                            btn.setBackgroundColor(ContextCompat.getColor(Login.this, R.color.black));

                            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {


                                 sweetAlertDialog.dismiss();

                                }
                            });
                            sweetAlertDialog.setCancelable(false);
                            sweetAlertDialog.show();



                        } else {
                            Intent intent = new Intent(Login.this, MainActivity.class);
                            startActivity(intent);
                            finish();

                        }


                    } else {


                        String fail_status = jsonObject.getString("success");

                        if (fail_status.equals("false")) {

                            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Pending")
                                    .setContentText("You can login once your account got approved...")

                                    .show();
                        }
                        progress.dismiss();
                    }
                } catch (JSONException e1) {

                    Toast.makeText(Login.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;
        }

    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        Toast.makeText(Login.this, "" + jsonstring, Toast.LENGTH_SHORT).show();

    }
}