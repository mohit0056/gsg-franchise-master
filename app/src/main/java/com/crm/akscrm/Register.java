package com.crm.akscrm;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.andreseko.SweetAlert.SweetAlertDialog;
import com.crm.akscrm.utill.ApiURL;
import com.crm.akscrm.utill.GPSTracker;
import com.crm.akscrm.utill.NetworkCall;
import com.crm.akscrm.utill.Progress;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.Gson;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static java.security.AccessController.getContext;

public class Register extends AppCompatActivity implements NetworkCall.MyNetworkCallBack {
    Spinner sp1;
    String[] role;
    TextView hide;
    RelativeLayout register;
    EditText fullname, password, phone, email, confPass, location, dob ,addEntry;
    GPSTracker gps;
    double latitude, longitude;
    String myrole, myfullname, mypassword, myphone, myemail, myconfPass, mylocation, mydob;
    final String regexStr = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";
    String key = "AIzaSyAG5yWZ4F-wBcOn6I1ci-PUl-kgt012Zm4";
    Boolean cancel;
    SimpleDateFormat sdf;
    String userid;
    SweetAlertDialog sweetAlertDialog;

    int clicklocation;
    private Geocoder geocoder;
    private final int REQUEST_PLACE_ADDRESS = 40;

    static final String pref_name = "Scratch";
    Progress progress;


    SharedPreferences mSharedPreference;
    NetworkCall networkCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        progress = new Progress(Register.this);
        networkCall = new NetworkCall(Register.this, Register.this);


        addEntry = findViewById(R.id.others);
        password = findViewById(R.id.passwordEDT);

        confPass = findViewById(R.id.confpassEDT);

        email = findViewById(R.id.emailEDT);
        location = findViewById(R.id.locationEDT);
        phone = findViewById(R.id.mobileEDT);
        dob = findViewById(R.id.dobEDT);
        fullname = findViewById(R.id.fullnameEDT);

        dob.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(
                        Register.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker,
                                          int selectedyear, int selectedmonth,
                                          int selectedday) {

                        mcurrentDate.set(Calendar.YEAR, selectedyear);
                        mcurrentDate.set(Calendar.MONTH, selectedmonth);
                        mcurrentDate.set(Calendar.DAY_OF_MONTH,
                                selectedday);
                        sdf = new SimpleDateFormat(
                                getResources().getString(
                                        R.string.datecardformat),
                                Locale.US);

                        dob.setText(sdf.format(mcurrentDate
                                .getTime()));
                    }
                }, mYear, mMonth, mDay);
                mydob = dob.toString().trim();
//                mydob = sdf.format(mcurrentDate
//                        .getTime());

                mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker.show();

            }
        });

        sp1 = (Spinner) findViewById(R.id.gender_spinner);
        role = getResources().getStringArray(R.array.roles);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, role);
        sp1.setAdapter(adapter);


        sp1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                myrole = String.valueOf(role[i]);
                if (myrole.equals("Others")) {

                    addEntry.setVisibility(View.VISIBLE);


                           String medit =  addEntry.getText().toString();
                           myrole=medit;
                }else{

                    addEntry.setVisibility(View.GONE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        register = findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                myfullname = fullname.getText().toString().trim();
                myphone = phone.getText().toString().trim();
                mypassword = password.getText().toString().trim();
                myconfPass = confPass.getText().toString().trim();
                myemail = email.getText().toString().trim();
                mydob = dob.getText().toString().trim();
                myemail = email.getText().toString().trim();
                mylocation = location.getText().toString().trim();

                fullname.setError(null);
                phone.setError(null);
                email.setError(null);
                password.setError(null);
                dob.setError(null);
                confPass.setError(null);
                location.setError(null);
                if (myrole.equals("Choose Role")) {
                    Toast.makeText(Register.this, "Please Choose Role", Toast.LENGTH_SHORT).show();
                    cancel = true;
                } else if (TextUtils.isEmpty(myfullname)) {
                    fullname.setError("Your Full Name is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(myemail)) {
                    email.setError("Re-enter Password is required");
                    cancel = true;
                } else if (!myemail.matches(ApiURL.emailRegex)) {
                    email.setError("Please Enter a valid Email");
                    cancel = true;
                } else if (TextUtils.isEmpty(mylocation)) {
                    location.setError("Location is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(mydob)) {
                    dob.setError("Date of Birth is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(myphone)) {
                    phone.setError("Phone number is required");
                    cancel = true;
                } else if (!myphone.matches(regexStr)) {
                    phone.setError("Please Enter Valid Phone no.");
                    cancel = true;
                } else if (TextUtils.isEmpty(mypassword)) {
                    password.setError("Password is required");
                    cancel = true;
                } else if (mypassword.length() < 6) {
                    password.setError("Password should be of min. 6 characters");
                    cancel = true;
                } else if (TextUtils.isEmpty(myconfPass)) {
                    confPass.setError(" Confirm Password");
                    cancel = true;
                }
//                else if (!confPass.equals(password)) {
//                    confPass.setError("Password does'nt match");
//                    cancel = true;
//                }
                else {

                    signUpMobile();


                }

            }


        });


//        Places.initialize(Register.this, key);
//        location.setFocusable(false);
//        location.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////              mloadingpointedt=loadingpointedt.getText().toString();
//                clicklocation = 1;
//
//                List<Place.Field> fieldList = Arrays.asList(Place.Field.ADDRESS, Place.Field.LAT_LNG, Place.Field.NAME);
//                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, Arrays.asList(Place.Field.ADDRESS_COMPONENTS, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)).build(Register.this);
//                startActivityForResult(intent, REQUEST_PLACE_ADDRESS);
//
////                Intent intent= new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY,fieldList).build(getContext());
////                startActivityForResult(intent,100);
//
//            }
//        });

    }

    private void signUpMobile() {

        networkCall.NetworkAPICall(ApiURL.user_register, true);


    }
    public void location(View view) {
//        location.setText(mylocation);


        currentLocation();

    }

    private void currentLocation() {

        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Register.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
//            Toast.makeText(getApplicationContext(),"You need have granted permission",Toast.LENGTH_SHORT).show();
            gps = new GPSTracker(Register.this);
            // Check if GPS enabled
            if (gps.canGetLocation()) {
                latitude = gps.getLatitude();
                longitude = gps.getLongitude();
//                Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                try {
                    Geocoder geo = new Geocoder(Register.this.getApplicationContext(), Locale.getDefault());
                    List<Address> addresses = geo.getFromLocation(latitude, longitude, 1);
                    if (addresses.isEmpty()) {
//                        customeraddress.setText("Waiting for Location");
                    } else {
                        if (addresses.size() > 0) {
                            mylocation = addresses.get(0).getFeatureName() + ", " + addresses.get(0).getLocality() + ", " + addresses.get(0).getAdminArea() + ", " + addresses.get(0).getCountryName();


                            location.setText(mylocation);

                            SharedPreferences.Editor editor = mSharedPreference.edit();
                            editor.putString("location", mylocation);
                            editor.apply();


//                            customeraddress.setText(maddress);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace(); // getFromLocation() may sometimes fail
                }
            } else {
                gps.showSettingsAlert();
            }
        }

    }
    public void SignIn(View view) {

        Intent intent = new Intent(Register.this, Login.class);
        startActivity(intent);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (clicklocation) {
            case 1:
                if (requestCode == REQUEST_PLACE_ADDRESS && resultCode == Activity.RESULT_OK) {
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    try {
                        List<Address> addresses;
                        geocoder = new Geocoder(Register.this, Locale.getDefault());
                        try {
                            addresses = geocoder.getFromLocation(place.getLatLng().latitude, place.getLatLng().longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                            String address1 = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                            String address2 = addresses.get(0).getAddressLine(1); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                            String city = addresses.get(0).getLocality();
                            String state = addresses.get(0).getAdminArea();
                            String country = addresses.get(0).getCountryName();
                            String postalCode = addresses.get(0).getPostalCode();

                            Log.e("Address1: ", "" + address1);
                            Log.e("Address2: ", "" + address2);
                            Log.e("AddressCity: ", "" + city);
                            Log.e("AddressState: ", "" + state);
                            Log.e("AddressCountry: ", "" + country);
                            Log.e("AddressPostal: ", "" + postalCode);
                            Log.e("AddressLatitude: ", "" + place.getLatLng().latitude);
                            Log.e("AddressLongitude: ", "" + place.getLatLng().longitude);

                            mylocation = addresses.get(0).getLocality();
                            location.setText(mylocation);


                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        //setMarker(latLng);
                    }
                }
                break;

        }

//        if(requestCode==100 && resultCode==RESULT_OK){
//            Place place=Autocomplete.getPlaceFromIntent(data);
//            loadingpointedt.setText(place.getAddress());
//        }else if (resultCode== AutocompleteActivity.RESULT_ERROR){
//            Status status =Autocomplete.getStatusFromIntent(data);
//            Toast.makeText(getContext(), status.getStatusMessage(), Toast.LENGTH_SHORT).show();
//
//        }
    }


    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.user_register:
                ion = (Builders.Any.B) Ion.with(Register.this)
                        .load("POST", ApiURL.user_register)
                        .setHeader("token", "zsd16xzv3jsytnp87tk7ygv73k8zmr0ekh6ly7mxaeyeh46oe8")
                        .setBodyParameter("fullname", myfullname)
                        .setBodyParameter("mobile", myphone)
                        .setBodyParameter("email", myemail)
                        .setBodyParameter("role", myrole)
                        .setBodyParameter("dob", mydob)
                        .setBodyParameter("currentaddress", mylocation)
                        .setBodyParameter("password", mypassword)
                        .setBodyParameter("confirm_password", myconfPass)
                ;
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.user_register:

                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");

                    if (succes.equals("true")) {
//                        JSONArray jsonObject1 = jsonObject.getJSONArray("data");
//
//                        for (int i = 0; i < jsonObject1.length(); i++) {
//                            JSONObject Jasonobject2 = jsonObject1.getJSONObject(0);
//                            userid = Jasonobject2.getString("id");
//
//                        }
//                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//                        SharedPreferences.Editor editor = prefs.edit();
//                        editor.putString("id", userid);
//
//                        editor.commit();
//                        mSharedPreference.edit().putBoolean("login", true).commit();

//                        new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
//                                .setTitleText("Registered")
//                                .setContentText(msg)
//                                .show();
////                        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
//
//
//                        Intent intent = new Intent(Register.this, Login.class);
//                        startActivity(intent);
//                        finish();

                        sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);
                        sweetAlertDialog.setTitleText("Registered Successfully");
                        sweetAlertDialog.setContentText("Your account is in Pending Mode.. Please Wait until it got approved..");
                        sweetAlertDialog.show();

                        Button btn = (Button) sweetAlertDialog.findViewById(R.id.confirm_button);
                        btn.setBackgroundColor(ContextCompat.getColor(Register.this, R.color.black));

                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {


                                Intent intent = new Intent(Register.this, Login.class);

                                startActivity(intent);
                                Register.this.finish();

                            }
                        });
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.show();



                    } else {

                        String status_fail = jsonObject.getString("success");
                        if (status_fail.equals("false")) {


//                            Toast.makeText(Register.this, msg.toString(), Toast.LENGTH_SHORT).show();

                        }

                    }
                } catch (JSONException e1) {


                    Toast.makeText(Register.this, jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();

                }
                break;
        }

    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }

}