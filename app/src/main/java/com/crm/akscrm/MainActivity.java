package com.crm.akscrm;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    private DrawerLayout mDrawer;
TextView name,id;
String myname,myid;
    SharedPreferences mSharedPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        myid = (mSharedPreference.getString("id", ""));
        myname = (mSharedPreference.getString("name", ""));

        mDrawer = (DrawerLayout) findViewById(R.id.drawerlayout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.vNavigation);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);


         name = (TextView)header.findViewById(R.id.name);
         id = (TextView)header.findViewById(R.id.id);
        name.setText(myname);
        id.setText(myid);
    }

    public void attendance(View view) {

        Intent intent = new Intent(MainActivity.this, Attendance.class);
        startActivity(intent);


    }

    public void Expenses(View view) {

        Intent intent = new Intent(MainActivity.this, Expenses.class);
        startActivity(intent);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void LeaveReq(View view) {
        Intent intent = new Intent(MainActivity.this, LeaveRequest.class);
        startActivity(intent);


    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.attendance) {

            Intent attendance = new Intent(MainActivity.this,Attendance.class);
            startActivity(attendance);
            return true;
            // Handle the camera action
        }

        else  if (id == R.id.logout) {

            Intent intent = new Intent(MainActivity.this, Login.class);
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit();
            editor.clear();
            editor.apply();
            editor.remove("login");//your key
            startActivity(intent);
            MainActivity.this.finish();

            return true;
            // Handle the camera action
        }



        else  if (id == R.id.leave) {

            Intent attendance = new Intent(MainActivity.this,LeaveRequest.class);
            startActivity(attendance);
            return true;
            // Handle the camera action
        }

        else  if (id == R.id.tasks) {

            Intent attendance = new Intent(MainActivity.this,Tasks.class);
            startActivity(attendance);
            return true;
            // Handle the camera action
        }


        else  if (id == R.id.addMembers) {

            Intent attendance = new Intent(MainActivity.this,AddMember.class);
            startActivity(attendance);
            return true;
            // Handle the camera action
        }

        else  if (id == R.id.Expenses) {

            Intent attendance = new Intent(MainActivity.this,Expenses.class);
            startActivity(attendance);
            return true;
            // Handle the camera action
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerlayout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void openDrawer(View view) {
        mDrawer.openDrawer(Gravity.LEFT);

    }

//    public void Targets(View view) {
//
//        Intent intent = new Intent(MainActivity.this, Targets.class);
//        startActivity(intent);
//    }

    public void Tasks(View view) {

        Intent intent = new Intent(MainActivity.this, Tasks.class);
        startActivity(intent);
    }

    public void member(View view) {
        Intent intent = new Intent(MainActivity.this, Member.class);
        startActivity(intent);

    }
}