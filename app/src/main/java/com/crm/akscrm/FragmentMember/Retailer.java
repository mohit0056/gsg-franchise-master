package com.crm.akscrm.FragmentMember;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crm.akscrm.Adapter.MemberDataAdapter;
import com.crm.akscrm.R;
import com.crm.akscrm.model.GetMemberDetails;
import com.crm.akscrm.utill.ApiURL;
import com.crm.akscrm.utill.NetworkCall;
import com.crm.akscrm.utill.Progress;
import com.google.gson.Gson;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Retailer#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Retailer extends Fragment implements NetworkCall.MyNetworkCallBack, MemberDataAdapter.ReturnView {
    Progress progress;
    NetworkCall networkCall;
    SharedPreferences mSharedPreference;
    RecyclerView recyclerView;
    RelativeLayout norecord;

    ArrayList<GetMemberDetails> arrGetMemberDetails = new ArrayList<>();
    MemberDataAdapter memberDataAdapter;
    GetMemberDetails model_GetMemberDetails;
    String userid;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public Retailer() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Retailer.
     */
    // TODO: Rename and change types and number of parameters
    public static Retailer newInstance(String param1, String param2) {
        Retailer fragment = new Retailer();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_retailer, container, false);

        progress = new Progress(getContext());
        networkCall = new NetworkCall(Retailer.this, getContext());
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getContext());
        userid = (mSharedPreference.getString("id", ""));
        recyclerView = v.findViewById(R.id.retailer);
        norecord = v.findViewById(R.id.relativeData);

        getMemberData();


        return v;
    }

    private void getMemberData() {

        networkCall.NetworkAPICall(ApiURL.getMember, true);

    }


    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.getMember:
                ion = (Builders.Any.B) Ion.with(getContext())
                        .load("GET", ApiURL.getMember)
                        .setHeader("token", "zsd16xzv3jsytnp87tk7ygv73k8zmr0ekh6ly7mxaeyeh46oe8")
                ;
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.getMember:
                try {
                    JSONObject getApprovalStatus = new JSONObject(jsonstring.toString());
                    String status = getApprovalStatus.getString("success");
                    if (status.equals("true")) {

                        JSONArray getApprovalStatusList = getApprovalStatus.getJSONArray("data");

                        for (int i = 0; i < getApprovalStatusList.length(); i++) {

                            String vendor = getApprovalStatusList.optJSONObject(i).getString("role");
                            if (vendor.equals("Retailer")) {
                                model_GetMemberDetails = new Gson().fromJson(getApprovalStatusList.optJSONObject(i).toString(), GetMemberDetails.class);


                                arrGetMemberDetails.add(model_GetMemberDetails);

                            }
                        }

                        memberDataAdapter = new MemberDataAdapter(arrGetMemberDetails, getContext(), R.layout.layout_member_detail, this::getAdapterView, 1);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setAdapter(memberDataAdapter);
                    } else {

                        Toast.makeText(getContext(), "Data not found", Toast.LENGTH_SHORT).show();
                        progress.dismiss();

                    }
                } catch (JSONException e1) {

                    norecord.setVisibility(View.VISIBLE);
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }

    @Override
    public void getAdapterView(View v, List objects, int position, int from) {

        model_GetMemberDetails = arrGetMemberDetails.get(position);

        TextView name, email, mobile, location;

        name = v.findViewById(R.id.name);
        email = v.findViewById(R.id.email);
        mobile = v.findViewById(R.id.mobile);
        location = v.findViewById(R.id.location);

        String myname, myemail, mymobile, mylocation;


        myname = model_GetMemberDetails.getName();
        myemail = model_GetMemberDetails.getEmail();
        mymobile = model_GetMemberDetails.getMobile();
        mylocation = model_GetMemberDetails.getLocation();


        name.setText(myname);
        email.setText(myemail);
        mobile.setText(mymobile);
        location.setText(mylocation);


    }

}