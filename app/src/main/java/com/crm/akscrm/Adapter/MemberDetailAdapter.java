package com.crm.akscrm.Adapter;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.crm.akscrm.AllTargets.Monthly;
import com.crm.akscrm.AllTargets.Quarterly;
import com.crm.akscrm.AllTargets.Yearly;
import com.crm.akscrm.FragmentMember.Architect;
import com.crm.akscrm.FragmentMember.Builder;
import com.crm.akscrm.FragmentMember.Dealer;
import com.crm.akscrm.FragmentMember.Distributor;
import com.crm.akscrm.FragmentMember.Retailer;

public class MemberDetailAdapter extends FragmentPagerAdapter {
    Context context;
    int totalTabs;

    public MemberDetailAdapter(Context c, FragmentManager fm, int totalTabs) {
        super(fm);
        context = c;
        this.totalTabs = totalTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {


            case 0:
                Dealer DealerFragment = new Dealer();
                return DealerFragment;
            case 1:
                Distributor DistributorFragment = new Distributor();
                return DistributorFragment;
            case 2:
                Retailer RetailerFragment = new Retailer();
                return RetailerFragment;
            case 3:
                Architect ArchitectFragment = new Architect();
                return ArchitectFragment;
            case 4:
                Builder BuilderFragment = new Builder();
                return BuilderFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return totalTabs;
    }
}