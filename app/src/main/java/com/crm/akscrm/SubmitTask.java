package com.crm.akscrm;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.crm.akscrm.utill.ApiURL;
import com.crm.akscrm.utill.GPSTracker;
import com.crm.akscrm.utill.NetworkCall;
import com.crm.akscrm.utill.Progress;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class SubmitTask extends AppCompatActivity {

    TextView hide;
    Spinner sp1;
    FusedLocationProviderClient client;
    SupportMapFragment mapFragment;
    String getLocation;
    GPSTracker gps;
    double latitude, longitude;
    String strLat, strLong;
    String[] role;
    EditText description, time, date;
    RelativeLayout submit;
    String myrole, mydescription, mytime, mydate, userid;
    final String regexStr = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";
    String key = "AIzaSyAG5yWZ4F-wBcOn6I1ci-PUl-kgt012Zm4";
    Boolean cancel;
    ImageView clockicon, dateicon;
    SimpleDateFormat sdf;
    int clicklocation;
    private Geocoder geocoder;
    private final int REQUEST_PLACE_ADDRESS = 40;

    static final String pref_name = "Scratch";
    Progress progress;


    SharedPreferences mSharedPreference;
    NetworkCall networkCall;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_task);

        progress = new Progress(SubmitTask.this);
//        networkCall = new NetworkCall(SubmitTask.this, SubmitTask.this);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        userid = (mSharedPreference.getString("id", ""));

        dateicon = findViewById(R.id.calender);

        description = findViewById(R.id.description);
//        time = findViewById(R.id.time);
//        time.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                Calendar mcurrentTime = Calendar.getInstance();
//                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
//                int minute = mcurrentTime.get(Calendar.MINUTE);
//                TimePickerDialog mTimePicker;
//                mTimePicker = new TimePickerDialog(SubmitTask.this, new TimePickerDialog.OnTimeSetListener() {
//                    @Override
//                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//                        time.setText( selectedHour + ":" + selectedMinute);
//                    clockicon.setVisibility(View.INVISIBLE);
//                    }
//                }, hour, minute, true);//Yes 24 hour time
//                mytime = time.toString().trim();
//
//                mTimePicker.setTitle("Select Time");
//                mTimePicker.show();
//
//            }
//        });

        date = findViewById(R.id.date);
        date.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(
                        SubmitTask.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker,
                                          int selectedyear, int selectedmonth,
                                          int selectedday) {

                        mcurrentDate.set(Calendar.YEAR, selectedyear);
                        mcurrentDate.set(Calendar.MONTH, selectedmonth);
                        mcurrentDate.set(Calendar.DAY_OF_MONTH,
                                selectedday);
                        sdf = new SimpleDateFormat(
                                getResources().getString(
                                        R.string.datecardformat),
                                Locale.US);

                        date.setText(sdf.format(mcurrentDate
                                .getTime()));
                        dateicon.setVisibility(View.INVISIBLE);


                    }
                }, mYear, mMonth, mDay);
                mydate = date.toString().trim();
//                mydate = sdf.format(mcurrentDate
//                        .getTime());

                mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker.show();

            }
        });

        sp1 = (Spinner) findViewById(R.id.gender_spinner);
        role = getResources().getStringArray(R.array.taskaRole);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, role);
        sp1.setAdapter(adapter);


        sp1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // hide.setVisibility(View.GONE);
                myrole = String.valueOf(role[i]);
                if (i == 5) {


                } else {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        submit = findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mydate = date.getText().toString().trim();
//                mytime = time.getText().toString().trim();
                mydescription = description.getText().toString().trim();

                date.setError(null);
                // time.setError(null);
                description.setError(null);

                if (TextUtils.isEmpty(mydate)) {
                    date.setError("This Field is required");
                    cancel = true;
                } else if (myrole.equals("Choose Partner")) {
                    Toast.makeText(SubmitTask.this, "Please Choose Leave Type", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }

//                else  if (TextUtils.isEmpty(mytime)) {
//                    time.setError("This Field is required");
//                    cancel = true;
//                }
//
                else if (TextUtils.isEmpty(mydescription)) {
                    description.setError("This Field is required");
                    cancel = true;
                } else {

                    getcurrent_locationin();
                    submitTask();

                }
            }
        });


    }

    private void getcurrent_locationin() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SubmitTask.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
//            Toast.makeText(getApplicationContext(),"You need have granted permission",Toast.LENGTH_SHORT).show();
            gps = new GPSTracker(SubmitTask.this);
            // Check if GPS enabled
            if (gps.canGetLocation()) {
                latitude = gps.getLatitude();
                longitude = gps.getLongitude();
//                Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                try {
                    Geocoder geo = new Geocoder(SubmitTask.this.getApplicationContext(), Locale.getDefault());
                    List<Address> addresses = geo.getFromLocation(latitude, longitude, 1);
                    if (addresses.isEmpty()) {
//                        customeraddress.setText("Waiting for Location");
                    } else {
                        if (addresses.size() > 0) {
                            getLocation = addresses.get(0).getFeatureName() + ", " + addresses.get(0).getLocality() + ", " + addresses.get(0).getAdminArea() + ", " + addresses.get(0).getCountryName();
                            strLat = String.valueOf(addresses.get(0).getLatitude());
                            strLong = String.valueOf(addresses.get(0).getLongitude());


                            //                            customeraddress.setText(maddress);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace(); // getFromLocation() may sometimes fail
                }
            } else {
                gps.showSettingsAlert();
            }
        }
    }

    private void submitTask() {


        progress.show();
        Ion.with(this)
                .load("POST", ApiURL.userTask)
                .setHeader("token", "zsd16xzv3jsytnp87tk7ygv73k8zmr0ekh6ly7mxaeyeh46oe8")
                .setBodyParameter("user_id", userid)
                .setBodyParameter("date", mydate)
//                .setBodyParameter("time", mytime)
                .setBodyParameter("description", mydescription)
                .setBodyParameter("location", getLocation)
                .setBodyParameter("latitude", strLat)
                .setBodyParameter("longitude", strLong)
                .setBodyParameter("category", myrole)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        try {
                            JSONObject reader = new JSONObject(result);
                            String success = reader.getString("success");
                            String message = reader.getString("message");
                            Toast.makeText(SubmitTask.this, "" + message, Toast.LENGTH_SHORT).show();

                            if (success.equals("true")) {
                                progress.dismiss();

                                Intent intent = new Intent(SubmitTask.this, Tasks.class);
                                startActivity(intent);
                                SubmitTask.this.finish();
                                // JSONObject jsonObject1 = reader.getJSONObject("data");

                            } else {
                                progress.dismiss();
                                Toast.makeText(SubmitTask.this, "" + message, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e1) {
                            e1.printStackTrace();
                            progress.dismiss();
                            Toast.makeText(SubmitTask.this, "" + e1, Toast.LENGTH_SHORT).show();

                        }
                    }

                });

//        networkCall.NetworkAPICall(ApiURL.user_task, true);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void back(View view) {
        super.onBackPressed();

    }


}