package com.crm.akscrm;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.crm.akscrm.utill.ApiURL;
import com.crm.akscrm.utill.GPSTracker;
import com.crm.akscrm.utill.NetworkCall;
import com.crm.akscrm.utill.Progress;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class AddMember extends AppCompatActivity implements NetworkCall.MyNetworkCallBack{

    Spinner sp1;
    String[] addMember;
    Progress progress;
    GPSTracker gps;
    double latitude, longitude;
    EditText fullname, org,gst,add1,ad2,city,state,pincode, phone, email, confPass, location, dob ,addEntry;
    Boolean cancel;
    SimpleDateFormat sdf;
    final String regexStr = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";
    String key = "AIzaSyAG5yWZ4F-wBcOn6I1ci-PUl-kgt012Zm4";
    String myrole, myfullname, myorg,mygst,myadd1,myad2,mycity,mystate,mypincode, myphone, myemail, mylocation, mydob;
    int clicklocation;
    private Geocoder geocoder;
    private final int REQUEST_PLACE_ADDRESS = 40;
RelativeLayout add;
    SharedPreferences mSharedPreference;
    NetworkCall networkCall;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_member);
        progress = new Progress(AddMember.this);
        networkCall = new NetworkCall(AddMember.this, AddMember.this);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mylocation = (mSharedPreference.getString("location", ""));
        email = findViewById(R.id.emailEDT);
        location = findViewById(R.id.locationEDT);
        phone = findViewById(R.id.mobileEDT);
        dob = findViewById(R.id.dobEDT);
        fullname = findViewById(R.id.fullnameEDT);
        org = findViewById(R.id.nameofOrgEDT);
        gst = findViewById(R.id.gstinEDT);
        add1 = findViewById(R.id.add1EDT);
        ad2 = findViewById(R.id.add2EDT);
        state = findViewById(R.id.stateEDT);
        city = findViewById(R.id.cityEDT);
        pincode = findViewById(R.id.pinEDT);

        dob.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(
                        AddMember.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker,
                                          int selectedyear, int selectedmonth,
                                          int selectedday) {

                        mcurrentDate.set(Calendar.YEAR, selectedyear);
                        mcurrentDate.set(Calendar.MONTH, selectedmonth);
                        mcurrentDate.set(Calendar.DAY_OF_MONTH,
                                selectedday);
                        sdf = new SimpleDateFormat(
                                getResources().getString(
                                        R.string.datecardformat),
                                Locale.US);

                        dob.setText(sdf.format(mcurrentDate
                                .getTime()));
                    }
                }, mYear, mMonth, mDay);
                mydob = dob.toString().trim();
//                mydob = sdf.format(mcurrentDate
//                        .getTime());

                mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker.show();

            }
        });

        sp1 = (Spinner) findViewById(R.id.gender_spinner);
        addMember = getResources().getStringArray(R.array.Member);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, addMember);
        sp1.setAdapter(adapter);


        sp1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                myrole = String.valueOf(addMember[i]);
//                if (myrole.equals("Others")) {
//
//                    addEntry.setVisibility(View.VISIBLE);
//

//                    String medit =  addEntry.getText().toString();
//                    myrole=medit;


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        add= findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                myfullname = fullname.getText().toString().trim();
                myphone = phone.getText().toString().trim();
                myorg = org.getText().toString().trim();
                mygst = gst.getText().toString().trim();
                myadd1 = add1.getText().toString().trim();
                myad2 = ad2.getText().toString().trim();
                mystate = state.getText().toString().trim();
                mycity = city.getText().toString().trim();
                mypincode = pincode.getText().toString().trim();

                myemail = email.getText().toString().trim();
                mydob = dob.getText().toString().trim();

                mylocation = location.getText().toString().trim();

                fullname.setError(null);
                phone.setError(null);
                email.setError(null);
                dob.setError(null);

                org.setError(null);
                gst.setError(null);
                add1.setError(null);
                ad2.setError(null);
                state.setError(null);
                city.setError(null);
                pincode.setError(null);
                location.setError(null);
                if (myrole.equals("Choose Role")) {
                    Toast.makeText(AddMember.this, "Please Choose Role", Toast.LENGTH_SHORT).show();
                    cancel = true;
                } else if (TextUtils.isEmpty(myfullname)) {
                    fullname.setError("Your Full Name is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(myemail)) {
                    email.setError("Re-enter Password is required");
                    cancel = true;
                } else if (!myemail.matches(ApiURL.emailRegex)) {
                    email.setError("Please Enter a valid Email");
                    cancel = true;
                } else if (TextUtils.isEmpty(mylocation)) {
                    location.setError("Location is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(mydob)) {
                    dob.setError("Date of Birth is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(myphone)) {
                    phone.setError("Phone number is required");
                    cancel = true;
                } else if (!myphone.matches(regexStr)) {
                    phone.setError("Please Enter Valid Phone no.");
                    cancel = true;
                }else if (TextUtils.isEmpty(myorg)) {
                    org.setError("This Field is required..");
                    cancel = true;
                }else if (TextUtils.isEmpty(mygst)) {
                    gst.setError("This Field is required..");
                    cancel = true;
                }else if (TextUtils.isEmpty(myadd1)) {
                    add1.setError("This Field is required..");
                    cancel = true;
                }else if (TextUtils.isEmpty(myad2)) {
                    ad2.setError("This Field is required..");
                    cancel = true;
                }else if (TextUtils.isEmpty(mycity)) {
                    city.setError("This Field is required..");
                    cancel = true;
                }else if (TextUtils.isEmpty(mystate)) {
                    state.setError("This Field is required..");
                    cancel = true;
                }else if (TextUtils.isEmpty(mypincode)) {
                    pincode.setError("This Field is required..");
                    cancel = true;
                }
//                else if (!confPass.equals(password)) {
//                    confPass.setError("Password does'nt match");
//                    cancel = true;
//                }
                else {

                    signUpMobile();


                }

            }


        });

//        Places.initialize(AddMember.this, key);
//        location.setFocusable(false);
//        location.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////              mloadingpointedt=loadingpointedt.getText().toString();
//                clicklocation = 1;
//
//                List<Place.Field> fieldList = Arrays.asList(Place.Field.ADDRESS, Place.Field.LAT_LNG, Place.Field.NAME);
//                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, Arrays.asList(Place.Field.ADDRESS_COMPONENTS, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)).build(AddMember.this);
//                startActivityForResult(intent, REQUEST_PLACE_ADDRESS);
//
////                Intent intent= new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY,fieldList).build(getContext());
////                startActivityForResult(intent,100);
//
//            }
//        });
//


    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (clicklocation) {
            case 1:
                if (requestCode == REQUEST_PLACE_ADDRESS && resultCode == Activity.RESULT_OK) {
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    try {
                        List<Address> addresses;
                        geocoder = new Geocoder(AddMember.this, Locale.getDefault());
                        try {
                            addresses = geocoder.getFromLocation(place.getLatLng().latitude, place.getLatLng().longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                            String address1 = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                            String address2 = addresses.get(0).getAddressLine(1); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                            String city = addresses.get(0).getLocality();
                            String state = addresses.get(0).getAdminArea();
                            String country = addresses.get(0).getCountryName();
                            String postalCode = addresses.get(0).getPostalCode();

                            Log.e("Address1: ", "" + address1);
                            Log.e("Address2: ", "" + address2);
                            Log.e("AddressCity: ", "" + city);
                            Log.e("AddressState: ", "" + state);
                            Log.e("AddressCountry: ", "" + country);
                            Log.e("AddressPostal: ", "" + postalCode);
                            Log.e("AddressLatitude: ", "" + place.getLatLng().latitude);
                            Log.e("AddressLongitude: ", "" + place.getLatLng().longitude);

                            mylocation = addresses.get(0).getLocality();
                            location.setText(mylocation);


                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        //setMarker(latLng);
                    }
                }
                break;

        }

//        if(requestCode==100 && resultCode==RESULT_OK){
//            Place place=Autocomplete.getPlaceFromIntent(data);
//            loadingpointedt.setText(place.getAddress());
//        }else if (resultCode== AutocompleteActivity.RESULT_ERROR){
//            Status status =Autocomplete.getStatusFromIntent(data);
//            Toast.makeText(getContext(), status.getStatusMessage(), Toast.LENGTH_SHORT).show();
//
//        }
    }

    public void back(View view) {

        super.onBackPressed();
    }
    private void signUpMobile() {

        networkCall.NetworkAPICall(ApiURL.addMember, true);

    }
    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.addMember:
                ion = (Builders.Any.B) Ion.with(AddMember.this)
                        .load("POST", ApiURL.addMember)
                        .setHeader("token", "zsd16xzv3jsytnp87tk7ygv73k8zmr0ekh6ly7mxaeyeh46oe8")
                        .setBodyParameter("name", myfullname)
                        .setBodyParameter("mobile", myphone)
                        .setBodyParameter("email", myemail)
                        .setBodyParameter("role", myrole)
                        .setBodyParameter("dob", mydob)
                        .setBodyParameter("location", mylocation)
                        .setBodyParameter("gstNumber", mygst)
                        .setBodyParameter("organization", myorg)
                        .setBodyParameter("address1", myadd1)
                        .setBodyParameter("address2", myad2)
                        .setBodyParameter("city", mycity)
                        .setBodyParameter("state", mystate)
                        .setBodyParameter("pincode", mypincode)

                ;
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.addMember:

                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");

                    if (succes.equals("true")) {



                        Toast.makeText(this, "Vendor added Successfully", Toast.LENGTH_SHORT).show();


                        Intent intent = new Intent(AddMember.this, Member.class);
                        startActivity(intent);
                        finish();


                    } else {

                        String status_fail = jsonObject.getString("success");
                        if (status_fail.equals("false")) {


//                            Toast.makeText(Register.this, msg.toString(), Toast.LENGTH_SHORT).show();

                        }

                    }
                } catch (JSONException e1) {


                    Toast.makeText(AddMember.this, jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();

                }
                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        Toast.makeText(AddMember.this, jsonstring+"msg".toString(), Toast.LENGTH_SHORT).show();

    }

    public void location(View view) {
//        location.setText(mylocation);


        currentLocation();

    }

    private void currentLocation() {

        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(AddMember.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
//            Toast.makeText(getApplicationContext(),"You need have granted permission",Toast.LENGTH_SHORT).show();
            gps = new GPSTracker(AddMember.this);
            // Check if GPS enabled
            if (gps.canGetLocation()) {
                latitude = gps.getLatitude();
                longitude = gps.getLongitude();
//                Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                try {
                    Geocoder geo = new Geocoder(AddMember.this.getApplicationContext(), Locale.getDefault());
                    List<Address> addresses = geo.getFromLocation(latitude, longitude, 1);
                    if (addresses.isEmpty()) {
//                        customeraddress.setText("Waiting for Location");
                    } else {
                        if (addresses.size() > 0) {
                            mylocation = addresses.get(0).getFeatureName() + ", " + addresses.get(0).getLocality() + ", " + addresses.get(0).getAdminArea() + ", " + addresses.get(0).getCountryName();


                            location.setText(mylocation);

                            SharedPreferences.Editor editor = mSharedPreference.edit();
                            editor.putString("location", mylocation);
                            editor.apply();


//                            customeraddress.setText(maddress);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace(); // getFromLocation() may sometimes fail
                }
            } else {
                gps.showSettingsAlert();
            }
        }

    }
}