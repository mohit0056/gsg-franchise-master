package com.crm.akscrm;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crm.akscrm.utill.ApiURL;
import com.crm.akscrm.utill.NetworkCall;
import com.crm.akscrm.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class RequestLeave extends AppCompatActivity implements NetworkCall.MyNetworkCallBack {

RelativeLayout choosehalf;
LinearLayout chooseperiod,chooseperi;
    Spinner sp1, sp2;


    ProgressBar simpleProgressBar;
    String[] leave, leavetype;
    EditText description, date, dateFrom, dateTo,datehalf;
    RelativeLayout submit;
    String userid, mydescription, mydate,mydatehalf, mydateFrom, mydateTo, myrole1, myrole2, mytotal, mycomplete, myremain;
    final String regexStr = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";
    TextView hide, hide2;
    Boolean cancel;
    SimpleDateFormat sdf;
    DatePickerDialog mDatePicker;
    Progress progress;

    TextView total, complete, remain;
    SharedPreferences mSharedPreference;
    NetworkCall networkCall;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_leave);

        progress = new Progress(RequestLeave.this);
        networkCall = new NetworkCall(RequestLeave.this, RequestLeave.this);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        userid = (mSharedPreference.getString("id", ""));
//        mytotal = (mSharedPreference.getString("unpaid_leave", ""));
//        mycomplete = (mSharedPreference.getString("unpaid_completed", ""));
//        myremain = (mSharedPreference.getString("unpaid_remaining", ""));
        date = findViewById(R.id.date);
        datehalf = findViewById(R.id.datehalf);
        simpleProgressBar = findViewById(R.id.simpleProgressBar);


        total = findViewById(R.id.total);
        chooseperiod = findViewById(R.id.choosePeriod);
        choosehalf = findViewById(R.id.choosehalf);
        chooseperi = findViewById(R.id.chooseperi);

       // total.setText(mytotal);
        complete = findViewById(R.id.complete);
        //complete.setText(mycomplete);

        remain = findViewById(R.id.remain);
        //remain.setText(myremain);


        date.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                mDatePicker = new DatePickerDialog(
                        RequestLeave.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker,
                                          int selectedyear, int selectedmonth,
                                          int selectedday) {

                        mcurrentDate.set(Calendar.YEAR, selectedyear);
                        mcurrentDate.set(Calendar.MONTH, selectedmonth);
                        mcurrentDate.set(Calendar.DAY_OF_MONTH,
                                selectedday);
                        sdf = new SimpleDateFormat(
                                getResources().getString(
                                        R.string.datecardformat),
                                Locale.US);

                        date.setText(sdf.format(mcurrentDate
                                .getTime()));
                    }
                }, mYear, mMonth, mDay);
                mydate = date.toString().trim();
//                mydob = sdf.format(mcurrentDate
//                        .getTime());

                mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker.show();

            }
        });

        datehalf.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();

                // initialising the layout

                final int day = calendar.get(Calendar.DAY_OF_MONTH);
                final int year = calendar.get(Calendar.YEAR);
                final int month = calendar.get(Calendar.MONTH);

                // initialising the datepickerdialog
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    mDatePicker = new DatePickerDialog(RequestLeave.this);
                    mDatePicker = new DatePickerDialog(RequestLeave.this, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(android.widget.DatePicker view, int year, int month, int dayOfMonth) {
                            // adding the selected date in the edittext
                            datehalf.setText(dayOfMonth + "/" + (month + 1) + "/" + year);
                            mydatehalf = datehalf.toString().trim();
                        }
                    }, year, month, day);

                    // set maximum date to be selected as today
                    mDatePicker.getDatePicker().setMinDate(calendar.getTimeInMillis());

                    // show the dialog
                    mDatePicker.show();
                }
                }
        });



        dateFrom = findViewById(R.id.dateFrom);
        dateFrom.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                final Calendar calendar = Calendar.getInstance();

                // initialising the layout

                final int day = calendar.get(Calendar.DAY_OF_MONTH);
                final int year = calendar.get(Calendar.YEAR);
                final int month = calendar.get(Calendar.MONTH);

                // initialising the datepickerdialog
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    mDatePicker = new DatePickerDialog(RequestLeave.this);
                    mDatePicker = new DatePickerDialog(RequestLeave.this, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(android.widget.DatePicker view, int year, int month, int dayOfMonth) {
                            // adding the selected date in the edittext
                            dateFrom.setText(dayOfMonth + "/" + (month + 1) + "/" + year);

                            mydateFrom = dateFrom.toString().trim();
                        }
                    }, year, month, day);

                    // set maximum date to be selected as today
                    mDatePicker.getDatePicker().setMinDate(calendar.getTimeInMillis());

                    // show the dialog
                    mDatePicker.show();
                }

            }
        });

        dateTo = findViewById(R.id.dateTo);
        dateTo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();

                // initialising the layout

                final int day = calendar.get(Calendar.DAY_OF_MONTH);
                final int year = calendar.get(Calendar.YEAR);
                final int month = calendar.get(Calendar.MONTH);

                // initialising the datepickerdialog
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    mDatePicker = new DatePickerDialog(RequestLeave.this);
                    mDatePicker = new DatePickerDialog(RequestLeave.this, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(android.widget.DatePicker view, int year, int month, int dayOfMonth) {
                            // adding the selected date in the edittext
                            dateTo.setText(dayOfMonth + "/" + (month + 1) + "/" + year);
                            mydateTo = dateTo.toString().trim();
                        }
                    }, year, month, day);

                    // set maximum date to be selected as today
                    mDatePicker.getDatePicker().setMinDate(calendar.getTimeInMillis());

                    // show the dialog
                    mDatePicker.show();
                }
            }
        });
        description = findViewById(R.id.description);
        sp1 = (Spinner) findViewById(R.id.gender_spinner);
        leavetype = getResources().getStringArray(R.array.leavesType);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, leavetype);
        sp1.setAdapter(adapter);


        sp1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                myrole1 = String.valueOf(leavetype[i]);

                if(myrole1.equals("Half day")){

                    choosehalf.setVisibility(View.VISIBLE);
                    chooseperiod.setVisibility(View.GONE);
                    chooseperi.setVisibility(View.GONE);
                }else{

                    choosehalf.setVisibility(View.GONE);
                    chooseperiod.setVisibility(View.VISIBLE);
                    chooseperi.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        sp2 = (Spinner) findViewById(R.id.cetegory_spinner);
        // sp2.setPrompt("Choose Leave Type");
        leave = getResources().getStringArray(R.array.leaves);

        final ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, R.layout.spinner_item, leave);
        sp2.setAdapter(adapter2);


        sp2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                myrole2 = String.valueOf(leave[i]);


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        submit = findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mydescription = description.getText().toString().trim();
                //  mydate = date.getText().toString().trim();
                mydateFrom = dateFrom.getText().toString().trim();
                mydateTo = dateTo.getText().toString().trim();


                description.setError(null);
                // date.setError(null);
//                dateFrom.setError(null);
//                dateTo.setError(null);

//                if (TextUtils.isEmpty(mydate)) {
//                    date.setError("This field is required");
//                    cancel = true;
//                }
                if (myrole1.equals("Choose Duration")) {
                    Toast.makeText(RequestLeave.this, "Please Choose Duration", Toast.LENGTH_SHORT).show();
                    cancel = true;
                } else if (myrole2.equals("Choose Leave Type")) {
                    Toast.makeText(RequestLeave.this, "Please Choose Leave Type", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }

//                else if (TextUtils.isEmpty(mydateFrom)) {
//                    dateFrom.setError("This field is required");
//                    cancel = true;
//                } else if (TextUtils.isEmpty(mydateTo)) {
//                    dateTo.setError("This field is required");
//                    cancel = true;
//                }else if (TextUtils.isEmpty(mydatehalf)) {
//                    datehalf.setError("This field is required");
//                    cancel = true;
//                }


                else if (TextUtils.isEmpty(mydescription)) {
                    description.setError("This field is required");
                    cancel = true;
                } else {

                    submitData();


                }

            }


        });

showData();
    }

    private void showData() {

        networkCall.NetworkAPICall(ApiURL.getLeaveData, true);

    }

    private void submitData() {

        networkCall.NetworkAPICall(ApiURL.userLeavePost, true);


    }


    public void leave(View view) {
        Intent intent = new Intent(RequestLeave.this, leaves.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void back(View view) {
        super.onBackPressed();

    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.userLeavePost:
                ion = (Builders.Any.B) Ion.with(RequestLeave.this)
                        .load("POST", ApiURL.userLeavePost)
                        .setHeader("token", "zsd16xzv3jsytnp87tk7ygv73k8zmr0ekh6ly7mxaeyeh46oe8")
                        .setBodyParameter("user_id", userid)
                        //.setBodyParameter("date", mydate)
                        .setBodyParameter("categoryone", myrole1)
                        .setBodyParameter("categorytwo", myrole2)
                        .setBodyParameter("dateFrom", mydateFrom)
                        .setBodyParameter("date", mydatehalf)
                        .setBodyParameter("dateTo", mydateTo)
                        .setBodyParameter("description", mydescription)

                ;
                break;

            case ApiURL.getLeaveData:
                ion = (Builders.Any.B) Ion.with(RequestLeave.this)
                        .load("POST", ApiURL.getLeaveData)
                        .setHeader("token", "zsd16xzv3jsytnp87tk7ygv73k8zmr0ekh6ly7mxaeyeh46oe8")
                        .setBodyParameter("user_id", userid)


                ;
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.userLeavePost:

                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");

                    if (succes.equals("true")) {
                        Toast.makeText(this, "Successfully Submitted", Toast.LENGTH_SHORT).show();


                        Intent intent = new Intent(RequestLeave.this, LeaveRequest.class);
                        startActivity(intent);
                        finish();


                    } else {

                        String status_fail = jsonObject.getString("success");
                        if (status_fail.equals("false")) {


                            Toast.makeText(RequestLeave.this, msg.toString(), Toast.LENGTH_SHORT).show();

                        }

                    }
                } catch (JSONException e1) {


                    Toast.makeText(RequestLeave.this, jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();

                }
                break;
            case ApiURL.getLeaveData:
            try {
                JSONObject jsonObjecta = new JSONObject(jsonstring.toString());
                String succes = jsonObjecta.getString("success");
                String msg = jsonObjecta.getString("message");

                if (succes.equals("true")) {

                    // JSONArray Earning = jsonObject.getJSONArray("Earningleave");
                    JSONArray unpaid = jsonObjecta.getJSONArray("unpaid");
//                        JSONArray sick = jsonObject.getJSONArray("SickLeave");
//                        JSONArray Paid = jsonObject.getJSONArray("Paidleave");

//                        for (int i = 0; i < Earning.length(); i++) {
//                            JSONObject Earning2 = Earning.getJSONObject(0);
//
//                            earningleaves = Earning2.getString("earningleaves");
//                            earningcompleted = Earning2.getString("earning completed");
//                            earningremaining = Earning2.getString("earning remaining");
//
//                            totalLeave1.setText(earningleaves);
//                            complete_leave_txt.setText(earningcompleted+" "+"Leave");
//                            remaining_txt.setText(earningremaining+" "+"Leave");
//                        }

                    for (int i = 0; i < unpaid.length(); i++) {
                        JSONObject json_unpaid = unpaid.getJSONObject(0);

                        mytotal = json_unpaid.getString("unpaid_leave");
                        mycomplete = json_unpaid.getString("unpaid_completed");
                        myremain = json_unpaid.getString("unpaid_remaining");





                        total.setText(mytotal+" "+"Leaves Per Year");
                        complete.setText(mycomplete+" "+"Leave");
                        remain.setText(myremain+" "+"Leave");
                        simpleProgressBar.setMax((int) Double.parseDouble(mytotal));
                        simpleProgressBar.setProgress((int) Double.parseDouble(mycomplete));
                    }
//                        for (int i = 0; i < sick.length(); i++) {
//                            JSONObject json_completed = sick.getJSONObject(0);
//
//                            str_total_sick = json_completed.getString("sick leaves");
//                            str_complete_sick = json_completed.getString("sick completed");
//                            str_remaining_sick = json_completed.getString("sick remaining");
//
//                            total_sick.setText(str_total_sick);
//                            complete_sick.setText(str_complete_sick+" "+"Leave");
//                            remaining_sick.setText(str_remaining_sick+" "+"Leave");
//                        }
//                        for (int i = 0; i < Paid.length(); i++) {
//                            JSONObject json_paid = Paid.getJSONObject(0);
//
//                            str_total_paid = json_paid.getString("paid leaves");
//                            str_complete_paid = json_paid.getString("paid completed");
//                            str_remain_paid = json_paid.getString("paid remaining");
//
//                            total_paid.setText(str_total_paid);
//                            complete_paid.setText(str_complete_paid+" "+"Leave");
//                            remain_paid.setText(str_remain_paid+" "+"Leave");
//                        }
                } else {

                    String status_fail = jsonObjecta.getString("success");
                    if (status_fail.equals("false")) {
                        Toast.makeText(RequestLeave.this, msg.toString(), Toast.LENGTH_SHORT).show();

                    }

                }
            } catch (JSONException e1) {


                Toast.makeText(RequestLeave.this, jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();

            }
            break;

        }

    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        Toast.makeText(RequestLeave.this, "" + jsonstring, Toast.LENGTH_SHORT).show();

    }
}