package com.gsg.franchise.models;

public class DynmicFranchiseModel {

    private String id;

    private String franchiseCost;

    private String processCharges;

    private String franchiseFee;

    private String marginMoney;

    private String subsidy;

    private String servicetype;

    private String type;

    private String image;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    public void setFranchiseCost(String franchiseCost) {
        this.franchiseCost = franchiseCost;
    }

    public String getFranchiseCost() {
        return this.franchiseCost;
    }

    public void setProcessCharges(String processCharges) {
        this.processCharges = processCharges;
    }

    public String getProcessCharges() {
        return this.processCharges;
    }

    public void setFranchiseFee(String franchiseFee) {
        this.franchiseFee = franchiseFee;
    }

    public String getFranchiseFee() {
        return this.franchiseFee;
    }

    public void setMarginMoney(String marginMoney) {
        this.marginMoney = marginMoney;
    }

    public String getMarginMoney() {
        return this.marginMoney;
    }

    public void setSubsidy(String subsidy) {
        this.subsidy = subsidy;
    }

    public String getSubsidy() {
        return this.subsidy;
    }

    public void setServicetype(String servicetype) {
        this.servicetype = servicetype;
    }

    public String getServicetype() {
        return this.servicetype;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return this.image;
    }
}