package com.gsg.franchise.models;

public class StaticFranchiseModel {

    private String id;

    private String franchiseCost;

    private String processCharges;

    private String franchiseFee;

    private String marginMoney;

    private String timeline;

    private String servicetype;

    private String image;

    public void setId(String id){
        this.id = id;
    }
    public String getId(){
        return this.id;
    }
    public void setFranchiseCost(String franchiseCost){
        this.franchiseCost = franchiseCost;
    }
    public String getFranchiseCost(){
        return this.franchiseCost;
    }
    public void setProcessCharges(String processCharges){
        this.processCharges = processCharges;
    }
    public String getProcessCharges(){
        return this.processCharges;
    }
    public void setFranchiseFee(String franchiseFee){
        this.franchiseFee = franchiseFee;
    }
    public String getFranchiseFee(){
        return this.franchiseFee;
    }
    public void setMarginMoney(String marginMoney){
        this.marginMoney = marginMoney;
    }
    public String getMarginMoney(){
        return this.marginMoney;
    }
    public void setTimeline(String timeline){
        this.timeline = timeline;
    }
    public String getTimeline(){
        return this.timeline;
    }
    public void setServicetype(String servicetype){
        this.servicetype = servicetype;
    }
    public String getServicetype(){
        return this.servicetype;
    }
    public void setImage(String image){
        this.image = image;
    }
    public String getImage(){
        return this.image;
    }
}