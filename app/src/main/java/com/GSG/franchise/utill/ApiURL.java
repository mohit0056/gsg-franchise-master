package com.gsg.franchise.utill;

public class ApiURL {
    public final static String main_url = "http://inmortaltech.com/GospeedyFrachise-APIs/";

    public final static String user_login = main_url+"login";
    public final static String send_otp = main_url+"forgotPassword";
    public final static String enter_OTP = main_url+"login";

    public final static String change_password = main_url+"newPassword";
    public final static String user_register = main_url+"customerRegisteration";
    public final static String webinar_form = main_url+"webinarRegister";
    public final static String franchise_form = main_url+"franchiseRegister";
    public final static String franchise_form_documents = main_url+"FranchiseNextform";
    public final static String Dynamic_Franchise = main_url+"dynamicFranchise";
    public final static String Dynamic_Franchise_details = main_url+"getdynamicDetails";
    public final static String Static_Franchise = main_url+"staticFranchise";
    public final static String Distributor_Franchise = main_url+"distributorFranchise";



    public final static String Kyc_Form = main_url+"kycFristForm";
    public final static String Kyc_Form_document = main_url+"kycsecondForm";
    public final static String transaction = main_url+"transction";
    public final static String creditScore = main_url+"Creditscore";
    public final static String getkyc = main_url+"getkyc";
    public final static String kycApplicationDetails = main_url+"kycApplicationDetails";
    public final static String kycPersonalDetails = main_url+"kycPersonalDetails";
    public final static String kycBussinessActivity = main_url+"kycBussinessActivity";
    public final static String kycBankDetailsSubmit = main_url+"kycBankDetailsSubmit";
    public final static String kycBankDetails2Submit = main_url+"kycBankDetails2Submit";
    public final static String kycDocument = main_url+"kycDocument";
    public final static String kycPreferred = main_url+"kycPreferred";
    public final static String cashfreeurl ="https://api.cashfree.com/api/v2/cftoken/order";



    public static String getOrderID() {
        long timeMS = System.currentTimeMillis() / 10000;
        String strOrder = "OR" + timeMS;
        return strOrder;
    }

    public static final String CASHFREE_mid = "1238985488c7705b84c2369a50898321";
    public static final String CASHFREE_SECRETID ="e97053ab1a25abbb4fbbb22497ed5976f3ea9a86";

    public final static String token = "zsd16xzv3jsytnp87tk7ygv73k8zmr0ekh6ly7mxaeyeh46oe8";
    public final static String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
            "[a-zA-Z0-9_+&*-]+)*@" +
            "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
            "A-Z]{2,7}$";
}
