package com.gsg.franchise.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.gsg.franchise.R;
import com.gsg.franchise.utill.ApiURL;
import com.gsg.franchise.utill.NetworkCall;
import com.gsg.franchise.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class KYC_details_1 extends AppCompatActivity implements NetworkCall.MyNetworkCallBack{
       TextInputLayout calendar_layout;
    SimpleDateFormat sdf;
    TextInputEditText calendar_edit;
    String[] business_product;
    String str_business="";
    Spinner business_spinner;
    Button btn_liner;
    TextInputEditText nm_enterprise,office_address,category_application,address_shop,telephone_no,email_edit,mobile_edit,pan_no,enterprise_pan,enterprise_name,constitution,other_constitution,gst_no,state_,district,branch;
    String  form="",str_nm_enterprise="",str_office_address="",str_category_application="",str_address_shop="",str_telephone_no="",str_email_edit="",str_mobile_edit="",str_pan_no="",str_enterprise_pan="",str_enterprise_name="",str_constitution="",str_other_constitution="",str_gst_no="",str_state_="",str_district="",str_branch="",str_calendar_edit="";
    Boolean cancel;
    NetworkCall networkCall;
    Progress progress;
    String  userid="";
    SharedPreferences mSharedPreference;
    public final static String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+"[a-zA-Z0-9_+&*-]+)*@" +"(?:[a-zA-Z0-9-]+\\.)+[a-z" +"A-Z]{2,7}$";
    final String regexStr = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";
    String Pan_regex = "[A-Z]{5}[0-9]{4}[A-Z]{1}";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_k_y_c_details_1);

        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        userid =mSharedPreference.getString("id", "");
        str_nm_enterprise =mSharedPreference.getString("str_nm_enterprise", "");
        str_office_address =mSharedPreference.getString("str_office_address", "");
        str_category_application =mSharedPreference.getString("str_category_application", "");
        str_address_shop =mSharedPreference.getString("str_address_shop", "");
        str_business =mSharedPreference.getString("str_business", "");
        str_telephone_no =mSharedPreference.getString("str_telephone_no", "");
        str_email_edit =mSharedPreference.getString("str_email_edit", "");
        str_pan_no =mSharedPreference.getString("str_pan_no", "");
        str_enterprise_pan =mSharedPreference.getString("str_enterprise_pan", "");
        str_constitution =mSharedPreference.getString("str_constitution", "");
        str_other_constitution =mSharedPreference.getString("str_other_constitution", "");
        str_gst_no =mSharedPreference.getString("str_gst_no", "");
        str_state_ =mSharedPreference.getString("str_state_", "");
        str_district =mSharedPreference.getString("str_district", "");
        str_branch =mSharedPreference.getString("str_branch", "");
        str_calendar_edit =mSharedPreference.getString("str_calendar_edit", "");
//        str_enterprise_name =mSharedPreference.getString("str_enterprise_name", "");
        str_mobile_edit =mSharedPreference.getString("str_mobile_edit", "");

        progress = new Progress(KYC_details_1.this);
        networkCall = new NetworkCall(KYC_details_1.this, KYC_details_1.this);

        calendar_layout=findViewById(R.id.calendar_layout);
        calendar_edit=findViewById(R.id.calendar_edit);
        business_spinner=findViewById(R.id.business_spinner);
        nm_enterprise=findViewById(R.id.nm_enterprise);
        office_address=findViewById(R.id.office_address);
        category_application=findViewById(R.id.category_application);
        address_shop=findViewById(R.id.address_shop);
        telephone_no=findViewById(R.id.telephone_no);
        email_edit=findViewById(R.id.email_edit);
        mobile_edit=findViewById(R.id.mobile_edit);
        pan_no=findViewById(R.id.pan_no);
        enterprise_pan=findViewById(R.id.enterprise_pan);
//        enterprise_name=findViewById(R.id.enterprise_name);
        constitution=findViewById(R.id.constitution);
        other_constitution=findViewById(R.id.other_constitution);
        gst_no=findViewById(R.id.gst_no);
        state_=findViewById(R.id.state_);
        district=findViewById(R.id.district);
        branch=findViewById(R.id.branch);
        btn_liner=findViewById(R.id.btn_liner);

//        form = getIntent().getStringExtra("form");
//        if (form.equals("kyc")){
//            str_nm_enterprise = getIntent().getStringExtra("str_nm_enterprise");
//            str_office_address = getIntent().getStringExtra("str_office_address");
//            str_category_application = getIntent().getStringExtra("str_category_application");
//            str_address_shop = getIntent().getStringExtra("str_address_shop");
//            str_business = getIntent().getStringExtra("str_business");
//            str_telephone_no = getIntent().getStringExtra("str_telephone_no");
//            str_email_edit = getIntent().getStringExtra("str_email_edit");
//            str_pan_no = getIntent().getStringExtra("str_pan_no");
//            str_enterprise_pan = getIntent().getStringExtra("str_enterprise_pan");
//            str_constitution = getIntent().getStringExtra("str_constitution");
//            str_other_constitution = getIntent().getStringExtra("str_other_constitution");
//            str_gst_no = getIntent().getStringExtra("str_gst_no");
//            str_state_ = getIntent().getStringExtra("str_state_");
//            str_district = getIntent().getStringExtra("str_district");
//            str_branch = getIntent().getStringExtra("str_branch");
//            form = getIntent().getStringExtra("form");


          if (!str_nm_enterprise.equals("")){
              nm_enterprise.setText(str_nm_enterprise);
          }
        if (!str_office_address.equals("")){
            office_address.setText(str_office_address);
        }
        if (!str_mobile_edit.equals("")){
            mobile_edit.setText(str_mobile_edit);
        }
        if (!str_category_application.equals("")){
            category_application.setText(str_category_application);
        } if (!str_address_shop.equals("")){
            address_shop.setText(str_address_shop);
        } if (!str_telephone_no.equals("")){
            telephone_no.setText(str_telephone_no);
        } if (!str_email_edit.equals("")){
            email_edit.setText(str_email_edit);
        } if (!str_pan_no.equals("")){
            pan_no.setText(str_pan_no);
        } if (!str_enterprise_pan.equals("")){
            enterprise_pan.setText(str_enterprise_pan);
        } if (!str_constitution.equals("")){
            constitution.setText(str_constitution);
        }
        if (!str_other_constitution.equals("")){
            other_constitution.setText(str_other_constitution);
        }
        if (!str_gst_no.equals("")){
            gst_no.setText(str_gst_no);
        }
        if (!str_state_.equals("")){
            state_.setText(str_state_);
        }
        if (!str_branch.equals("")){
            branch.setText(str_branch);
        }
        if (!str_calendar_edit.equals("")){
            calendar_edit.setText(str_calendar_edit);
        }
        if (!str_district.equals("")){
            district.setText(str_district);
        }
//        if (!str_enterprise_name.equals("")){
//            enterprise_name.setText(str_enterprise_name);
//        }
//        }


        calendar_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(KYC_details_1.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker,int selectedyear, int selectedmonth,int selectedday) {
                        mcurrentDate.set(Calendar.YEAR, selectedyear);
                        mcurrentDate.set(Calendar.MONTH, selectedmonth);
                        mcurrentDate.set(Calendar.DAY_OF_MONTH,
                                selectedday);
                        sdf = new SimpleDateFormat(
                                getResources().getString(
                                        R.string.datecardformat),
                                Locale.US);
                        str_calendar_edit=sdf.format(mcurrentDate.getTime());

                        calendar_edit.setText(str_calendar_edit);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker.show();

            }
        });
        business_product = getResources().getStringArray(R.array.business_per);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, business_product);
        business_spinner.setAdapter(adapter);
        business_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                str_business = String.valueOf(business_product[i]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        btn_liner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_nm_enterprise = nm_enterprise.getText().toString().trim();
                str_office_address = office_address.getText().toString().trim();
                str_category_application =category_application.getText().toString().trim();
                str_address_shop =address_shop.getText().toString().trim();
                str_telephone_no =telephone_no.getText().toString().trim();
                str_email_edit =email_edit.getText().toString().trim();
                str_pan_no =pan_no.getText().toString().trim();
                str_enterprise_pan =enterprise_pan.getText().toString().trim();
//                str_enterprise_name =enterprise_name.getText().toString().trim();
                str_constitution =constitution.getText().toString().trim();
                str_other_constitution =other_constitution.getText().toString().trim();
                str_gst_no =gst_no.getText().toString().trim();
                str_calendar_edit =calendar_edit.getText().toString().trim();
                str_state_ =state_.getText().toString().trim();
                str_district =district.getText().toString().trim();
                str_branch =branch.getText().toString().trim();
                str_mobile_edit =mobile_edit.getText().toString().trim();
                nm_enterprise.setError(null);
                office_address.setError(null);
                category_application.setError(null);
                address_shop.setError(null);
                telephone_no.setError(null);
                email_edit.setError(null);
                pan_no.setError(null);
              //  enterprise_pan.setError(null);
//                enterprise_name.setError(null);
                constitution.setError(null);
                other_constitution.setError(null);
             //   gst_no.setError(null);
                calendar_edit.setError(null);
                state_.setError(null);
                district.setError(null);
                branch.setError(null);
                mobile_edit.setError(null);

                if (TextUtils.isEmpty(str_nm_enterprise)) {
                    Toast.makeText(KYC_details_1.this, "Name of the Enterprise", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (TextUtils.isEmpty(str_office_address)) {
                    Toast.makeText(KYC_details_1.this, "Office Address is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (TextUtils.isEmpty(str_category_application)) {
                    Toast.makeText(KYC_details_1.this, "Category", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (TextUtils.isEmpty(str_address_shop)) {
                    Toast.makeText(KYC_details_1.this, "Address of Factory/Shop is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (str_business.equals("Select")) {
                    Toast.makeText(KYC_details_1.this, "Select Business Premises", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (TextUtils.isEmpty(str_telephone_no)) {
                    Toast.makeText(KYC_details_1.this, "Telephone no is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (TextUtils.isEmpty(str_email_edit)) {
                    Toast.makeText(KYC_details_1.this, "Email is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                } else if(!str_email_edit.matches(emailRegex)) {
                    email_edit.setError("Please Enter Valid E-mail");
                    cancel = true;
                }else if (TextUtils.isEmpty(str_mobile_edit)) {
                    Toast.makeText(KYC_details_1.this, "Mobile no. is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if(!str_mobile_edit.matches(regexStr)) {
                  mobile_edit.setError("Please Enter Valid Phone Number");
                    cancel = true;
                }else if (TextUtils.isEmpty(str_pan_no)) {
                    Toast.makeText(KYC_details_1.this, "Pan no is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if(!str_pan_no.matches(Pan_regex)) {
                    pan_no.setError("Please Enter Valid PAN Number");
                    cancel = true;
                }

//                else if (TextUtils.isEmpty(str_enterprise_pan)) {
//                    Toast.makeText(KYC_details_1.this, "Name of the Enterprise PAN", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }
//                else if (TextUtils.isEmpty(str_enterprise_name)) {
//                    Toast.makeText(KYC_details_1.this, "Constitution is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }
                else if (TextUtils.isEmpty(str_constitution)) {
                    Toast.makeText(KYC_details_1.this, "Constitution is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (TextUtils.isEmpty(str_other_constitution)) {
                    Toast.makeText(KYC_details_1.this, "Other Constitution is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }

//                else if (TextUtils.isEmpty(str_gst_no)) {
//                    Toast.makeText(KYC_details_1.this, "Udyog Aadhaar No / GST Registration No. is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }

                else if (TextUtils.isEmpty(str_calendar_edit)) {
                    Toast.makeText(KYC_details_1.this, "Date of Establishment/Incorporation is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (TextUtils.isEmpty(str_state_)) {
                    Toast.makeText(KYC_details_1.this, "State is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (TextUtils.isEmpty(str_district)) {
                    Toast.makeText(KYC_details_1.this, "City where Loan is required/District is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (TextUtils.isEmpty(str_branch)) {
                    Toast.makeText(KYC_details_1.this, "Branch where loan is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else {

                    nm_enterprise.setText(str_nm_enterprise);
                    office_address.setText(str_office_address);
                    category_application.setText(str_category_application);
                    address_shop.setText(str_address_shop);
                    telephone_no.setText(str_telephone_no);
                    email_edit.setText(str_email_edit);
                    pan_no.setText(str_pan_no);
                    enterprise_pan.setText(str_enterprise_pan);
                    constitution.setText(str_constitution);
                    other_constitution.setText(str_other_constitution);
                    gst_no.setText(str_gst_no);
                    state_.setText(str_state_);
                    district.setText(str_district);
                    branch.setText(str_branch);
                    telephone_no.setText(str_telephone_no);
                    mobile_edit.setText(str_mobile_edit);
                    calendar_edit.setText(str_calendar_edit);

                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("str_nm_enterprise", str_nm_enterprise);
                    editor.putString("str_office_address", str_office_address);
                    editor.putString("str_category_application", str_category_application);
                    editor.putString("str_address_shop", str_address_shop);
                    editor.putString("str_business", str_business);
                    editor.putString("str_email_edit", str_email_edit);
                    editor.putString("str_mobile_edit", str_mobile_edit);
                    editor.putString("str_pan_no", str_pan_no);
                    editor.putString("str_enterprise_pan", str_enterprise_pan);
                    editor.putString("str_telephone_no", str_telephone_no);
                    editor.putString("str_constitution", str_constitution);
                    editor.putString("str_other_constitution", str_other_constitution);
                    editor.putString("str_gst_no", str_gst_no);
                    editor.putString("str_calendar_edit", str_calendar_edit);
                    editor.putString("str_district", str_district);
                    editor.putString("str_branch", str_branch);
                    editor.putString("str_state_", str_state_);
                    editor.commit();
                    kyc_Application_Details();
                }
            }
        });



    }

    private void kyc_Application_Details() {
        networkCall.NetworkAPICall(ApiURL.kycApplicationDetails, true);
    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.kycApplicationDetails:
                ion = (Builders.Any.B) Ion.with(KYC_details_1.this)
                        .load("POST", ApiURL.kycApplicationDetails)
                        .setHeader("token", ApiURL.token)
                        .setBodyParameter("user_id", userid)
                        .setBodyParameter("nameEnterprise", str_nm_enterprise)
                        .setBodyParameter("cateogry", str_category_application)
                        .setBodyParameter("addressfactory/shop", str_address_shop)
                        .setBodyParameter("BussinessPremises", str_business)
                        .setBodyParameter("TelephoneNumber", str_telephone_no)
                        .setBodyParameter("email", str_email_edit)
                        .setBodyParameter("mobile", str_mobile_edit)
                        .setBodyParameter("state", str_state_)
                        .setBodyParameter("panNumber", str_pan_no)
                        .setBodyParameter("enterprisePan", str_enterprise_pan)
//                        .setBodyParameter("enterprisename", str_enterprise_name)
                        .setBodyParameter("consitution", str_constitution)
                        .setBodyParameter("otherConsitution", str_other_constitution)
                        .setBodyParameter("UdogadharNo/gstRegisterationNo", str_gst_no)
                        .setBodyParameter("Establishment/incorporation",str_calendar_edit)
                        .setBodyParameter("officeAddress", str_office_address)
                        .setBodyParameter("City/District", str_district)
                        .setBodyParameter("branch", str_branch);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.kycApplicationDetails:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String status = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");

                    if (status.equals("true")) {

                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        str_nm_enterprise=jsonObject1.getString("nameEnterprise");
                        str_office_address=jsonObject1.getString("officeAddress");
                        str_category_application=jsonObject1.getString("cateogry");
                        str_address_shop=jsonObject1.getString("addressfactory/shop");
                        str_business=jsonObject1.getString("BussinessPremises");
                        str_email_edit=jsonObject1.getString("email");
                        str_mobile_edit= jsonObject1.getString("mobile");
                        str_pan_no=jsonObject1.getString("pan");
                        str_enterprise_pan=jsonObject1.getString("enterprisePan");
//                        str_enterprise_name=jsonObject1.getString("enterprisename");
                        str_constitution=jsonObject1.getString("consitution");
                        str_other_constitution=jsonObject1.getString("otherConsitution");
                        str_gst_no=jsonObject1.getString("UdogadharNo/gstRegisterationNo");
                        str_calendar_edit=jsonObject1.getString("Establishment/incorporation");
                        str_district=jsonObject1.getString("City/District");
                       str_branch=jsonObject1.getString("branch");
                       str_state_=jsonObject1.getString("state");
                       Intent intent=new Intent(KYC_details_1.this,KYC_personal_Details.class);
                        startActivity(intent);
                        finish();

                    } else {


                        String fail_status = jsonObject.getString("success");

                        if (fail_status.equals("false")) {


                            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                        }
                        progress.dismiss();
                    }
                } catch (JSONException e1) {


                    Toast.makeText(KYC_details_1.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        Toast.makeText(KYC_details_1.this, jsonstring, Toast.LENGTH_SHORT).show();

    }
}