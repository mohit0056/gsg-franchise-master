package com.gsg.franchise.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.gsg.franchise.R;
import com.gsg.franchise.utill.ApiURL;
import com.gsg.franchise.utill.NetworkCall;
import com.gsg.franchise.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONException;
import org.json.JSONObject;

public class WebinarForm extends AppCompatActivity implements NetworkCall.MyNetworkCallBack {
    EditText name, email, address, gender, phone, occupaion;
    RelativeLayout submitForm;
    Boolean isLogin;
    Boolean cancel;
    public final static String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
            "[a-zA-Z0-9_+&*-]+)*@" +
            "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
            "A-Z]{2,7}$";
    static final String pref_name = "GSG";
    Progress progress;
    NetworkCall networkCall;
    Spinner sp1;
    String[] product;
    String s1;
    SharedPreferences mSharedPreference;

    String myname, myphone, myemail, myaddress, mygender, user_id, myoccupation;

    final String regexStr = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webinar_form);

        mSharedPreference = getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        user_id = (mSharedPreference.getString("id", ""));
        progress = new Progress(WebinarForm.this);
        networkCall = new NetworkCall(WebinarForm.this, WebinarForm.this);
        sp1 = (Spinner) findViewById(R.id.gender_spinner);
        address = (EditText) findViewById(R.id.addressEDT);
        email = (EditText) findViewById(R.id.emailEDT);
        phone = (EditText) findViewById(R.id.phnumEDT);
        name = (EditText) findViewById(R.id.nameEDT);

        product = getResources().getStringArray(R.array.Gender);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, product);
        sp1.setAdapter(adapter);


        sp1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mygender = String.valueOf(product[i]);
                if (i == 3) {


                } else {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        occupaion = findViewById(R.id.occupationEDT);
        submitForm = findViewById(R.id.submitForm);
        submitForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myname = name.getText().toString().trim();
                myemail = email.getText().toString().trim();
                myphone = phone.getText().toString().trim();
                myaddress = address.getText().toString().trim();
//                mygender = gender.getText().toString().trim();
                myoccupation = occupaion.getText().toString().trim();

                name.setError(null);
                email.setError(null);
                phone.setError(null);
//                gender.setError(null);
                address.setError(null);
                occupaion.setError(null);

                if (TextUtils.isEmpty(myname)) {
                    name.setError("Your Name is required");
                    cancel = true;
                } else if (mygender.equals("Select Gender")) {

                    Toast.makeText(WebinarForm.this, "Please Select Gender", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(myemail)) {
                    email.setError("Your Email is required");
                    cancel = true;
                } else if (!myemail.matches(emailRegex)) {
                    email.setError("Please Enter Valid Email");
                    cancel = true;
                } else if (TextUtils.isEmpty(myphone)) {
                    phone.setError("Phone number is required");
                    cancel = true;
                }

//                else if (TextUtils.isEmpty(mygender)) {
//                    gender.setError("Gender is required");
//                    cancel = true;
//                }

                else if (!myphone.matches(regexStr)) {
                    phone.setError("Please Enter Valid Phone no.");
                    cancel = true;
                } else if (TextUtils.isEmpty(myaddress)) {
                    address.setError("Address is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(myoccupation)) {
                    occupaion.setError("Occupation is required");
                    cancel = true;
                } else {

                    SubmitForm();


                }
            }
        });


    }

    private void SubmitForm() {
        networkCall.NetworkAPICall(ApiURL.webinar_form, true);

    }

    public void back(View view) {
        super.onBackPressed();
    }

//    public void submitAlert(View view) {
//
//        Dialog dialogg = new Dialog(WebinarForm.this, R.style.PauseDialog);
//        dialogg.setContentView(R.layout.webinar_submit_layout);
//        RelativeLayout home = dialogg.findViewById(R.id.home);
//        home.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent home = new Intent(WebinarForm.this, MainActivity.class);
//                startActivity(home);
//                dialogg.dismiss();
//                WebinarForm.this.finish();
//            }
//        });

//        TextView ifInterested = dialogg.findViewById(R.id.ifInterested);
//        ifInterested.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });

//
//        Window window = dialogg.getWindow();
//        WindowManager.LayoutParams wlp = window.getAttributes();
//        wlp.gravity = Gravity.CENTER;
//        dialogg.show();
//    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.webinar_form:
                ion = (Builders.Any.B) Ion.with(WebinarForm.this)
                        .load("POST", ApiURL.webinar_form)
                        .setHeader("token", ApiURL.token)
                        .setBodyParameter("username", myname)
                        .setBodyParameter("email", myemail)
                        .setBodyParameter("user_id", user_id)
                        .setBodyParameter("phone", myphone)
                        .setBodyParameter("address", myaddress)
                        .setBodyParameter("gender", mygender)
                        .setBodyParameter("occupation", myoccupation);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.webinar_form:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");

                    if (succes.equals("true")) {


//                        Intent intent = new Intent(SignUp.this, MainActivity.class);
//                        startActivity(intent);
//                        finish();

                        Dialog dialogg = new Dialog(WebinarForm.this, R.style.PauseDialog);
                        dialogg.setContentView(R.layout.webinar_submit_layout);
                        RelativeLayout home = dialogg.findViewById(R.id.home);
                        home.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent home = new Intent(WebinarForm.this, MainActivity.class);
                                startActivity(home);
                                dialogg.dismiss();
                                WebinarForm.this.finish();
                            }
                        });

                        Window window = dialogg.getWindow();
                        WindowManager.LayoutParams wlp = window.getAttributes();
                        wlp.gravity = Gravity.CENTER;
                        dialogg.show();

                    } else {

                        String status_fail = jsonObject.getString("success");
                        if (status_fail.equals("false")) {
                            Toast.makeText(WebinarForm.this, msg.toString(), Toast.LENGTH_SHORT).show();
                            progress.dismiss();
                        }

                    }
                } catch (JSONException e1) {


                    Toast.makeText(WebinarForm.this, jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();
                }


                break;
        }

    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }
}