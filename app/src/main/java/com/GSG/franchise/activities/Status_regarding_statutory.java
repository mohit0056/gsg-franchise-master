package com.gsg.franchise.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;

import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.provider.Settings;
import android.provider.SyncStateContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.Network;
import com.google.android.material.textfield.TextInputEditText;
import com.gsg.franchise.R;
import com.gsg.franchise.utill.ApiURL;
import com.gsg.franchise.utill.NetworkCall;
import com.gsg.franchise.utill.Progress;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import static com.gsg.franchise.utill.Util.context;

public class Status_regarding_statutory extends AppCompatActivity implements NetworkCall.MyNetworkCallBack {
    Button next_btn;
    ImageView back_image;
    Boolean cancel;
    TextInputEditText remark,id_proof,id_proof_no,address_proof,address_proof_no,identity_Address_Business_doc,six_month_pdf,sales_tax_doc,cash_credit_bs,sales_up_date,project_report_doc,moa_partnership_pdf,third_party_guarantee_doc,other_additional_documents;
    int clickImage;
    final int RESULT_LOAD_IMG = 1000;
    RadioButton establishment_yes,establishment_no,establishment_na,provisional_yes,provisional_no,provisional_na,drug_license_yes,drug_license_no,drug_license_na,Sales_Tax_yes,Sales_Tax_no,Sales_Tax_na,income_Tax_yes,income_Tax_no,income_Tax_na,
    remaining_outstanding_yes,remaining_outstanding_no,remaining_outstanding_na;
    String str_establishment,str_provisional,str_drug_license,str_sales_Tax,str_income_Tax,str_remaining_outstanding,str_remark,str_id_proof,str_address_proof,str_identity_Address,new_str_six_month_pdf,str_six_month_pdf,str_sales_tax_doc,str_cash_credit_bs,str_sales_up_date,str_project_report_doc,str_moa_partnership_pdf,str_third_party_guarantee_doc,str_other_additional_documents;
    private  final  int REQ=1;
    NetworkCall networkCall;
    Progress progress;
    String  userid="";
    private static final int PICK_FROM_GALLERY = 1;
    private static final int  REQUEST_CODE_DOC= 1212;
    Uri contentURI1;
    File six_month_File,moa_partnership_pdf_file,file_other_additional_documents;
    SharedPreferences mSharedPreference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_regarding_statutory);

        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        userid =mSharedPreference.getString("id", "");
        progress = new Progress(Status_regarding_statutory.this);
        networkCall = new NetworkCall(Status_regarding_statutory.this, Status_regarding_statutory.this);


        str_establishment =mSharedPreference.getString("str_establishment", "");
        str_provisional =mSharedPreference.getString("str_provisional", "");
        str_drug_license =mSharedPreference.getString("str_drug_license", "");
        str_sales_Tax =mSharedPreference.getString("str_sales_Tax", "");
        str_income_Tax =mSharedPreference.getString("str_income_Tax", "");
        str_remaining_outstanding =mSharedPreference.getString("str_remaining_outstanding", "");
        str_remark =mSharedPreference.getString("str_remark", "");
        str_id_proof =mSharedPreference.getString("str_id_proof", "");
        str_address_proof =mSharedPreference.getString("str_address_proof", "");
        str_identity_Address =mSharedPreference.getString("str_identity_Address", "");
        str_six_month_pdf =mSharedPreference.getString("str_six_month_pdf", "");
        str_cash_credit_bs =mSharedPreference.getString("str_cash_credit_bs", "");
        str_sales_up_date =mSharedPreference.getString("str_sales_up_date", "");
        str_sales_tax_doc =mSharedPreference.getString("str_sales_tax_doc", "");
        str_moa_partnership_pdf =mSharedPreference.getString("str_moa_partnership_pdf", "");
        str_project_report_doc =mSharedPreference.getString("str_project_report_doc", "");
        str_other_additional_documents =mSharedPreference.getString("str_other_additional_documents", "");

        back_image=findViewById(R.id.back_image);
        six_month_pdf=findViewById(R.id.six_month_pdf);
        establishment_yes=findViewById(R.id.establishment_yes);
        establishment_no=findViewById(R.id.establishment_no);
        establishment_na=findViewById(R.id.establishment_na);
        provisional_yes=findViewById(R.id.provisional_yes);
        provisional_no=findViewById(R.id.provisional_no);
        provisional_na=findViewById(R.id.provisional_na);
        drug_license_yes=findViewById(R.id.drug_license_yes);
        drug_license_no=findViewById(R.id.drug_license_no);
        drug_license_na=findViewById(R.id.drug_license_na);
        Sales_Tax_yes=findViewById(R.id.Sales_Tax_yes);
        Sales_Tax_no=findViewById(R.id.Sales_Tax_no);
        Sales_Tax_na=findViewById(R.id.Sales_Tax_na);
        income_Tax_yes=findViewById(R.id.income_Tax_yes);
        income_Tax_no=findViewById(R.id.income_Tax_no);
        income_Tax_na=findViewById(R.id.income_Tax_na);
        remaining_outstanding_yes=findViewById(R.id.remaining_outstanding_yes);
        remaining_outstanding_no=findViewById(R.id.remaining_outstanding_no);
        remaining_outstanding_na=findViewById(R.id.remaining_outstanding_na);
        remark=findViewById(R.id.remark);
        id_proof=findViewById(R.id.id_proof);
        id_proof_no=findViewById(R.id.id_proof_no);
        address_proof=findViewById(R.id.address_proof);
        address_proof_no=findViewById(R.id.address_proof_no);
        identity_Address_Business_doc=findViewById(R.id.identity_Address_Business_doc);
        sales_tax_doc=findViewById(R.id.sales_tax_doc);
        cash_credit_bs=findViewById(R.id.cash_credit_bs);
        sales_up_date=findViewById(R.id.sales_up_date);
        project_report_doc=findViewById(R.id.project_report_doc);
        moa_partnership_pdf=findViewById(R.id.moa_partnership_pdf);
        third_party_guarantee_doc=findViewById(R.id.third_party_guarantee_doc);
        other_additional_documents=findViewById(R.id.other_additional_documents);
        next_btn=findViewById(R.id.next_btn);

        if (str_establishment.equals("Yes")){
            establishment_yes.setChecked(true);
            establishment_no.setChecked(false);
            establishment_na.setChecked(false);
        }else if (str_establishment.equals("No")){
            establishment_yes.setChecked(false);
            establishment_no.setChecked(true);
            establishment_na.setChecked(false);
        }else if (str_establishment.equals("N.A")){
            establishment_yes.setChecked(false);
            establishment_no.setChecked(false);
            establishment_na.setChecked(true);
        }

        if (str_provisional.equals("Yes")){
            provisional_yes.setChecked(true);
            provisional_no.setChecked(false);
            provisional_na.setChecked(false);
        }else if (str_provisional.equals("No")){
            provisional_yes.setChecked(false);
            provisional_no.setChecked(true);
            provisional_na.setChecked(false);
        }else if (str_provisional.equals("N.A")){
            provisional_yes.setChecked(false);
            provisional_no.setChecked(false);
            provisional_na.setChecked(true);
        }


        if (str_drug_license.equals("Yes")){
            drug_license_yes.setChecked(true);
            drug_license_no.setChecked(false);
            drug_license_na.setChecked(false);
        }else if (str_drug_license.equals("No")){
            drug_license_yes.setChecked(false);
            drug_license_no.setChecked(true);
            drug_license_na.setChecked(false);
        }else if (str_drug_license.equals("N.A")){
            drug_license_yes.setChecked(false);
            drug_license_no.setChecked(false);
            drug_license_na.setChecked(true);
        }


        if (str_sales_Tax.equals("Yes")){
            Sales_Tax_yes.setChecked(true);
            Sales_Tax_no.setChecked(false);
            Sales_Tax_na.setChecked(false);
        }else if (str_sales_Tax.equals("No")){
            Sales_Tax_yes.setChecked(false);
            Sales_Tax_no.setChecked(true);
            Sales_Tax_na.setChecked(false);
        }else if (str_sales_Tax.equals("N.A")){
            Sales_Tax_yes.setChecked(false);
            Sales_Tax_no.setChecked(false);
            Sales_Tax_na.setChecked(true);
        }


        if (str_income_Tax.equals("Yes")){
            income_Tax_yes.setChecked(true);
            income_Tax_no.setChecked(false);
            income_Tax_na.setChecked(false);
        }else if (str_income_Tax.equals("No")){
            income_Tax_yes.setChecked(false);
            income_Tax_no.setChecked(true);
            income_Tax_na.setChecked(false);
        }else if (str_income_Tax.equals("N.A")){
            income_Tax_yes.setChecked(false);
            income_Tax_no.setChecked(false);
            income_Tax_na.setChecked(true);
        }

        if (str_remaining_outstanding.equals("Yes")){
            remaining_outstanding_yes.setChecked(true);
            remaining_outstanding_no.setChecked(false);
            remaining_outstanding_na.setChecked(false);
        }else if (str_remaining_outstanding.equals("No")){
            remaining_outstanding_yes.setChecked(false);
            remaining_outstanding_no.setChecked(true);
            remaining_outstanding_na.setChecked(false);
        }else if (str_remaining_outstanding.equals("N.A")){
            remaining_outstanding_yes.setChecked(false);
            remaining_outstanding_no.setChecked(false);
            remaining_outstanding_na.setChecked(true);
        }

        if (!str_remark.equals("")){
            remark.setText(str_remark);
        }
        if (!str_id_proof.equals("")){
            id_proof.setText(str_id_proof);
        }
        if (!str_address_proof.equals("")){
            address_proof.setText(str_address_proof);
        }

        if (!str_identity_Address.equals("")){
            identity_Address_Business_doc.setText(str_identity_Address);
        }
        if (!str_six_month_pdf.equals("")){
            six_month_pdf.setText(str_six_month_pdf);
        }
        if (!str_cash_credit_bs.equals("")){
            cash_credit_bs.setText(str_cash_credit_bs);
        }
        if (!str_sales_up_date.equals("")){
            sales_up_date.setText(str_sales_up_date);
        }
        if (!str_sales_tax_doc.equals("")){
            sales_tax_doc.setText(str_sales_tax_doc);
        }
        if (!str_moa_partnership_pdf.equals("")){
            moa_partnership_pdf.setText(str_moa_partnership_pdf);
        }
        if (!str_project_report_doc.equals("")){
            project_report_doc.setText(str_project_report_doc);
        }
        if (!str_other_additional_documents.equals("")){
            other_additional_documents.setText(str_other_additional_documents);
        }







        establishment_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    str_establishment="Yes";
                    establishment_no.setChecked(false);
                    establishment_na.setChecked(false);

                }
            }
        });
        establishment_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    str_establishment="No";
                    establishment_yes.setChecked(false);
                    establishment_na.setChecked(false);

                }
            }
        });
        establishment_na.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    str_establishment="N.A";
                    establishment_no.setChecked(false);
                    establishment_yes.setChecked(false);

                }
            }
        });

        provisional_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    str_provisional="Yes";
                    provisional_no.setChecked(false);
                    provisional_na.setChecked(false);

                }
            }
        });
        provisional_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    str_provisional="No";
                    provisional_yes.setChecked(false);
                    provisional_na.setChecked(false);

                }
            }
        });
        provisional_na.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    str_provisional="N.A";
                    provisional_no.setChecked(false);
                    provisional_yes.setChecked(false);

                }
            }
        });

        drug_license_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    str_drug_license="Yes";
                    drug_license_no.setChecked(false);
                    drug_license_na.setChecked(false);

                }
            }
        });

        drug_license_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    str_drug_license="No";
                    drug_license_yes.setChecked(false);
                    drug_license_na.setChecked(false);

                }
            }
        });
        drug_license_na.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    str_drug_license="N.A";
                    drug_license_no.setChecked(false);
                    drug_license_yes.setChecked(false);

                }
            }
        });

        Sales_Tax_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    str_sales_Tax="Yes";
                    Sales_Tax_no.setChecked(false);
                    Sales_Tax_na.setChecked(false);

                }
            }
        });

        Sales_Tax_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    str_sales_Tax="No";
                    Sales_Tax_na.setChecked(false);
                    Sales_Tax_yes.setChecked(false);

                }
            }
        });

        Sales_Tax_na.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    str_sales_Tax="N.A";
                    Sales_Tax_no.setChecked(false);
                    Sales_Tax_yes.setChecked(false);

                }
            }
        });

        income_Tax_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    str_income_Tax="Yes";
                    income_Tax_no.setChecked(false);
                    income_Tax_na.setChecked(false);

                }
            }
        });


        income_Tax_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    str_income_Tax="No";
                    income_Tax_yes.setChecked(false);
                    income_Tax_na.setChecked(false);

                }
            }
        });
        income_Tax_na.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    str_income_Tax="N.A";
                    income_Tax_no.setChecked(false);
                    income_Tax_yes.setChecked(false);

                }
            }
        });

        remaining_outstanding_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    str_remaining_outstanding="Yes";
                    remaining_outstanding_no.setChecked(false);
                    remaining_outstanding_na.setChecked(false);

                }
            }
        });


        remaining_outstanding_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    str_remaining_outstanding="No";
                    remaining_outstanding_yes.setChecked(false);
                    remaining_outstanding_na.setChecked(false);

                }
            }
        });
        remaining_outstanding_na.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    str_remaining_outstanding="N.A";
                    remaining_outstanding_no.setChecked(false);
                    remaining_outstanding_yes.setChecked(false);

                }
            }
        });


        id_proof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try { if (ActivityCompat.checkSelfPermission(Status_regarding_statutory.this,android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(Status_regarding_statutory.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                }else {
                    clickImage=1;
                    loadImagefromGallery();
                }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        address_proof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try { if (ActivityCompat.checkSelfPermission(Status_regarding_statutory.this,android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(Status_regarding_statutory.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                }else {
                    clickImage=2;
                    loadImagefromGallery();
                }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        identity_Address_Business_doc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try { if (ActivityCompat.checkSelfPermission(Status_regarding_statutory.this,android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(Status_regarding_statutory.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                }else {
                    clickImage=3;
                    loadImagefromGallery();
                }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        sales_tax_doc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try { if (ActivityCompat.checkSelfPermission(Status_regarding_statutory.this,android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(Status_regarding_statutory.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                }else {
                    clickImage=5;
                    loadImagefromGallery();
                }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        project_report_doc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try { if (ActivityCompat.checkSelfPermission(Status_regarding_statutory.this,android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(Status_regarding_statutory.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                }else {
                    clickImage=6;
                    loadImagefromGallery();
                }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        third_party_guarantee_doc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try { if (ActivityCompat.checkSelfPermission(Status_regarding_statutory.this,android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(Status_regarding_statutory.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                }else {
                    clickImage=8;
                    loadImagefromGallery();
                }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Status_regarding_statutory.this,Banking_details_2.class);
                startActivity(intent);
                Status_regarding_statutory.this.finish();
            }
        });

        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_remark=remark.getText().toString().trim();
//                str_id_proof=id_proof_no.getText().toString().trim();
//                str_address_proof=address_proof_no.getText().toString().trim();
                str_cash_credit_bs=cash_credit_bs.getText().toString().trim();
                str_sales_up_date=sales_up_date.getText().toString().trim();
//                remark.setError(null);
//                id_proof_no.setError(null);
//                address_proof_no.setError(null);
//                cash_credit_bs.setError(null);
//                sales_up_date.setError(null);

                if (str_establishment.equals("")) {
                    Toast.makeText(Status_regarding_statutory.this, "Registration under Shops and Establishment ACT is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (str_provisional.equals("")) {
                    Toast.makeText(Status_regarding_statutory.this, "Registration under MSME (Provisional / Final)", Toast.LENGTH_SHORT).show();
                    cancel = true;
                } else if (str_drug_license.equals("")) {
                    Toast.makeText(Status_regarding_statutory.this, "Drug License is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (str_sales_Tax.equals("")) {
                    Toast.makeText(Status_regarding_statutory.this, "Latest Sales Tax Return Filed is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (str_income_Tax.equals("")) {
                    Toast.makeText(Status_regarding_statutory.this, "Latest Income Tax Return Filed is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (str_remaining_outstanding.equals("")) {
                    Toast.makeText(Status_regarding_statutory.this, "Any other Statutory dues remaining outstanding is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (TextUtils.isEmpty(str_remark)) {
                    Toast.makeText(Status_regarding_statutory.this, "Remarks is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if(str_id_proof.equals("")){
                    Toast.makeText(Status_regarding_statutory.this, "Select ID Proof No. or Document", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if(str_address_proof.equals("")){
                    Toast.makeText(Status_regarding_statutory.this, "Select Address Proof No. or Document", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if(str_identity_Address==null){
                    Toast.makeText(Status_regarding_statutory.this, "Select Identity/Address of Business Enterprise or Statement of Accounts Document", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if(str_six_month_pdf==null){
                    Toast.makeText(Status_regarding_statutory.this, "Statement of Accounts ( Last Six Months )", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if(str_sales_tax_doc==null){
                    Toast.makeText(Status_regarding_statutory.this, "Select B/S (Last 2 yrs) with IT/Sales Tax Returns Document", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if(str_cash_credit_bs.equals("")){
                    Toast.makeText(Status_regarding_statutory.this, "Select Projected B/S (Cash credit- 1 yr; Term loan - loan period)", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if(str_sales_up_date.equals("")){
                    Toast.makeText(Status_regarding_statutory.this, "Select Sales up to the Date of Application", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if(str_project_report_doc==null){
                    Toast.makeText(Status_regarding_statutory.this, "Select Proposed Project Report document", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if(str_moa_partnership_pdf==null){
                    Toast.makeText(Status_regarding_statutory.this, "Select MOA & AOA of Co./Partnership deed document", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if(str_third_party_guarantee_doc==null){
                    Toast.makeText(Status_regarding_statutory.this, "Third Party Guarantee statement document", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if(str_other_additional_documents==null){
                    Toast.makeText(Status_regarding_statutory.this, "Other Additional document", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else {

                   if (str_establishment.equals("Yes")){
                       establishment_yes.setChecked(true);
                       establishment_no.setChecked(false);
                       establishment_na.setChecked(false);
                   }else if (str_establishment.equals("No")){
                       establishment_yes.setChecked(false);
                       establishment_no.setChecked(true);
                       establishment_na.setChecked(false);
                   }else if (str_establishment.equals("N.A")){
                       establishment_yes.setChecked(false);
                       establishment_no.setChecked(false);
                       establishment_na.setChecked(true);
                   }

                    if (str_provisional.equals("Yes")){
                        provisional_yes.setChecked(true);
                        provisional_no.setChecked(false);
                        provisional_na.setChecked(false);
                    }else if (str_provisional.equals("No")){
                        provisional_yes.setChecked(false);
                        provisional_no.setChecked(true);
                        provisional_na.setChecked(false);
                    }else if (str_provisional.equals("N.A")){
                        provisional_yes.setChecked(false);
                        provisional_no.setChecked(false);
                        provisional_na.setChecked(true);
                    }


                    if (str_drug_license.equals("Yes")){
                        drug_license_yes.setChecked(true);
                        drug_license_no.setChecked(false);
                        drug_license_na.setChecked(false);
                    }else if (str_drug_license.equals("No")){
                        drug_license_yes.setChecked(false);
                        drug_license_no.setChecked(true);
                        drug_license_na.setChecked(false);
                    }else if (str_drug_license.equals("N.A")){
                        drug_license_yes.setChecked(false);
                        drug_license_no.setChecked(false);
                        drug_license_na.setChecked(true);
                    }


                    if (str_sales_Tax.equals("Yes")){
                        Sales_Tax_yes.setChecked(true);
                        Sales_Tax_no.setChecked(false);
                        Sales_Tax_na.setChecked(false);
                    }else if (str_sales_Tax.equals("No")){
                        Sales_Tax_yes.setChecked(false);
                        Sales_Tax_no.setChecked(true);
                        Sales_Tax_na.setChecked(false);
                    }else if (str_sales_Tax.equals("N.A")){
                        Sales_Tax_yes.setChecked(false);
                        Sales_Tax_no.setChecked(false);
                        Sales_Tax_na.setChecked(true);
                    }


                    if (str_income_Tax.equals("Yes")){
                        income_Tax_yes.setChecked(true);
                        income_Tax_no.setChecked(false);
                        income_Tax_na.setChecked(false);
                    }else if (str_income_Tax.equals("No")){
                        income_Tax_yes.setChecked(false);
                        income_Tax_no.setChecked(true);
                        income_Tax_na.setChecked(false);
                    }else if (str_income_Tax.equals("N.A")){
                        income_Tax_yes.setChecked(false);
                        income_Tax_no.setChecked(false);
                        income_Tax_na.setChecked(true);
                    }

                    if (str_remaining_outstanding.equals("Yes")){
                        remaining_outstanding_yes.setChecked(true);
                        remaining_outstanding_no.setChecked(false);
                        remaining_outstanding_na.setChecked(false);
                    }else if (str_remaining_outstanding.equals("No")){
                        remaining_outstanding_yes.setChecked(false);
                        remaining_outstanding_no.setChecked(true);
                        remaining_outstanding_na.setChecked(false);
                    }else if (str_remaining_outstanding.equals("N.A")){
                        remaining_outstanding_yes.setChecked(false);
                        remaining_outstanding_no.setChecked(false);
                        remaining_outstanding_na.setChecked(true);
                    }

                    remark.setText(str_remark);
                    id_proof.setText(str_id_proof);
                    address_proof.setText(str_address_proof);
                    identity_Address_Business_doc.setText(str_identity_Address);
                    six_month_pdf.setText(str_six_month_pdf);
                    cash_credit_bs.setText(str_cash_credit_bs);
                    sales_up_date.setText(str_sales_up_date);
                    sales_tax_doc.setText(str_sales_tax_doc);
                    moa_partnership_pdf.setText(str_moa_partnership_pdf);
                    project_report_doc.setText(str_project_report_doc);
                    other_additional_documents.setText(str_other_additional_documents);

                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("str_establishment", str_establishment);
                    editor.putString("str_provisional", str_provisional);
                    editor.putString("str_drug_license", str_drug_license);
                    editor.putString("str_drug_license", str_drug_license);
                    editor.putString("str_sales_Tax", str_sales_Tax);
                    editor.putString("str_income_Tax", str_income_Tax);
                    editor.putString("str_remaining_outstanding", str_remaining_outstanding);
                    editor.putString("str_remark", str_remark);
                    editor.putString("str_id_proof", str_id_proof);
                    editor.putString("str_address_proof", str_address_proof);
                    editor.putString("str_identity_Address", str_identity_Address);
                    editor.putString("str_six_month_pdf", str_six_month_pdf);
                    editor.putString("str_cash_credit_bs", str_cash_credit_bs);
                    editor.putString("str_sales_up_date", str_sales_up_date);
                    editor.putString("str_sales_tax_doc", str_sales_tax_doc);
                    editor.putString("str_moa_partnership_pdf", str_moa_partnership_pdf);
                    editor.putString("str_project_report_doc", str_project_report_doc);
                    editor.putString("str_other_additional_documents", str_other_additional_documents);
                    editor.commit();
                    kyc_statutory();

                }
            }
        });
        six_month_pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                try { if (ActivityCompat.checkSelfPermission(Status_regarding_statutory.this,android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//                    ActivityCompat.requestPermissions(Status_regarding_statutory.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_DOC);
//                }else {
                    clickImage=4;


                Intent intent = new Intent();
                intent.setType("application/pdf");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select PDF"), 1);






//                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
//                        intent.setAction(Intent.ACTION_GET_CONTENT);
//                        intent.setType("application/pdf");
//                        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                        intent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
//                        startActivityForResult(intent, clickImage);

//                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Files.getContentUri());
//                    intent.setAction(Intent.ACTION_GET_CONTENT);
//                    intent.setType("application/pdf");
//                    intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                    intent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
//                    startActivityForResult(intent, clickImage);
//                }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
            }
        });
        moa_partnership_pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try { if (ActivityCompat.checkSelfPermission(Status_regarding_statutory.this,android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(Status_regarding_statutory.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_DOC);
                }else {
                    clickImage=7;
//                    Intent intent = new Intent();
//                    intent.setAction(Intent.ACTION_GET_CONTENT);
//                    intent.setType("application/pdf");
//                    intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                    intent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
//                    startActivityForResult(intent, clickImage);

                    Intent intent = new Intent();
                    intent.setType("application/pdf");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select PDF"), 1);

                }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        other_additional_documents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try { if (ActivityCompat.checkSelfPermission(Status_regarding_statutory.this,android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(Status_regarding_statutory.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_DOC);
                }else {
                    clickImage=9;
//                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                    intent.setAction(Intent.ACTION_GET_CONTENT);
//                    intent.setType("application/pdf");
//                    intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                    intent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
//                    startActivityForResult(intent, clickImage);

                    Intent intent = new Intent();
                    intent.setType("application/pdf");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select PDF"), 1);

                }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

    }

    private void loadImagefromGallery() {
        if (clickImage == 1) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        }else if (clickImage == 2) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        }else if (clickImage == 3) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        }else if (clickImage == 5) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        }else if (clickImage == 6) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        }else if (clickImage == 8) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (clickImage) {
            case 1:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI1 = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            str_id_proof = getRealPathFromURI(selectedImage);
                            File File_data_1 = new File(str_id_proof);
                            String id_proof_name=File_data_1.getName();
                            id_proof.setText(id_proof_name);
                            id_proof_no.setText("");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
            case 2:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI1 = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            str_address_proof = getRealPathFromURI(selectedImage);
                            File File_data_1 = new File(str_address_proof);
                            String address_proof_name=File_data_1.getName();
                            address_proof.setText(address_proof_name);
                            address_proof_no.setText("");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
            case 3:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI1 = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            str_identity_Address = getRealPathFromURI(selectedImage);
                            File File_data_1 = new File(str_identity_Address);
                            String identity_name=File_data_1.getName();
                            identity_Address_Business_doc.setText(identity_name);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;

            case 4:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = data.getData();
                    String uriString = uri.toString();
                    six_month_File = new File(uriString);
                    String path = six_month_File.getAbsolutePath();
                    String displayName = null;

                  //  if (uriString.startsWith("file://")) {

                        displayName = six_month_File.getName();
                        str_six_month_pdf = displayName;
                        six_month_pdf.setText(str_six_month_pdf);
                    //}

//                    if (uriString.startsWith("content://")) {
//                        Cursor cursor = null;
//                        try {
//                            cursor = getApplicationContext().getContentResolver().query(uri, null, null, null, null);
//                            if (cursor != null && cursor.moveToFirst()) {
//                                displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
//                                str_six_month_pdf=displayName;
//
//
//                                six_month_pdf.setText(displayName);
//
//
//                                File dir = Environment.getExternalStorageDirectory();
//                                six_month_File = new File(dir,path+"/"+displayName);
//
//                            }
//                        } finally {
//                            cursor.close();
//                        }
//                    } else if (uriString.startsWith("file://")) {
//                        displayName = myFile.getName();
//                        // Toast.makeText ( getActivity (),"hii"+displayName,Toast.LENGTH_LONG ).show ();
//                    }

                   /* Uri uri = data.getData();
                    String uriString = uri.toString();
                     six_month_File = new File(uriString);
                     String path = six_month_File.getAbsolutePath();
                    String displayName = null;

                    if (uriString.startsWith("content://")) {
                        Cursor cursor = null;
                        try {
                            cursor = getApplicationContext().getContentResolver().query(uri, null, null, null, null);
                            if (cursor != null && cursor.moveToFirst()) {
                                displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                                str_six_month_pdf=displayName;
                                str_moa_partnership_pdf=path+"/"+displayName;
                                six_month_pdf.setText(str_six_month_pdf);

                            }
                        } finally {
                            cursor.close();
                        }
                    } else if (uriString.startsWith("file://")) {
                        displayName = six_month_File.getName();
                        str_six_month_pdf=displayName;
                        six_month_pdf.setText(str_six_month_pdf);

                    }*/

                }



                break;
            case 5:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI1 = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            str_sales_tax_doc = getRealPathFromURI(selectedImage);
                            File File_data_1 = new File(str_sales_tax_doc);
                            String sales_tax_name=File_data_1.getName();
                            sales_tax_doc.setText(sales_tax_name);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }



                    break;
                }
            case 6:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI1 = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            str_project_report_doc = getRealPathFromURI(selectedImage);
                            File File_data_1 = new File(str_project_report_doc);
                            String project_report_name=File_data_1.getName();
                            project_report_doc.setText(project_report_name);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
            case 7:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = data.getData();
                    String uriString = uri.toString();
                    moa_partnership_pdf_file = new File(uriString);
                    String path = moa_partnership_pdf_file.getAbsolutePath();
                    String displayName = null;

//                    if (uriString.startsWith("content://")) {
//                        Cursor cursor = null;
//                        try {
//                            cursor = getApplicationContext().getContentResolver().query(uri, null, null, null, null);
//                            if (cursor != null && cursor.moveToFirst()) {
//                                displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
////                                str_moa_partnership_pdf=displayName;
//
//                                str_moa_partnership_pdf=path+"/"+displayName;
//                                moa_partnership_pdf.setText(displayName);
//                            }
//                        } finally {
//                            cursor.close();
//                        }
//                    } else if (uriString.startsWith("file://")) {
                    if (uriString.startsWith("file://")) {
                        displayName = moa_partnership_pdf_file.getName();

                        str_moa_partnership_pdf=displayName;
                        moa_partnership_pdf.setText(str_moa_partnership_pdf);

                    }
//                    }
                }
                break;
            case 8:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI1 = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            str_third_party_guarantee_doc = getRealPathFromURI(selectedImage);
                            File File_data_1 = new File(str_third_party_guarantee_doc);
                            String party_guarantee_name=File_data_1.getName();
                            third_party_guarantee_doc.setText(party_guarantee_name);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
            case 9:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = data.getData();
                    String uriString = uri.toString();
                    file_other_additional_documents = new File(uriString);
                    String path = file_other_additional_documents.getAbsolutePath();

                    Log.e("path",path);

                    String displayName = null;

//                    if (uriString.startsWith("content://")) {
//                        Cursor cursor = null;
//                        try {
//                            cursor = getApplicationContext().getContentResolver().query(uri, null, null, null, null);
//                            if (cursor != null && cursor.moveToFirst()) {
//                                displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
//                                Log.e("path",path+"/"+displayName);
//                                str_other_additional_documents=path+"/"+displayName;
//                                other_additional_documents.setText(displayName);
//                            }
//                        } finally {
//                            cursor.close();
//                        }
//                    } else if (uriString.startsWith("file://")) {

                    if (uriString.startsWith("file://")) {
                        displayName = file_other_additional_documents.getName();
                        str_other_additional_documents = displayName;
                        other_additional_documents.setText(str_other_additional_documents);

                    }
//                    }
                }
                break;
        }


//        switch (requestCode) {
//            case 1212:
//                if (resultCode == RESULT_OK) {
//                    // Get the Uri of the selected file
//                    Uri uri = data.getData();
//                    String uriString = uri.toString();
//                    File myFile = new File(uriString);
//                    String path = myFile.getAbsolutePath();
//                    String displayName = null;
//
//                    if (uriString.startsWith("content://")) {
//                        Cursor cursor = null;
//                        try {
//                            cursor = getApplicationContext().getContentResolver().query(uri, null, null, null, null);
//                            if (cursor != null && cursor.moveToFirst()) {
//                                displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
//                                str_identity_Address=displayName;
//                                six_month_pdf.setText(str_identity_Address);
//                            }
//                        } finally {
//                            cursor.close();
//                        }
//                    } else if (uriString.startsWith("file://")) {
//                        displayName = myFile.getName();
//                        str_identity_Address=displayName;
//                        six_month_pdf.setText(str_identity_Address);
//                    }
//                }
//                break;
//        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public void onBackPressed() {
        Intent intent=new Intent(Status_regarding_statutory.this,Banking_details_2.class);
        startActivity(intent);
        Status_regarding_statutory.this.finish();
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }
    private void kyc_statutory() {
      //  progress.show();
       networkCall.NetworkAPICall(ApiURL.kycDocument, true);
//        Ion.with(this)
//                .load("POST", ApiURL.kycDocument)
//                .setHeader("token", ApiURL.token)
//                .setMultipartParameter("user_id", userid)
//                .setMultipartParameter("established/act", str_establishment)
//                .setMultipartParameter("provision/final", str_provisional)
//                .setMultipartParameter("drugLicense", str_drug_license)
//                .setMultipartParameter("latestsales", str_sales_Tax)
//                .setMultipartParameter("latestincome", str_income_Tax)
//                .setMultipartParameter("outstanding", str_remaining_outstanding)
//                .setMultipartParameter("remark", str_remark)
//                .setMultipartFile("document1", new File(str_id_proof))
//                .setMultipartFile("document2", new File(str_address_proof))
//                .setMultipartFile("document3", new File(str_identity_Address))
//                .setMultipartFile("pdf1", new File(str_six_month_pdf))
//                .setMultipartFile("document4", new File(str_sales_tax_doc))
//                .setMultipartParameter("projected/b/s", str_cash_credit_bs)
//                .setMultipartParameter("salesup", str_sales_up_date)
//                .setMultipartFile("document5", new File(str_project_report_doc))
//                .setMultipartFile("pdf2", new File(str_moa_partnership_pdf))
//                .setMultipartFile("document6", new File(str_third_party_guarantee_doc))
//                .setMultipartFile("pdf3", new File(str_other_additional_documents))
//                .asString()
//                .setCallback(new FutureCallback<String>() {
//                    @Override
//                    public void onCompleted(Exception e, String result) {
//
//
//
//
//                        try {
//
//                            if(result!=null){
//                                // Do you work here on success
//
//                                JSONObject reader = new JSONObject(result);
//                                JSONObject jsonObject = new JSONObject(reader.toString());
//                                String status = jsonObject.getString("success");
//                                String msg = jsonObject.getString("message");
//
//                                if (status.equals("true")) {
//                                    progress.dismiss();
//                                    Intent intent=new Intent(Status_regarding_statutory.this,Preferred_bank.class);
//                                    startActivity(intent);
//                                    Status_regarding_statutory.this.finish();
//
//                                    Toast.makeText(Status_regarding_statutory.this, "" + msg, Toast.LENGTH_SHORT).show();
//
//                                }else {
//                                    progress.dismiss();
//                                    Toast.makeText(Status_regarding_statutory.this, "" + msg, Toast.LENGTH_SHORT).show();
//
//                                }
//                            }else{
//                                // null response or Exception occur
//
//                                Toast.makeText(Status_regarding_statutory.this, "hihi", Toast.LENGTH_SHORT).show();
//                            }
//
//                        } catch (JSONException e1) {
//                            e1.printStackTrace();
//                            progress.dismiss();
//                            Toast.makeText(Status_regarding_statutory.this, "" + e1, Toast.LENGTH_SHORT).show();
//
//                        }
//                    }
//                });

    }
    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.kycDocument:
                ion = (Builders.Any.B) Ion.with(Status_regarding_statutory.this)
                        .load("POST", ApiURL.kycDocument)
                        .setHeader("token", ApiURL.token)
                        .setMultipartParameter("user_id", userid)
                        .setMultipartParameter("established/act", str_establishment)
                        .setMultipartParameter("provision/final", str_provisional)
                        .setMultipartParameter("drugLicense", str_drug_license)
                        .setMultipartParameter("latestsales", str_sales_Tax)
                        .setMultipartParameter("latestincome", str_income_Tax)
                        .setMultipartParameter("outstanding", str_remaining_outstanding)
                        .setMultipartParameter("remark", str_remark)
                        .setMultipartFile("document1", new File(str_id_proof))
                        .setMultipartFile("document2", new File(str_address_proof))
                        .setMultipartFile("document3", new File(str_identity_Address))
                        .setMultipartFile("pdf1", new File(str_six_month_pdf))
                        .setMultipartFile("document4", new File(str_sales_tax_doc))
                        .setMultipartParameter("projected/b/s", str_cash_credit_bs)
                        .setMultipartParameter("salesup", str_sales_up_date)
                        .setMultipartFile("document5", new File(str_project_report_doc))
                        .setMultipartFile("pdf2", new File(str_moa_partnership_pdf))
                        .setMultipartFile("document6", new File(str_third_party_guarantee_doc))
                        .setMultipartFile("pdf3", new File(str_other_additional_documents));

                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.kycDocument:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String status = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");

                    if (status.equals("true")) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        str_establishment=jsonObject1.getString("established/act");
                        str_provisional=jsonObject1.getString("provision/final");
                        str_drug_license=jsonObject1.getString("drugLicense");
                        str_sales_Tax=jsonObject1.getString("latestsales");
                        str_remaining_outstanding=jsonObject1.getString("outstanding");
                        str_remark=jsonObject1.getString("remark");
                        str_id_proof= jsonObject1.getString("document1");
                        str_address_proof=jsonObject1.getString("document2");
                        str_identity_Address=jsonObject1.getString("document3");
                        str_six_month_pdf=jsonObject1.getString("pdf1");
                        str_cash_credit_bs=jsonObject1.getString("projected/b/s");
                        str_sales_up_date=jsonObject1.getString("salesup");
                        str_sales_tax_doc=jsonObject1.getString("document4");
                        str_moa_partnership_pdf=jsonObject1.getString("pdf2");
                        str_project_report_doc=jsonObject1.getString("document5");
                        str_other_additional_documents=jsonObject1.getString("pdf3");


//                        nm_enterprise.setText(str_establishment);
                        remark.setText(str_remark);
                        id_proof.setText(str_id_proof);
                        address_proof.setText(str_address_proof);
                        identity_Address_Business_doc.setText(str_identity_Address);
                        six_month_pdf.setText(str_six_month_pdf);
                        cash_credit_bs.setText(str_cash_credit_bs);
                        sales_up_date.setText(str_sales_up_date);
                        sales_tax_doc.setText(str_sales_tax_doc);
                        moa_partnership_pdf.setText(str_moa_partnership_pdf);
                        project_report_doc.setText(str_project_report_doc);
                        other_additional_documents.setText(str_other_additional_documents);

                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("str_remark", str_remark);
                        editor.putString("str_id_proof", str_id_proof);
                        editor.putString("str_address_proof", str_address_proof);
                        editor.putString("str_identity_Address", str_identity_Address);
                        editor.putString("str_six_month_pdf", str_six_month_pdf);
                        editor.putString("str_cash_credit_bs", str_cash_credit_bs);
                        editor.putString("str_sales_up_date", str_sales_up_date);
                        editor.putString("str_sales_tax_doc", str_sales_tax_doc);
                        editor.putString("str_moa_partnership_pdf", str_moa_partnership_pdf);
                        editor.putString("str_project_report_doc", str_project_report_doc);
                        editor.putString("str_other_additional_documents", str_other_additional_documents);

                        editor.commit();
                        Intent intent=new Intent(Status_regarding_statutory.this,Preferred_bank.class);
                        startActivity(intent);
                        Status_regarding_statutory.this.finish();

                    } else {
                        String fail_status = jsonObject.getString("success");
                        if (fail_status.equals("false")) {
                            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                        }
                        progress.dismiss();
                    }
                } catch (JSONException e1) {
                    Toast.makeText(Status_regarding_statutory.this, "" + e1, Toast.LENGTH_SHORT).show();
                    Log.d("pdf", String.valueOf(e1));

                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        Toast.makeText(Status_regarding_statutory.this, jsonstring, Toast.LENGTH_SHORT).show();
Log.d("pdf",jsonstring);
    }
    public String getPDFPath(Uri uri){

        final String id = DocumentsContract.getDocumentId(uri);
        final Uri contentUri = ContentUris.withAppendedId(
                Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContentResolver().query(contentUri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
}