package com.gsg.franchise.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.loader.content.CursorLoader;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gsg.franchise.R;
import com.gsg.franchise.fragments.Franchise;
import com.gsg.franchise.utill.ApiURL;
import com.gsg.franchise.utill.NetworkCall;
import com.gsg.franchise.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class UploadDocument extends AppCompatActivity implements NetworkCall.MyNetworkCallBack {
    Handler handler;
    private static final int IMAGE_PICKER_SELECT = 1;
    File File_data1, File_data2, File_data3;
    private static final int PICK_FROM_GALLERY = 1;
    String filePath1 = "", filePath2 = "", filePath3 = "";
    ImageView adharback, adharfront, pancard, adharbackSmall, adharfrontSmall, pancardSmall;
    RelativeLayout cback, cfront, cpan, webview;
    final int RESULT_LOAD_IMG = 1000;
    int clickImage;
    SharedPreferences mSharedPreference;
    Progress progress;
    static final String pref_name = "GSG";
    NetworkCall networkCall;
    Uri contentURI;
    String userid, email, phone;
    TextView txtback, txtfront, txtpan, phonenum, emailid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_document);
        progress = new Progress(UploadDocument.this);
        networkCall = new NetworkCall(UploadDocument.this, UploadDocument.this);
        mSharedPreference = getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        userid = (mSharedPreference.getString("id", ""));
        email = (mSharedPreference.getString("email", ""));
        phone = (mSharedPreference.getString("mobile", ""));
        adharback = findViewById(R.id.imgBack);
        adharfront = findViewById(R.id.imgFront);
        pancard = findViewById(R.id.imgpan);

        webview = findViewById(R.id.webview);
        webview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit();
            }
        });

        txtback = findViewById(R.id.txtback);
        txtfront = findViewById(R.id.txtfront);
        txtpan = findViewById(R.id.txtpan);
        phonenum = findViewById(R.id.phonenum);

        emailid = findViewById(R.id.emailId);

        adharbackSmall = findViewById(R.id.imgBackssmall);
        adharfrontSmall = findViewById(R.id.imgfrontSmall);
        pancardSmall = findViewById(R.id.imgPANSmall);

        cback = findViewById(R.id.rback);
        cback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(UploadDocument.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(UploadDocument.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                } else {
                    clickImage = 1;
                    loadImagefromGallery();
                }
            }
        });


        cfront = findViewById(R.id.rfront);
        cfront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(UploadDocument.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(UploadDocument.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                } else {
                    clickImage = 2;
                    loadImagefromGallery();
                }
            }
        });
        cpan = findViewById(R.id.cpan);
        cpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(UploadDocument.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(UploadDocument.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                } else {
                    clickImage = 3;
                    loadImagefromGallery();
                }
            }
        });
        emailid.setText(email);
        phonenum.setText(phone);

    }

    private void submit() {

        networkCall.NetworkAPICall(ApiURL.franchise_form_documents, true);
    }

    public void back(View view) {
        super.onBackPressed();
    }


    private void loadImagefromGallery() {

        if (clickImage == 1) {

            Intent galleryIntent1 = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent1, RESULT_LOAD_IMG);


        } else if (clickImage == 2) {

            Intent galleryIntent2 = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent2, RESULT_LOAD_IMG);

        } else if (clickImage == 3) {

            Intent galleryIntent3 = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent3, RESULT_LOAD_IMG);

        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (clickImage) {
            case 1:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            adharback.setImageURI(selectedImage);
                            adharbackSmall.setVisibility(View.INVISIBLE);
                            txtback.setVisibility(View.INVISIBLE);
                            filePath1 = getRealPathFromURI(selectedImage);
                            File_data1 = new File(filePath1);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;


            case 2:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            adharfront.setImageURI(selectedImage);
                            adharfrontSmall.setVisibility(View.INVISIBLE);
                            txtfront.setVisibility(View.INVISIBLE);
                            filePath2 = getRealPathFromURI(selectedImage);
                            File_data2 = new File(filePath2);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;

            case 3:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            pancard.setImageURI(selectedImage);
                            pancardSmall.setVisibility(View.INVISIBLE);
                            txtpan.setVisibility(View.INVISIBLE);
                            filePath3 = getRealPathFromURI(selectedImage);
                            File_data3 = new File(filePath3);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;


        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private String getRealPathFromURI(Uri uri) {

        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(getApplicationContext(), uri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }


    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.franchise_form_documents:
                ion = (Builders.Any.B) Ion.with(UploadDocument.this)
                        .load("POST", ApiURL.franchise_form_documents)
                        .setHeader("token", ApiURL.token)
                        .setMultipartParameter("user_id", userid)
                        .setMultipartFile("panimage", new File(String.valueOf(filePath3)))
                        .setMultipartFile("adharfimage", new File(String.valueOf(filePath2)))
                        .setMultipartFile("adharbimage", new File(String.valueOf(filePath1)));
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.franchise_form_documents:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");

                    if (succes.equals("true")) {

//                        Intent intent = new Intent(UploadDocument.this, CreditScoreWebView.class);
//                        startActivity(intent);
//                        finish();
//                        Dialog dialogg = new Dialog(UploadDocument.this, R.style.PauseDialog);
//                        dialogg.setContentView(R.layout.site_loader_layout);
//                        handler = new Handler();
//                        handler.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                Intent intent = new Intent(UploadDocument.this, CreditScoreWebView.class);
//                                startActivity(intent);
//                                finish();
//                            }
//                        }, 1500);
//                        Window window = dialogg.getWindow();
//                        WindowManager.LayoutParams wlp = window.getAttributes();
//                        wlp.gravity = Gravity.CENTER;
//                        dialogg.show();

                        getSupportFragmentManager().beginTransaction()
                                .add(android.R.id.content, new Franchise()).commit();


                    } else {

                        String status_fail = jsonObject.getString("success");
                        if (status_fail.equals("false")) {
                            Toast.makeText(UploadDocument.this, msg.toString(), Toast.LENGTH_SHORT).show();
//                            progress.dismiss();
                        }

                    }
                } catch (JSONException e1) {


                    Toast.makeText(UploadDocument.this, jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();
                }


                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }
}