package com.gsg.franchise.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.gsg.franchise.KYC.KYCnormalDetails;
import com.gsg.franchise.R;
import com.gsg.franchise.fragments.Franchise;
import com.gsg.franchise.utill.ApiURL;
import com.gsg.franchise.utill.Progress;
import com.gocashfree.cashfreesdk.CFPaymentService;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_APP_ID;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_EMAIL;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_NAME;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_PHONE;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_AMOUNT;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_ID;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_NOTE;


public class Cash_activity extends AppCompatActivity {
    ImageView back_btn;
    Button addbtn;
    String user_id="",menteramountedit="",creditedamount="",totalamount="",mobile="",orderid="",total_cost="",myfranchise_cost="",myfranchise_fees="",package_id="";
    EditText enteramountedit;
    private String txn_id = "";
    Progress progress;
   SharedPreferences mSharedPreference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_casfree);

        progress = new Progress(Cash_activity.this);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        user_id = (mSharedPreference.getString("id", ""));

        total_cost = getIntent().getStringExtra("total_cost");
        package_id = getIntent().getStringExtra("package_id");
        myfranchise_cost = getIntent().getStringExtra("myfranchise_cost");
        myfranchise_fees = getIntent().getStringExtra("myfranchise_fees");
        progress.show();
        orderid = ApiURL.getOrderID();
        cashfree_genrate_token(orderid,total_cost);

//        addbtn=(Button)findViewById(R.id.addbtn);

//        enteramountedit=(EditText)findViewById(R.id.enteramountedit);
//
//        addbtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                menteramountedit=enteramountedit.getText().toString();
//
//                orderid = ApiURL.getOrderID();
//                double addmoneycas = Double.parseDouble(menteramountedit);
//                if (addmoneycas <= 0) {
//                    Toast.makeText(Cash_activity.this,  "Amount should be greater then Rs.10", Toast.LENGTH_SHORT).show();
//                }else if (menteramountedit.equals("")){
//                    Toast.makeText(Cash_activity.this, "Please Enter Amount", Toast.LENGTH_SHORT).show();
//
//                } else {
//                }
//
//            }
//        });

    }
    private void cashfree_genrate_token(final String orderid, final String amount) {
        JsonObject json = new JsonObject();
        json.addProperty("orderId", orderid);
        json.addProperty("orderAmount", amount);
        json.addProperty("orderCurrency", "INR");
//      json.addProperty("user_id", MyProfile.getProfile().getUserId());

        Ion.with(Cash_activity.this)
                .load("POST",ApiURL.cashfreeurl)
                .setHeader("x-client-id", ApiURL.CASHFREE_mid)
                .setHeader("x-client-secret", ApiURL.CASHFREE_SECRETID)
                .setJsonObjectBody(json)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {

                        try {
                            Log.d("cashfreedata", String.valueOf(result.toString()));
                            JSONObject jsonObject = new JSONObject(result.toString());
                            if (jsonObject.optString("status").equalsIgnoreCase("OK")) {
                                doPayment(jsonObject.optString("cftoken"), orderid, amount);
                            } else {
                                Toast.makeText(Cash_activity.this, "SERVER ERROR", Toast.LENGTH_SHORT).show();


                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        } catch (NullPointerException ex) {
                            Toast.makeText(Cash_activity.this, "SERVER ERROR", Toast.LENGTH_SHORT).show();


                            ex.printStackTrace();
                        }
//                        JSONObject jsonObject = new JSONObject(result.)
                        // do stuff with the result or error
                    }
                });
    }
    public void doPayment(String Tokken, String odr_id, String amount) {

        String token = Tokken;
//       String stage = "PROD";
////        String appId = "42503bba18ea3af85ed7e9ec730524";
        String stage = "PROD";
        String appId = ApiURL.CASHFREE_mid;
        Map<String, String> params = new HashMap<>();
        String mobile_number = "";
        String email_id = "";


//        params.put(PARAM_PAYMENT_OPTION, "card");
//        params.put(PARAM_CARD_NUMBER, "4434260000000008");//Replace Card number
//        params.put(PARAM_CARD_MM, "05"); // Card Expiry Month in MM
//        params.put(PARAM_CARD_YYYY, "2021"); // Card Expiry Year in YYYY
//        params.put(PARAM_CARD_HOLDER, "John Doe"); // Card Holder name
//        params.put(PARAM_CARD_CVV, "123");


//        params.put(PARAM_PAYMENT_OPTION, "upi");
//        params.put(PARAM_UPI_VPA, "testsuccess@gocash");

//        if (Integer.parseInt(mobile) >= 10) {
//            mobile_number = mobile;
//        } else {
//            mobile_number = "+910000000000";
//        }

        params.put(PARAM_APP_ID, appId);
        params.put(PARAM_ORDER_ID, odr_id);
        params.put(PARAM_ORDER_AMOUNT, amount);
        params.put(PARAM_ORDER_NOTE, "");
        params.put(PARAM_CUSTOMER_NAME, "testingandroid");
        params.put(PARAM_CUSTOMER_PHONE, "+910000000000");
        params.put(PARAM_CUSTOMER_EMAIL, "manojchahal93@gmail.com");



//        params.put(PARAM_NOTIFY_URL, ApiUrl.NOTIFI_URL);
//        postData.put("referenceId", REFERENCE_ID);
//        for (Map.Entry entry : params.entrySet()) {
//            Log.d("CFSKDSample", entry.getKey() + " " + entry.getValue());
//        }

        CFPaymentService cfPaymentService = CFPaymentService.getCFPaymentServiceInstance();
        cfPaymentService.setOrientation(0);

        // Use the following method for initiating Payments
        // First color - Toolbar background
        // Second color - Toolbar text and back arrow color
//        cfPaymentService.doPayment(this, params, token, stage, "#CA272A", "#CA272A", true);
        cfPaymentService.doPayment(this, params, token, stage, "#8f3295", "#FFFFFFFF", true);

//        upiPayment(this, params,token,stage);


    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable  Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("", "API Response : ");
        //Prints all extras. Replace with app logic.
        if (data != null) {
            Bundle bundle = data.getExtras();

            if (bundle != null)
                Log.e("suceess_data", bundle.toString());

            for (String key : bundle.keySet()) {
                if (bundle.getString(key) != null) {
                    Log.e("TAG", key + " : " + bundle.getString(key));
                }
            }

            try {

                if (bundle.getString("txStatus").equalsIgnoreCase("Success") || bundle.getString("txMsg").equalsIgnoreCase("Transaction Successful")) {
//                  /
//                        onBackPressed();
//                    }
                    Toast.makeText(this, bundle.getString("txMsg"), Toast.LENGTH_SHORT).show();
                    txn_id=bundle.getString("referenceId");
                    transaction_();



//                                        add_cash_amount(bundle.getString("orderId"), bundle.getString("orderAmount"));
                } else {
                    progress.dismiss();
//                    getSupportFragmentManager().beginTransaction()
//                            .add(android.R.id.content, new Franchise()).commit();
                    Intent intent=new Intent(Cash_activity.this,MainActivity.class);
                     startActivity(intent);
                    Cash_activity.this.finish();

//                    Utility.showToastMessage(AddCashActivity.this, bundle.getString("txMsg"), "2");
                }
//                Utility.showToastMessage(AddCashActivity.this, bundle.getString("txMsg"), "2");
            } catch (Exception e) {
                progress.dismiss();
                Intent intent=new Intent(Cash_activity.this,MainActivity.class);
                startActivity(intent);
                Cash_activity.this.finish();
//                Utility.showToastMessage(/AddCashActivity.this, "payment cancelled ", "2");
            }
        }    }

    private void transaction_() {
       progress.show();
        Ion.with(this)
                .load("POST", ApiURL.transaction)
                .setHeader("token", ApiURL.token)
                .setBodyParameter("user_id",user_id)
                .setBodyParameter("package_id",package_id)
                .setBodyParameter("fee",myfranchise_fees)
                .setBodyParameter("cost",myfranchise_cost)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            String success = jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("true")) {
                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
//                                creditedamount=jsonObject1.getString("amount");
//                                totalamount=jsonObject1.getString("total_amount");
//
//                                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//                                SharedPreferences.Editor editor = prefs.edit();
//                                editor.putString("creditedamount", creditedamount);
//                                editor.putString("totalamount", totalamount);
//                                editor.apply();

                                Intent intent=new Intent(Cash_activity.this, KYCnormalDetails.class);
                                startActivity(intent);
                                finish();

                            } else if (success.equals("false")) {
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                progress.dismiss();
                            }

                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }
                });

    }


    @Override
    public void onBackPressed() {
//        progress.dismiss();
            super.onBackPressed();
        }
}