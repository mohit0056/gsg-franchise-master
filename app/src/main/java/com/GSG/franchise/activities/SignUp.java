package com.gsg.franchise.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.gsg.franchise.R;
import com.gsg.franchise.utill.ApiURL;
import com.gsg.franchise.utill.NetworkCall;
import com.gsg.franchise.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONException;
import org.json.JSONObject;

public class SignUp extends AppCompatActivity implements NetworkCall.MyNetworkCallBack {
    EditText name, password, email, phone_no, confirm_password;
    RelativeLayout btnmobileRegister;
    Boolean isLogin;
    Boolean cancel;
    public final static String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
            "[a-zA-Z0-9_+&*-]+)*@" +
            "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
            "A-Z]{2,7}$";
    static final String pref_name = "GSG";
    Progress progress;
    NetworkCall networkCall;
    SharedPreferences mSharedPreference;
    String myname, mymobile_number, myemail, mypassword, mconfpass, user_id, token;
    final String regexStr = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mSharedPreference = getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        user_id = (mSharedPreference.getString("id", ""));
        token = (mSharedPreference.getString("token", ""));
        progress = new Progress(SignUp.this);
        networkCall = new NetworkCall(SignUp.this, SignUp.this);
        phone_no = (EditText) findViewById(R.id.mobileEDT);
        email = (EditText) findViewById(R.id.emailEDT);
        password = (EditText) findViewById(R.id.passwordEDT);
        name = (EditText) findViewById(R.id.nameEDT);
        confirm_password = (EditText) findViewById(R.id.confpassEDT);
        btnmobileRegister = findViewById(R.id.register);
        btnmobileRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myname = name.getText().toString().trim();
                myemail = email.getText().toString().trim();
                mymobile_number = phone_no.getText().toString().trim();
                mypassword = password.getText().toString().trim();
                mconfpass = confirm_password.getText().toString().trim();

                name.setError(null);
                email.setError(null);
                phone_no.setError(null);
                password.setError(null);
                confirm_password.setError(null);

                if (TextUtils.isEmpty(myname)) {
                    name.setError("Your Name is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(myemail)) {
                    email.setError("Your Email is required");
                    cancel = true;
                } else if (!myemail.matches(emailRegex)) {
                    email.setError("Please Enter Valid Email");
                    cancel = true;
                } else if (TextUtils.isEmpty(mymobile_number)) {
                    phone_no.setError("Phone number is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(mypassword)) {
                    password.setError("Password is required");
                    cancel = true;
                } else if (!mymobile_number.matches(regexStr)) {
                    phone_no.setError("Please Enter Valid Phone no.");
                    cancel = true;
                } else if (mypassword.length() < 6) {
                    password.setError("Password minimum contain 6 character");
                    cancel = true;
                } else if (TextUtils.isEmpty(mconfpass)) {
                    confirm_password.setError("Re-enter Password is required");
                    cancel = true;
                } else if (!mconfpass.equals(mypassword)) {
                    confirm_password.setError("Both Passwords are Different");
                    cancel = true;
                } else {
                    signUpMobile();
                }
            }
        });

    }

    private void signUpMobile() {
        networkCall.NetworkAPICall(ApiURL.user_register, true);

    }

    public void SignIn(View view) {
        Intent intent = new Intent(SignUp.this, Login.class);
        startActivity(intent);
    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.user_register:
                ion = (Builders.Any.B) Ion.with(SignUp.this)
                        .load("POST", ApiURL.user_register)
                        .setHeader("token", ApiURL.token)
                        .setBodyParameter("name", myname)
                        .setBodyParameter("email", myemail)
                        .setBodyParameter("mobile", mymobile_number)
                        .setBodyParameter("password", mypassword)
                        .setBodyParameter("confirm_password", mconfpass);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.user_register:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");

                    if (succes.equals("true")) {
                        Intent intent = new Intent(SignUp.this, Login.class);
                        startActivity(intent);
                        finish();
                    }

                    else {
                        String status_fail = jsonObject.getString("success");

                        if (status_fail.equals("false")) {
                            Toast.makeText(SignUp.this, msg.toString(), Toast.LENGTH_SHORT).show();
                            progress.dismiss();
                        }
                    }
                } catch (JSONException e1) {

                    Toast.makeText(SignUp.this, jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

        Toast.makeText(SignUp.this, jsonstring, Toast.LENGTH_SHORT).show();


    }
}
