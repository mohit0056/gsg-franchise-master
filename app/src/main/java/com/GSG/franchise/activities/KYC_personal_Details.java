package com.gsg.franchise.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.gsg.franchise.R;
import com.gsg.franchise.utill.ApiURL;
import com.gsg.franchise.utill.NetworkCall;
import com.gsg.franchise.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class KYC_personal_Details extends AppCompatActivity  implements NetworkCall.MyNetworkCallBack {
    ImageView back_image;
    Button next_btn;
    TextInputEditText other_doc,name_personal,dob,father,qualification,pan_personal,residential_address,pin_code_edit,aadhar_din_no,voter_id,personal_qualification,experience_yrs_personal,driving_license_personal,other_doc_personal,relation_personal,telephone_personal,experience_personal,
            available_space_personal ;
    String  userid,form="",str_business="",str_nm_enterprise="",str_office_address="",str_category_application="",str_address_shop="",str_telephone_no="",str_email_edit="",str_pan_no="",str_enterprise_pan="",str_enterprise_name="",str_constitution="",str_other_constitution="",str_gst_no="",str_state_="",str_district="",str_branch="",str_calendar_edit="";
    String str_other_doc,str_name_personal="",gender="",str_dob,str_father="",str_qualification="",str_pan_personal="",str_personal_category_spinner,str_residential_address="",str_pin_code_edit,str_aadhar_din_no="",str_voter_id="",str_personal_qualification="",str_experience_yrs_personal="",str_driving_license_personal="",str_other_doc_personal="",str_relation_personal="",str_telephone_personal="",str_experience_personal="",
            str_available_space_personal="";
    RadioButton male,female;
    SimpleDateFormat sdf;
    Boolean cancel;
    Spinner personal_category_spinner;
    NetworkCall networkCall;
    Progress progress;
    String[] personal_cate;
    SharedPreferences mSharedPreference;
    String Pan_regex = "[A-Z]{5}[0-9]{4}[A-Z]{1}";
    String aadhar_regex = "^[0-9]{4}[ -]?[0-9]{4}[ -]?[0-9]{4}$";
    String voter_regex = "^([a-zA-Z]){3}([0-9]){7}?$";
    String driving_license_regex = "^(([A-Z]{2}[0-9]{2})( )|([A-Z]{2}-[0-9]{2}))((19|20)[0-9][0-9])[0-9]{7}$";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_k_y_c_personal__details);

//        form = getIntent().getStringExtra("form");
//        if (form.equals("kyc")){
//            str_nm_enterprise = getIntent().getStringExtra("str_nm_enterprise");
//            str_office_address = getIntent().getStringExtra("str_office_address");
//            str_category_application = getIntent().getStringExtra("str_category_application");
//            str_address_shop = getIntent().getStringExtra("str_address_shop");
//            str_business = getIntent().getStringExtra("str_business");
//            str_telephone_no = getIntent().getStringExtra("str_telephone_no");
//            str_email_edit = getIntent().getStringExtra("str_email_edit");
//            str_pan_no = getIntent().getStringExtra("str_pan_no");
//            str_enterprise_pan = getIntent().getStringExtra("str_enterprise_pan");
//            str_constitution = getIntent().getStringExtra("str_constitution");
//            str_other_constitution = getIntent().getStringExtra("str_other_constitution");
//            str_gst_no = getIntent().getStringExtra("str_gst_no");
//            str_state_ = getIntent().getStringExtra("str_state_");
//            str_district = getIntent().getStringExtra("str_district");
//            str_branch = getIntent().getStringExtra("str_branch");
//            form = getIntent().getStringExtra("form");
//        }

        progress = new Progress(KYC_personal_Details.this);
        networkCall = new NetworkCall(KYC_personal_Details.this, KYC_personal_Details.this);

        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        userid =mSharedPreference.getString("id", "");
        str_name_personal =mSharedPreference.getString("str_name_personal", "");
        str_dob =mSharedPreference.getString("str_dob", "");
        str_father =mSharedPreference.getString("str_father", "");
        str_qualification =mSharedPreference.getString("str_qualification", "");
        str_pan_personal =mSharedPreference.getString("str_pan_personal", "");
        str_residential_address =mSharedPreference.getString("str_residential_address", "");
        str_pin_code_edit =mSharedPreference.getString("str_pin_code_edit", "");
        str_aadhar_din_no =mSharedPreference.getString("str_aadhar_din_no", "");
        str_voter_id =mSharedPreference.getString("str_voter_id", "");
        str_personal_qualification =mSharedPreference.getString("str_personal_qualification", "");
        str_experience_yrs_personal =mSharedPreference.getString("str_experience_yrs_personal", "");
        str_driving_license_personal =mSharedPreference.getString("str_driving_license_personal", "");
        str_relation_personal =mSharedPreference.getString("str_relation_personal", "");
        str_experience_personal =mSharedPreference.getString("str_experience_personal", "");
        str_telephone_personal =mSharedPreference.getString("str_telephone_personal", "");
        str_available_space_personal =mSharedPreference.getString("str_available_space_personal", "");

        name_personal=findViewById(R.id.name_personal);
        male=findViewById(R.id.male);
        female=findViewById(R.id.female);
        dob=findViewById(R.id.dob);
        father=findViewById(R.id.father);
        qualification=findViewById(R.id.qualification);
        pan_personal=findViewById(R.id.pan_personal);
        residential_address=findViewById(R.id.residential_address);
        pin_code_edit=findViewById(R.id.pin_code_edit);
        aadhar_din_no=findViewById(R.id.aadhar_din_no);
        voter_id=findViewById(R.id.voter_id);
        personal_qualification=findViewById(R.id.personal_qualification);
        experience_yrs_personal=findViewById(R.id.experience_yrs_personal);
        driving_license_personal=findViewById(R.id.driving_license_personal);
        other_doc_personal=findViewById(R.id.other_doc_personal);
        relation_personal=findViewById(R.id.relation_personal);
        telephone_personal=findViewById(R.id.telephone_personal);
        experience_personal=findViewById(R.id.experience_personal);
        available_space_personal=findViewById(R.id.available_space_personal);
        personal_category_spinner=findViewById(R.id.personal_category_spinner);
        other_doc=findViewById(R.id.other_doc);


        next_btn=findViewById(R.id.next_btn);
        back_image=findViewById(R.id.back_image);


         if (!str_name_personal.equals("")){
             name_personal.setText(str_name_personal);
         }

        if (!str_dob.equals("")){
            dob.setText(str_dob);
        }
        if (!str_father.equals("")){
            father.setText(str_father);
        }

//        if (!str_other_doc.equals("")){
//            other_doc.setText(str_other_doc);
//        }
        if (!str_qualification.equals("")){
            qualification.setText(str_qualification);
        }
        if (!str_pan_personal.equals("")){
            pan_personal.setText(str_pan_personal);
        }
        if (!str_residential_address.equals("")){
            residential_address.setText(str_residential_address);
        }
        if (!str_pin_code_edit.equals("")){
            pin_code_edit.setText(str_pin_code_edit);
        }
        if (!str_aadhar_din_no.equals("")){
            aadhar_din_no.setText(str_aadhar_din_no);
        }

        if (!str_voter_id.equals("")){
            voter_id.setText(str_voter_id);
        }
        if (!str_personal_qualification.equals("")){
            personal_qualification.setText(str_personal_qualification);
        }

        if (!str_experience_yrs_personal.equals("")){
            experience_yrs_personal.setText(str_experience_yrs_personal);
        } if (!str_driving_license_personal.equals("")){
            driving_license_personal.setText(str_driving_license_personal);
        }

        if (!str_relation_personal.equals("")){
            relation_personal.setText(str_relation_personal);
        }

        if (!str_experience_personal.equals("")){
            experience_personal.setText(str_experience_personal);
        }


        if (!str_telephone_personal.equals("")){
            telephone_personal.setText(str_telephone_personal);
        }
        if (!str_available_space_personal.equals("")){
            available_space_personal.setText(str_available_space_personal);
        }

        male.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    gender="Male";
                    female.setChecked(false);
                }
                else {
                    gender="FeMale";
                }
            }
        });
        female.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    gender="FeMale";
                    male.setChecked(false);
                }
                else {
                    gender="Male";
                }
            }
        });

        personal_cate = getResources().getStringArray(R.array.Personal_Category);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, personal_cate);
        personal_category_spinner.setAdapter(adapter);
        personal_category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                str_personal_category_spinner = String.valueOf(personal_cate[i]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(KYC_personal_Details.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        mcurrentDate.set(Calendar.YEAR, selectedyear);
                        mcurrentDate.set(Calendar.MONTH, selectedmonth);
                        mcurrentDate.set(Calendar.DAY_OF_MONTH,
                                selectedday);
                        sdf = new SimpleDateFormat(
                                getResources().getString(
                                        R.string.datecardformat),
                                Locale.US);



                        str_dob=sdf.format(mcurrentDate.getTime());

                        dob.setText(str_dob);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker.show();



                onDateSet(v,2021,9,6);

            }
        });


        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(KYC_personal_Details.this,KYC_details_1.class);
//                intent.putExtra("str_nm_enterprise", str_nm_enterprise);
//                intent.putExtra("str_office_address", str_office_address);
//                intent.putExtra("str_category_application", str_category_application);
//                intent.putExtra("str_address_shop", str_address_shop);
//                intent.putExtra("str_business", str_business);
//                intent.putExtra("str_telephone_no", str_telephone_no);
//                intent.putExtra("str_email_edit", str_email_edit);
//                intent.putExtra("str_pan_no", str_pan_no);
//                intent.putExtra("str_enterprise_pan", str_enterprise_pan);
//                intent.putExtra("str_constitution", str_constitution);
//                intent.putExtra("str_other_constitution", str_other_constitution);
//                intent.putExtra("str_gst_no", str_gst_no);
//                intent.putExtra("str_calendar_edit", str_calendar_edit);
//                intent.putExtra("str_state_", str_state_);
//                intent.putExtra("str_district", str_district);
//                intent.putExtra("str_branch", str_branch);
//                intent.putExtra("form", "kyc");
                startActivity(intent);
                KYC_personal_Details.this.finish();
            }
        });
        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_name_personal = name_personal.getText().toString().trim();
                str_dob = dob.getText().toString().trim();
                str_father =father.getText().toString().trim();
                str_other_doc =other_doc.getText().toString().trim();
                str_qualification =qualification.getText().toString().trim();
                str_pan_personal =pan_personal.getText().toString().trim();
                str_residential_address =residential_address.getText().toString().trim();
                str_pin_code_edit =pin_code_edit.getText().toString().trim();
                str_aadhar_din_no =aadhar_din_no.getText().toString().trim();
                str_voter_id =voter_id.getText().toString().trim();
                str_personal_qualification =personal_qualification.getText().toString().trim();
                str_experience_yrs_personal =experience_yrs_personal.getText().toString().trim();
                str_driving_license_personal =driving_license_personal.getText().toString().trim();
                str_other_doc_personal =other_doc_personal.getText().toString().trim();
                str_relation_personal =relation_personal.getText().toString().trim();
                str_telephone_personal =telephone_personal.getText().toString().trim();
                str_experience_personal =experience_personal.getText().toString().trim();
                str_available_space_personal =available_space_personal.getText().toString().trim();
                name_personal.setError(null);
                dob.setError(null);
                father.setError(null);
                qualification.setError(null);
                pan_personal.setError(null);
              //  other_doc.setError(null);
                residential_address.setError(null);
                pin_code_edit.setError(null);
                aadhar_din_no.setError(null);
             //   voter_id.setError(null);
                personal_qualification.setError(null);
                experience_yrs_personal.setError(null);
                driving_license_personal.setError(null);
               // other_doc_personal.setError(null);
                relation_personal.setError(null);
                telephone_personal.setError(null);
                experience_personal.setError(null);
                available_space_personal.setError(null);

                if (TextUtils.isEmpty(str_name_personal)) {
                    Toast.makeText(KYC_personal_Details.this, "Name of the Enterprise is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (TextUtils.isEmpty(str_dob)) {
                    Toast.makeText(KYC_personal_Details.this, "Date of Birth is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (TextUtils.isEmpty(str_father)) {
                    Toast.makeText(KYC_personal_Details.this, "Father / Spouse is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (TextUtils.isEmpty(str_qualification)) {
                    Toast.makeText(KYC_personal_Details.this, "Academic Qualification is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (str_personal_category_spinner.equals("Select")) {
                    Toast.makeText(KYC_personal_Details.this, "Select Category", Toast.LENGTH_SHORT).show();
                    cancel = true;
                } else if (TextUtils.isEmpty(str_pan_personal)) {
                    Toast.makeText(KYC_personal_Details.this, "PAN is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if(!str_pan_personal.matches(Pan_regex)) {
                    pan_personal.setError("Please Enter Valid PAN Number");
                    cancel = true;
                }else if (TextUtils.isEmpty(str_residential_address)) {
                    Toast.makeText(KYC_personal_Details.this, "Residential Address is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (TextUtils.isEmpty(str_pin_code_edit)) {
                    Toast.makeText(KYC_personal_Details.this, "PIN Code is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (TextUtils.isEmpty(str_aadhar_din_no)) {
                    Toast.makeText(KYC_personal_Details.this, "Aadhar No./DIN No. is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }

//                else if (TextUtils.isEmpty(str_voter_id)) {
//                    Toast.makeText(KYC_personal_Details.this, "Voter ID No.is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }

//                else if(!str_voter_id.matches(voter_regex)) {
//                    Toast.makeText(KYC_personal_Details.this, "Please Enter Valid Voter ID Number", Toast.LENGTH_SHORT).show();
//                }

                else if (TextUtils.isEmpty(str_personal_qualification)) {
                    Toast.makeText(KYC_personal_Details.this, "Qualification is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (TextUtils.isEmpty(str_experience_yrs_personal)) {
                    Toast.makeText(KYC_personal_Details.this, "Experience(Yrs) is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }

//                else if (TextUtils.isEmpty(str_driving_license_personal)) {
//                    Toast.makeText(KYC_personal_Details.this, "Driving License No. is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }

//                else if(!str_driving_license_personal.matches(driving_license_regex)) {
//                    driving_license_personal.setError("Please Enter Valid Driving License Number");
//                    cancel = true;
//                }

//                else if (TextUtils.isEmpty(str_other_doc)) {
//                    other_doc.setError("This field is required");
//                    cancel = true;
//                }

//                else if (TextUtils.isEmpty(str_other_doc_personal)) {
//                    Toast.makeText(KYC_personal_Details.this, "Other Document No.is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }

                else if (TextUtils.isEmpty(str_relation_personal)) {
                    Toast.makeText(KYC_personal_Details.this, "Relationship with officials / Director of the bank (if any) is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (TextUtils.isEmpty(str_telephone_personal)) {
                    Toast.makeText(KYC_personal_Details.this, "Telephone No. (Residence) is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (TextUtils.isEmpty(str_experience_personal)) {
                    Toast.makeText(KYC_personal_Details.this, "Experience in the line of activity (Years) is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (TextUtils.isEmpty(str_available_space_personal)) {
                    Toast.makeText(KYC_personal_Details.this, "Available space for Business is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else {

                    kyc_Personal_Details();


                  /*  Intent intent=new Intent(KYC_personal_Details.this,Business_kyc_details.class);
                    intent.putExtra("str_nm_enterprise", str_nm_enterprise);
                    intent.putExtra("str_office_address", str_office_address);
                    intent.putExtra("str_category_application", str_category_application);
                    intent.putExtra("str_address_shop", str_address_shop);
                    intent.putExtra("str_business", str_business);
                    intent.putExtra("str_telephone_no", str_telephone_no);
                    intent.putExtra("str_email_edit", str_email_edit);
                    intent.putExtra("str_pan_no", str_pan_no);
                    intent.putExtra("str_enterprise_pan", str_enterprise_pan);
                    intent.putExtra("str_constitution", str_constitution);
                    intent.putExtra("str_other_constitution", str_other_constitution);
                    intent.putExtra("str_gst_no", str_gst_no);
                    intent.putExtra("str_calendar_edit", str_calendar_edit);
                    intent.putExtra("str_state_", str_state_);
                    intent.putExtra("str_district", str_district);
                    intent.putExtra("str_branch", str_branch);
                    intent.putExtra("str_dob", str_dob);
                    intent.putExtra("str_name_personal", str_name_personal);
                    intent.putExtra("str_father", str_father);
                    intent.putExtra("str_qualification", str_qualification);
                    intent.putExtra("str_pan_personal", str_pan_personal);
                    intent.putExtra("str_residential_address", str_residential_address);
                    intent.putExtra("str_pin_code_edit", str_pin_code_edit);
                    intent.putExtra("str_aadhar_din_no", str_aadhar_din_no);
                    intent.putExtra("str_voter_id", str_voter_id);
                    intent.putExtra("str_personal_qualification", str_personal_qualification);
                    intent.putExtra("str_experience_yrs_personal", str_experience_yrs_personal);
                    intent.putExtra("str_driving_license_personal", str_driving_license_personal);
                    intent.putExtra("str_other_doc_personal", str_other_doc_personal);
                    intent.putExtra("str_relation_personal", str_relation_personal);
                    intent.putExtra("str_telephone_personal", str_telephone_personal);
                    intent.putExtra("str_experience_personal", str_experience_personal);
                    intent.putExtra("str_available_space_personal", str_available_space_personal);
                    intent.putExtra("form", "kyc");
                    startActivity(intent);
                    KYC_personal_Details.this.finish();*/
                }
            }
        });
    }

    private void kyc_Personal_Details() {
        networkCall.NetworkAPICall(ApiURL.kycPersonalDetails, true);
    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent(KYC_personal_Details.this,KYC_details_1.class);
       /* intent.putExtra("str_nm_enterprise", str_nm_enterprise);
        intent.putExtra("str_office_address", str_office_address);
        intent.putExtra("str_category_application", str_category_application);
        intent.putExtra("str_address_shop", str_address_shop);
        intent.putExtra("str_business", str_business);
        intent.putExtra("str_telephone_no", str_telephone_no);
        intent.putExtra("str_email_edit", str_email_edit);
        intent.putExtra("str_pan_no", str_pan_no);
        intent.putExtra("str_enterprise_pan", str_enterprise_pan);
        intent.putExtra("str_constitution", str_constitution);
        intent.putExtra("str_other_constitution", str_other_constitution);
        intent.putExtra("str_gst_no", str_gst_no);
        intent.putExtra("str_calendar_edit", str_calendar_edit);
        intent.putExtra("str_state_", str_state_);
        intent.putExtra("str_district", str_district);
        intent.putExtra("str_branch", str_branch);
        intent.putExtra("form", "kyc");*/
        startActivity(intent);
        KYC_personal_Details.this.finish();
    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.kycPersonalDetails:
                ion = (Builders.Any.B) Ion.with(KYC_personal_Details.this)
                        .load("POST", ApiURL.kycPersonalDetails)
                        .setHeader("token", ApiURL.token)
                        .setBodyParameter("user_id", userid)
                        .setBodyParameter("name", str_name_personal)
                        .setBodyParameter("gender", gender)
                        .setBodyParameter("dob", str_dob)
                        .setBodyParameter("father/spouse", str_father)
                        .setBodyParameter("Academicqualification", str_qualification)
                        .setBodyParameter("category", str_personal_category_spinner)
                        .setBodyParameter("panNumber", str_pan_personal)
                        .setBodyParameter("Residentialaddress", str_residential_address)
                        .setBodyParameter("pincode", str_pin_code_edit)
                        .setBodyParameter("adharNumber", str_aadhar_din_no)
                        .setBodyParameter("voteNumber", str_voter_id)
                        .setBodyParameter("qualification", str_personal_qualification)
                        .setBodyParameter("yearExprience", str_experience_yrs_personal)
                        .setBodyParameter("drivingLicense", str_driving_license_personal)
                        .setBodyParameter("otherDocument","")
                        .setBodyParameter("otherDocumentno", str_other_doc_personal)
                        .setBodyParameter("relationship/directorBank", str_relation_personal)
                        .setBodyParameter("ExprienceActivity/years", str_experience_personal)
                        .setBodyParameter("telephoneNo/Residence", str_telephone_personal)
                        .setBodyParameter("spaceBussiness", str_available_space_personal);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.kycPersonalDetails:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String status = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");

                    if (status.equals("true")) {

                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        str_name_personal=jsonObject1.getString("name");
                        gender=jsonObject1.getString("gender");
                        str_father=jsonObject1.getString("father/spouse");
                        str_qualification=jsonObject1.getString("Academicqualification");
                        str_business=jsonObject1.getString("category2");
                        str_pan_personal=jsonObject1.getString("panNumber");
                        str_dob= jsonObject1.getString("dob");
                        str_residential_address=jsonObject1.getString("Residentialaddress");
                        str_pin_code_edit=jsonObject1.getString("pincode");
                        str_voter_id=jsonObject1.getString("voteNumber");
                        str_aadhar_din_no=jsonObject1.getString("adharNumber");
                        str_personal_qualification=jsonObject1.getString("qualification");
                        str_experience_yrs_personal=jsonObject1.getString("yearExprience");
                        str_driving_license_personal=jsonObject1.getString("drivingLicense");
                        str_other_doc_personal=jsonObject1.getString("otherDocumentno");
                        str_relation_personal=jsonObject1.getString("relationship/directorBank");
                        str_experience_personal=jsonObject1.getString("ExprienceActivity/years");
                        str_telephone_personal=jsonObject1.getString("telephoneNo/Residence");
                        str_available_space_personal=jsonObject1.getString("spaceBussiness");

                        name_personal.setText(str_name_personal);
                        dob.setText(str_dob);
                        father.setText(str_father);
                        qualification.setText(str_qualification);
                        pan_personal.setText(str_pan_personal);
                        residential_address.setText(str_residential_address);
                        pin_code_edit.setText(str_pin_code_edit);
                         aadhar_din_no.setText(str_aadhar_din_no);
                        voter_id.setText(str_voter_id);
                        personal_qualification.setText(str_personal_qualification);
                        experience_yrs_personal.setText(str_experience_yrs_personal);
                        driving_license_personal.setText(str_driving_license_personal);
                        relation_personal.setText(str_relation_personal);
                         experience_personal.setText(str_experience_personal);
                        telephone_personal.setText(str_telephone_personal);
                         available_space_personal.setText(str_available_space_personal);



                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("str_name_personal", str_name_personal);
                        editor.putString("str_dob", str_dob);
                        editor.putString("str_father", str_father);
                        editor.putString("str_qualification", str_qualification);
                        editor.putString("str_pan_personal", str_pan_personal);
                        editor.putString("str_residential_address", str_residential_address);
                        editor.putString("str_pin_code_edit", str_pin_code_edit);
                        editor.putString("str_aadhar_din_no", str_aadhar_din_no);
                        editor.putString("str_voter_id", str_voter_id);
                        editor.putString("str_personal_qualification", str_personal_qualification);
                        editor.putString("str_experience_yrs_personal", str_experience_yrs_personal);
                        editor.putString("str_driving_license_personal", str_driving_license_personal);
                        editor.putString("str_relation_personal", str_relation_personal);
                        editor.putString("str_experience_personal", str_experience_personal);
                        editor.putString("str_telephone_personal", str_telephone_personal);
                        editor.putString("str_available_space_personal", str_available_space_personal);
                        editor.commit();
                        Intent intent=new Intent(KYC_personal_Details.this,Business_kyc_details.class);
                        startActivity(intent);
                        finish();

                    } else {


                        String fail_status = jsonObject.getString("success");

                        if (fail_status.equals("false")) {


                            Toast.makeText(KYC_personal_Details.this, msg, Toast.LENGTH_SHORT).show();
                        }
                        progress.dismiss();
                    }
                } catch (JSONException e1) {


                    Toast.makeText(KYC_personal_Details.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        Toast.makeText(KYC_personal_Details.this, jsonstring, Toast.LENGTH_SHORT).show();

    }

    public void onDateSet(View view, int year, int month, int day) {
        Calendar userAge = new GregorianCalendar(year,month,day);
        Calendar minAdultAge = new GregorianCalendar();
        minAdultAge.add(Calendar.YEAR, -18);
        if (minAdultAge.before(userAge)) {
            Toast.makeText(KYC_personal_Details.this, "hi", Toast.LENGTH_SHORT).show();;
        }
    }
}