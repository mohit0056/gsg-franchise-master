package com.gsg.franchise.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.gsg.franchise.R;
import com.gsg.franchise.utill.ApiURL;
import com.gsg.franchise.utill.NetworkCall;
import com.gsg.franchise.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Login extends AppCompatActivity implements NetworkCall.MyNetworkCallBack{
    EditText email, password;
    String myemail, mypassword,myname;
    RelativeLayout btnmobileLogin;
    Boolean isLogin;
    Boolean cancel;
    SharedPreferences mSharedPreference;
    static final String pref_name = "GSG";
    Progress progress;
    String  userid,stemail,stphone,name;
    final String regexStr = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";
    public final static String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
            "[a-zA-Z0-9_+&*-]+)*@" +
            "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
            "A-Z]{2,7}$";
    NetworkCall networkCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mSharedPreference = getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        userid = (mSharedPreference.getString("id", ""));


        isLogin = mSharedPreference.getBoolean("login", false);
        if (isLogin) {
            Intent intent = new Intent(Login.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }

        progress = new Progress(Login.this);
        networkCall = new NetworkCall(Login.this, Login.this);

        email = (EditText) findViewById(R.id.emailEDT);
        password = (EditText) findViewById(R.id.passwordEDT);

        btnmobileLogin = findViewById(R.id.btnmobileLogin);
        btnmobileLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                myemail = email.getText().toString().trim();
                mypassword = password.getText().toString().trim();
                email.setError(null);
                password.setError(null);

                if (TextUtils.isEmpty(myemail)) {
                    email.setError("E-mail is Required");
                    cancel = true;
                }
//                else if(!myemail.matches(emailRegex)) {
//                    email.setError("Please Enter Valid E-mail");
//                    cancel = true;
//                }

                else if (TextUtils.isEmpty(mypassword)) {
                    password.setError("Password is required");
                    cancel = true;
                }

                else if (password.length() < 6) {
                    password.setError("Password minimum contain 6 character");
                    cancel = true;
                }

                else {

                    login();


                }
            }
        });


    }

    private void login() {

        networkCall.NetworkAPICall(ApiURL.user_login, true);

    }


    public void SignUp(View view) {
        Intent intent = new Intent(Login.this, SignUp.class);
        startActivity(intent);
    }


    public void forgot(View view) {
        Intent forgot = new Intent(Login.this, ForgotPassword.class);
        startActivity(forgot);
    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.user_login:
                ion = (Builders.Any.B) Ion.with(Login.this)
                        .load("POST", ApiURL.user_login)
                        .setHeader("token", ApiURL.token)
                        .setBodyParameter("email", myemail)
                        .setBodyParameter("password", mypassword);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.user_login:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String status = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");

                    if (status.equals("true")) {

                        JSONArray jsonObject1 = jsonObject.getJSONArray("data");

                        for (int i = 0; i < jsonObject1.length(); i++) {
                            JSONObject Jasonobject1 = jsonObject1.getJSONObject(0);
                            userid = Jasonobject1.getString("id");
                            myname = Jasonobject1.getString("name");
                            stemail = Jasonobject1.getString("email");
                            stphone = Jasonobject1.getString("mobile");
//                            mymobile_number = Jasonobject1.getString("mobile");
//                            wallletAmount = Jasonobject1.getString("walllet_amount");
                        }
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("name", myname);
                        editor.putString("email", stemail);
                        editor.putString("mobile", stphone);
                        editor.putString("id", userid);
                        editor.commit();
                        mSharedPreference.edit().putBoolean("login", true).commit();



                                Intent intent = new Intent(Login.this, MainActivity.class);
                                startActivity(intent);
                                finish();

                    } else {


                        String fail_status = jsonObject.getString("success");

                        if (fail_status.equals("false")) {


                             Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                        }
                        progress.dismiss();
                    }
                } catch (JSONException e1) {


                     Toast.makeText(Login.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

        Toast.makeText(Login.this, jsonstring, Toast.LENGTH_SHORT).show();

    }
}