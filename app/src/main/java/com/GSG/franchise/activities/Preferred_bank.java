package com.gsg.franchise.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.gsg.franchise.R;
import com.gsg.franchise.utill.ApiURL;
import com.gsg.franchise.utill.NetworkCall;
import com.gsg.franchise.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Preferred_bank extends AppCompatActivity implements NetworkCall.MyNetworkCallBack{
    Button next_btn;
    ImageView back_image;
    TextInputEditText bank_name, bank_branch, application_bank_doc,signature,submission_date;
    String str_bank_name, str_bank_branch,str_application_bank_doc,str_signature,str_submission_date;
    private static final int PICK_FROM_GALLERY = 1;
    int clickImage;
    final int RESULT_LOAD_IMG = 1000;
    Uri contentURI1;
    SimpleDateFormat sdf;
    Boolean cancel;
    NetworkCall networkCall;
    Progress progress;
    String  userid="";
    SharedPreferences mSharedPreference;
    CheckBox declaration_check,objection_checkbox;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferred_bank);
        progress = new Progress(Preferred_bank.this);
        networkCall = new NetworkCall(Preferred_bank.this, Preferred_bank.this);


        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        userid =mSharedPreference.getString("id", "");
        str_bank_name =mSharedPreference.getString("str_bank_name", "");
        str_bank_branch =mSharedPreference.getString("str_bank_branch", "");
        str_application_bank_doc =mSharedPreference.getString("str_application_bank_doc", "");
        str_signature =mSharedPreference.getString("str_signature", "");
        str_submission_date =mSharedPreference.getString("str_submission_date", "");


        back_image = findViewById(R.id.back_image);
        bank_name = findViewById(R.id.bank_name);
        bank_branch = findViewById(R.id.bank_branch);
        application_bank_doc = findViewById(R.id.application_bank_doc);
        signature = findViewById(R.id.signature);
        submission_date = findViewById(R.id.submission_date);
        next_btn = findViewById(R.id.next_btn);
        declaration_check = findViewById(R.id.declaration_check);
        objection_checkbox = findViewById(R.id.objection_checkbox);


        if (!str_bank_name.equals("")){
            bank_name.setText(str_bank_name);
        }
        if (!str_bank_branch.equals("")){
            bank_branch.setText(str_bank_branch);
        }
        if (!str_application_bank_doc.equals("")){
            application_bank_doc.setText(str_application_bank_doc);
        }
        if (!str_signature.equals("")){
            signature.setText(str_signature);
        }
        if (!str_submission_date.equals("")){
            submission_date.setText(str_submission_date);
        }
        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Preferred_bank.this, Status_regarding_statutory.class);
                startActivity(intent);
                Preferred_bank.this.finish();
            }
        });


        application_bank_doc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (ActivityCompat.checkSelfPermission(Preferred_bank.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(Preferred_bank.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                    } else {
                        clickImage = 1;
                        loadImagefromGallery();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        signature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (ActivityCompat.checkSelfPermission(Preferred_bank.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(Preferred_bank.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                    } else {
                        clickImage = 2;
                        loadImagefromGallery();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        submission_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(Preferred_bank.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        mcurrentDate.set(Calendar.YEAR, selectedyear);
                        mcurrentDate.set(Calendar.MONTH, selectedmonth);
                        mcurrentDate.set(Calendar.DAY_OF_MONTH,
                                selectedday);
                        sdf = new SimpleDateFormat(
                                getResources().getString(
                                        R.string.datecardformat),
                                Locale.US);
                        str_submission_date=sdf.format(mcurrentDate.getTime());

                        submission_date.setText(str_submission_date);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker.show();
            }
        });


        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_bank_name = bank_name.getText().toString().trim();
                str_bank_branch = bank_branch.getText().toString().trim();

                if (TextUtils.isEmpty(str_bank_name)) {
                    Toast.makeText(Preferred_bank.this, "Bank Name is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                } else if (TextUtils.isEmpty(str_bank_branch)) {
                    Toast.makeText(Preferred_bank.this, "Branch is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (str_application_bank_doc==null){
                    Toast.makeText(Preferred_bank.this, "Application document is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (str_signature==null){
                    Toast.makeText(Preferred_bank.this, "Signature is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (TextUtils.isEmpty(str_submission_date)){
                    Toast.makeText(Preferred_bank.this, "Submission Date is required", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else if (!declaration_check.isChecked()) {
                    Toast.makeText(getApplicationContext(), "Please accept the Declaration", Toast.LENGTH_SHORT).show();
                }else if (!objection_checkbox.isChecked()) {
                    Toast.makeText(getApplicationContext(), "Please accept the Objection Clause", Toast.LENGTH_SHORT).show();
                }else {
                    kyc_preferred();

                }
            }
        });
    }

    private void loadImagefromGallery() {
        if (clickImage == 1) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        }else if (clickImage == 2) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (clickImage) {
            case 1:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI1 = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            str_application_bank_doc = getRealPathFromURI(selectedImage);
                            File File_data_1 = new File(str_application_bank_doc);
                            String application_book_name = File_data_1.getName();
                            application_bank_doc.setText(application_book_name);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
            case 2:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI1 = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            str_signature = getRealPathFromURI(selectedImage);
                            File File_data_1 = new File(str_signature);
                            String signature_name = File_data_1.getName();
                            signature.setText(signature_name);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent(Preferred_bank.this,Status_regarding_statutory.class);
        startActivity(intent);
        Preferred_bank.this.finish();
    }

    private void kyc_preferred() {
        networkCall.NetworkAPICall(ApiURL.kycPreferred, true);
    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.kycPreferred:
                ion = (Builders.Any.B) Ion.with(Preferred_bank.this)
                        .load("POST", ApiURL.kycPreferred)
                        .setHeader("token", ApiURL.token)
                        .setMultipartParameter("user_id", userid)
                        .setMultipartParameter("Bank_name", str_bank_name)
                        .setMultipartParameter("Branch", str_bank_branch)
                        .setMultipartFile("document", new File(str_application_bank_doc))
                        .setMultipartFile("signature", new File(str_signature))
                        .setMultipartParameter("date",str_submission_date)
                        .setMultipartParameter("decalaration",str_submission_date)
                        .setMultipartParameter("objectClause",str_submission_date);

                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.kycPreferred:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String status = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");

                    if (status.equals("true")) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        str_bank_name=jsonObject1.getString("Bank_name");
                        str_bank_branch=jsonObject1.getString("branch");
                        str_application_bank_doc=jsonObject1.getString("document");
                        str_signature=jsonObject1.getString("signature");
                        str_submission_date=jsonObject1.getString("date");

                        bank_name.setText(str_bank_name);
                        bank_branch.setText(str_bank_branch);
                        application_bank_doc.setText(str_application_bank_doc);
                        signature.setText(str_signature);
                        submission_date.setText(str_submission_date);


                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("str_bank_name", str_bank_name);
                        editor.putString("str_bank_branch", str_bank_branch);
                        editor.putString("str_application_bank_doc", str_application_bank_doc);
                        editor.putString("str_signature", str_signature);
                        editor.putString("str_submission_date", str_submission_date);
                        editor.commit();
                        Intent intent = new Intent(Preferred_bank.this, Check_List.class);
                        startActivity(intent);
                        Preferred_bank.this.finish();

                    } else {
                        String fail_status = jsonObject.getString("success");
                        if (fail_status.equals("false")) {
                            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                        }
                        progress.dismiss();
                    }
                } catch (JSONException e1) {
                    Toast.makeText(Preferred_bank.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        Toast.makeText(Preferred_bank.this, jsonstring, Toast.LENGTH_SHORT).show();

    }
}