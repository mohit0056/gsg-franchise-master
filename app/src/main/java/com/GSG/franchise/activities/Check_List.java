package com.gsg.franchise.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.gsg.franchise.R;
import com.gsg.franchise.fragments.Franchise;
import com.gsg.franchise.fragments.Home;
import com.gsg.franchise.utill.Progress;

public class Check_List extends AppCompatActivity {
      ImageView back_image;
    Handler handler;
    Progress progress;
    Button next_btn;
    @Override
    public void onBackPressed() {
        Intent intent=new Intent(Check_List.this,Preferred_bank.class);
        startActivity(intent);
        Check_List.this.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check__list);
        back_image=findViewById(R.id.back_image);
        next_btn=findViewById(R.id.next_btn);
        progress = new Progress(Check_List.this);
        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Check_List.this,Preferred_bank.class);
                startActivity(intent);
                Check_List.this.finish();
//
            }
        });
        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progress.show();
                handler=new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progress.dismiss();
//                        getSupportFragmentManager().beginTransaction()
//                                .add(android.R.id.content, new Home()).commit();

                        Intent intent=new Intent(Check_List.this, MainActivity.class);
                        startActivity(intent);
                        Check_List.this.finish();
                    }
                },3000);
            }
        });

    }
}