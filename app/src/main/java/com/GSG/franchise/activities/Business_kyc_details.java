package com.gsg.franchise.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.gsg.franchise.R;
import com.gsg.franchise.utill.ApiURL;
import com.gsg.franchise.utill.NetworkCall;
import com.gsg.franchise.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Business_kyc_details extends AppCompatActivity implements NetworkCall.MyNetworkCallBack {
     ImageView back_image;
     Button next_btn;
    String  userid,form="",str_business="",str_nm_enterprise="",str_office_address="",str_category_application="",str_address_shop="",str_telephone_no="",str_email_edit="",str_pan_no="",str_enterprise_pan="",str_enterprise_name="",str_constitution="",str_other_constitution="",str_gst_no="",str_state_="",str_district="",str_branch="",str_calendar_edit="";
    RadioButton yes_business,no_business;
    TextInputEditText business_minority,social_category,business_registration_act,business_nature_association,business_presently_banking,business_address_associate,business_associate_concerns,business_office_address,
            business_registration_no,business_date,business_automobile,business_proposed,business_since,business_exit;
    String str_check_box="",str_business_radio="",str_business_minority="",str_social_category="",str_business_registration_act="",str_business_nature_association="",str_business_presently_banking="",str_business_address_associate="",str_business_associate_concerns="",
            str_business_office_address="",str_business_registration_no="",str_business_date="",str_business_automobile="",str_business_proposed="",str_business_since="",str_business_exit="";
    CheckBox bronze,silver,gold,diamond,platinum,no;
    Boolean cancel;
    SimpleDateFormat sdf;
    NetworkCall networkCall;
    Progress progress;
    SharedPreferences mSharedPreference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busniess_kyc_details);

        progress = new Progress(Business_kyc_details.this);
        networkCall = new NetworkCall(Business_kyc_details.this, Business_kyc_details.this);

        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        userid =mSharedPreference.getString("id", "");
        str_business_exit =mSharedPreference.getString("str_business_exit", "");
        str_business_since =mSharedPreference.getString("str_business_since", "");
        str_business_proposed =mSharedPreference.getString("str_business_proposed", "");
        str_business_automobile =mSharedPreference.getString("str_business_automobile", "");
        str_business_date =mSharedPreference.getString("str_business_date", "");
        str_business_registration_no =mSharedPreference.getString("str_business_registration_no", "");
        str_business_office_address =mSharedPreference.getString("str_business_office_address", "");
        str_business_address_associate =mSharedPreference.getString("str_business_address_associate", "");
        str_business_presently_banking =mSharedPreference.getString("str_business_presently_banking", "");
        str_business_nature_association =mSharedPreference.getString("str_business_nature_association", "");
        str_business_registration_act =mSharedPreference.getString("str_business_registration_act", "");
        str_social_category =mSharedPreference.getString("str_social_category", "");
        str_business_minority =mSharedPreference.getString("str_business_minority", "");
        str_business_radio =mSharedPreference.getString("str_business_radio", "");
        str_check_box =mSharedPreference.getString("str_check_box", "");

//        form = getIntent().getStringExtra("form");
//        if (form.equals("kyc")){
//            str_nm_enterprise = getIntent().getStringExtra("str_nm_enterprise");
//            str_office_address = getIntent().getStringExtra("str_office_address");
//            str_category_application = getIntent().getStringExtra("str_category_application");
//            str_address_shop = getIntent().getStringExtra("str_address_shop");
//            str_business = getIntent().getStringExtra("str_business");
//            str_telephone_no = getIntent().getStringExtra("str_telephone_no");
//            str_email_edit = getIntent().getStringExtra("str_email_edit");
//            str_pan_no = getIntent().getStringExtra("str_pan_no");
//            str_enterprise_pan = getIntent().getStringExtra("str_enterprise_pan");
//            str_constitution = getIntent().getStringExtra("str_constitution");
//            str_other_constitution = getIntent().getStringExtra("str_other_constitution");
//            str_gst_no = getIntent().getStringExtra("str_gst_no");
//            str_state_ = getIntent().getStringExtra("str_state_");
//            str_district = getIntent().getStringExtra("str_district");
//            str_branch = getIntent().getStringExtra("str_branch");
//            form = getIntent().getStringExtra("form");
//        }

        yes_business=findViewById(R.id.yes_business);
        no_business=findViewById(R.id.no_business);
        back_image=findViewById(R.id.back_image);
        next_btn=findViewById(R.id.next_btn);
        business_minority=findViewById(R.id.business_minority);
        social_category=findViewById(R.id.social_category);
        business_registration_act=findViewById(R.id.business_registration_act);
        business_nature_association=findViewById(R.id.business_nature_association);
        business_presently_banking=findViewById(R.id.business_presently_banking);
        business_address_associate=findViewById(R.id.business_address_associate);
        business_associate_concerns=findViewById(R.id.business_associate_concerns);
        business_office_address=findViewById(R.id.business_office_address);
        business_registration_no=findViewById(R.id.business_registration_no);
        business_date=findViewById(R.id.business_date);
        business_automobile=findViewById(R.id.business_automobile);
        business_proposed=findViewById(R.id.business_proposed);
        business_since=findViewById(R.id.business_since);
        business_exit=findViewById(R.id.business_exit);
        bronze=findViewById(R.id.bronze);
        silver=findViewById(R.id.silver);
        gold=findViewById(R.id.gold);
        diamond=findViewById(R.id.diamond);
        platinum=findViewById(R.id.platinum);
        no=findViewById(R.id.no);

        if (!str_business_exit.equals("")){
            business_exit.setText(str_business_exit);
        }

        if (!str_business_since.equals("")){
            business_since.setText(str_business_since);
        }
        if (!str_business_proposed.equals("")){
            business_proposed.setText(str_business_proposed);
        }
        if (!str_business_automobile.equals("")){
            business_automobile.setText(str_business_automobile);
        }
        if (!str_business_date.equals("")){
            business_date.setText(str_business_date);
        }

        if (!str_business_registration_no.equals("")){
            business_registration_no.setText(str_business_registration_no);
        }

        if (!str_business_office_address.equals("")){
            business_office_address.setText(str_business_office_address);
        }
        if (!str_business_address_associate.equals("")){
           business_address_associate.setText(str_business_address_associate);
        }
        if (!str_business_nature_association.equals("")){
            business_nature_association.setText(str_business_nature_association);
        }
        if (!str_business_registration_act.equals("")){
            business_registration_act.setText(str_business_registration_act);
        }
        if (!str_social_category.equals("")){
            social_category.setText(str_social_category);
        }
        if (!str_business_minority.equals("")){
            business_minority.setText(str_business_minority);
        }


        if (str_check_box.equals("Bronze")){
            silver.setChecked(false);
            gold.setChecked(false);
            diamond.setChecked(false);
            bronze.setChecked(true);
            platinum.setChecked(false);
            no.setChecked(false);

        }else if (str_check_box.equals("Silver")){
            silver.setChecked(true);
            gold.setChecked(false);
            diamond.setChecked(false);
            bronze.setChecked(false);
            platinum.setChecked(false);
            no.setChecked(false);

        } else if (str_check_box.equals("Gold")){
            silver.setChecked(false);
            gold.setChecked(true);
            diamond.setChecked(false);
            bronze.setChecked(false);
            platinum.setChecked(false);
            no.setChecked(false);

        } else if (str_check_box.equals("Diamond")){
            silver.setChecked(false);
            gold.setChecked(false);
            diamond.setChecked(true);
            bronze.setChecked(false);
            platinum.setChecked(false);
            no.setChecked(false);

        } else if (str_check_box.equals("Platinum")){
            silver.setChecked(false);
            gold.setChecked(false);
            diamond.setChecked(false);
            bronze.setChecked(false);
            platinum.setChecked(true);
            no.setChecked(false);
        }else if (str_check_box.equals("No")){
            silver.setChecked(false);
            gold.setChecked(false);
            diamond.setChecked(false);
            bronze.setChecked(false);
            platinum.setChecked(false);
            no.setChecked(true);

        }else {
            silver.setChecked(false);
            gold.setChecked(false);
            diamond.setChecked(false);
            bronze.setChecked(false);
            platinum.setChecked(false);
            no.setChecked(false);

        }


        if (str_business_radio.equals("Yes")){
            yes_business.setChecked(true);
        }else if (str_business_radio.equals("No")){
            no_business.setChecked(true);
        }else {
            yes_business.setChecked(false);
            no_business.setChecked(false);
        }


        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Business_kyc_details.this,KYC_personal_Details.class);
                startActivity(intent);
                Business_kyc_details.this.finish();
            }
        });

        bronze.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    str_check_box="Bronze";
                    bronze.setChecked(true);

                    silver.setChecked(false);
                    gold.setChecked(false);
                    diamond.setChecked(false);
                    platinum.setChecked(false);
                    no.setChecked(false);

                }
            }
        });

        silver.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    str_check_box="Silver";
                    silver.setChecked(true);

                    bronze.setChecked(false);
                    gold.setChecked(false);
                    diamond.setChecked(false);
                    platinum.setChecked(false);
                    no.setChecked(false);

                }
            }
        });
        gold.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    str_check_box="Gold";
                    gold.setChecked(true);
                    silver.setChecked(false);
                    bronze.setChecked(false);
                    diamond.setChecked(false);
                    platinum.setChecked(false);
                    no.setChecked(false);

                }
            }
        });
        diamond.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    str_check_box="Diamond";
                    diamond.setChecked(true);
                    silver.setChecked(false);
                    gold.setChecked(false);
                    bronze.setChecked(false);
                    platinum.setChecked(false);
                    no.setChecked(false);

                }
            }
        });
        platinum.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    str_check_box="Platinum";
                    platinum.setChecked(true);
                    silver.setChecked(false);
                    gold.setChecked(false);
                    diamond.setChecked(false);
                    bronze.setChecked(false);
                    no.setChecked(false);
                }
            }
        });

        no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    str_check_box="No";
                    no.setChecked(true);

                    silver.setChecked(false);
                    gold.setChecked(false);
                    diamond.setChecked(false);
                    bronze.setChecked(false);
                }
            }
        });


        yes_business.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    str_business_radio="Yes";
                    no_business.setChecked(false);
                }

            }
        });
        no_business.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    str_business_radio="No";
                    yes_business.setChecked(false);
                }

            }
        });


        business_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(Business_kyc_details.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        mcurrentDate.set(Calendar.YEAR, selectedyear);
                        mcurrentDate.set(Calendar.MONTH, selectedmonth);
                        mcurrentDate.set(Calendar.DAY_OF_MONTH,
                                selectedday);
                        sdf = new SimpleDateFormat(
                                getResources().getString(
                                        R.string.datecardformat),
                                Locale.US);
                        str_business_date=sdf.format(mcurrentDate.getTime());

                        business_date.setText(str_business_date);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker.show();
            }
        });

        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                str_business_minority = business_minority.getText().toString().trim();
                str_social_category = social_category.getText().toString().trim();
                str_business_registration_act =business_registration_act.getText().toString().trim();
                str_business_nature_association =business_nature_association.getText().toString().trim();
                str_business_presently_banking =business_presently_banking.getText().toString().trim();
                str_business_address_associate =business_address_associate.getText().toString().trim();
                str_business_associate_concerns =business_associate_concerns.getText().toString().trim();
                str_business_office_address =business_office_address.getText().toString().trim();
                str_business_registration_no =business_registration_no.getText().toString().trim();
                str_business_automobile =business_automobile.getText().toString().trim();
                str_business_proposed =business_proposed.getText().toString().trim();
                str_business_since =business_since.getText().toString().trim();
                str_business_exit =business_exit.getText().toString().trim();

//                business_minority.setError(null);
//                social_category.setError(null);
//                business_registration_act.setError(null);
//                business_nature_association.setError(null);
//                business_presently_banking.setError(null);
//                business_address_associate.setError(null);
//                business_associate_concerns.setError(null);
//                business_office_address.setError(null);
//                business_registration_no.setError(null);
//                business_date.setError(null);
//                business_automobile.setError(null);
//                business_proposed.setError(null);
//                business_since.setError(null);
//                business_exit.setError(null);

//                if (TextUtils.isEmpty(str_business_minority)) {
//                    Toast.makeText(Business_kyc_details.this, "If Minority is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_social_category)) {
//                    Toast.makeText(Business_kyc_details.this, "Social Category is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_business_registration_act)) {
//                    Toast.makeText(Business_kyc_details.this, "Registration Act is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_business_nature_association)) {
//                    Toast.makeText(Business_kyc_details.this, "Nature of Association is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_business_presently_banking)) {
//                    Toast.makeText(Business_kyc_details.this, "Presently Banking With is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_business_address_associate)) {
//                    Toast.makeText(Business_kyc_details.this, "Address of Associate Concerns is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_business_associate_concerns)) {
//                    Toast.makeText(Business_kyc_details.this, "Name of Associate Concerns is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_business_office_address)) {
//                    Toast.makeText(Business_kyc_details.this, "Registered Office Address is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_business_registration_no)) {
//                    Toast.makeText(Business_kyc_details.this, "Registration No. is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_business_date)) {
//                    Toast.makeText(Business_kyc_details.this, "Commencement Date is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_business_automobile)) {
//                    Toast.makeText(Business_kyc_details.this, "Automobile Service is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_business_proposed)) {
//                    Toast.makeText(Business_kyc_details.this, "Proposed is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_business_since)) {
//                    Toast.makeText(Business_kyc_details.this, "Since is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_business_exit)) {
//                    Toast.makeText(Business_kyc_details.this, "Existing is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (str_business_radio.equals("")) {
//                    Toast.makeText(Business_kyc_details.this, "Please check MSME unit is ZED rated", Toast.LENGTH_SHORT).show();
//                    cancel = true;
           //     }else


                    if (str_check_box.equals("")) {
                    Toast.makeText(Business_kyc_details.this, "Please select Relationship of Proprietors", Toast.LENGTH_SHORT).show();
                    cancel = true;
                }else {


                    kyc_Bussiness_Activity();
                    /*Intent intent=new Intent(Business_kyc_details.this,Banking_details_1.class);
                    intent.putExtra("str_nm_enterprise", str_nm_enterprise);
                    intent.putExtra("str_office_address", str_office_address);
                    intent.putExtra("str_category_application", str_category_application);
                    intent.putExtra("str_address_shop", str_address_shop);
                    intent.putExtra("str_business", str_business);
                    intent.putExtra("str_telephone_no", str_telephone_no);
                    intent.putExtra("str_email_edit", str_email_edit);
                    intent.putExtra("str_pan_no", str_pan_no);
                    intent.putExtra("str_enterprise_pan", str_enterprise_pan);
                    intent.putExtra("str_constitution", str_constitution);
                    intent.putExtra("str_other_constitution", str_other_constitution);
                    intent.putExtra("str_gst_no", str_gst_no);
                    intent.putExtra("str_calendar_edit", str_calendar_edit);
                    intent.putExtra("str_state_", str_state_);
                    intent.putExtra("str_district", str_district);
                    intent.putExtra("str_branch", str_branch);
                    intent.putExtra("form", "kyc");
                    startActivity(intent);
                    Business_kyc_details.this.finish();*/
                }
            }
        });
    }

    private void kyc_Bussiness_Activity() {
        networkCall.NetworkAPICall(ApiURL.kycBussinessActivity, true);
    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent(Business_kyc_details.this,KYC_personal_Details.class);
        startActivity(intent);
        Business_kyc_details.this.finish();
    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.kycBussinessActivity:
                ion = (Builders.Any.B) Ion.with(Business_kyc_details.this)
                        .load("POST", ApiURL.kycBussinessActivity)
                        .setHeader("token", ApiURL.token)
                        .setBodyParameter("user_id", userid)
                        .setBodyParameter("existing", str_business_exit)
                        .setBodyParameter("since", str_business_since)
                        .setBodyParameter("proposed", str_business_proposed)
                        .setBodyParameter("automobile_service", str_business_automobile)
                        .setBodyParameter("commencementDate", str_business_date)
                        .setBodyParameter("ZedRate", str_check_box)
                        .setBodyParameter("registerNo", str_business_registration_no)
                        .setBodyParameter("Residentialaddress", str_business_office_address)
                        .setBodyParameter("associateName", str_business_associate_concerns)
                        .setBodyParameter("associateAddress", str_business_address_associate)
                        .setBodyParameter("presentlyBanking", str_business_presently_banking)
                        .setBodyParameter("natureAssociation", str_business_nature_association)
                        .setBodyParameter("registrationAct", str_business_registration_act)
                        .setBodyParameter("socialCategory", str_social_category)
                        .setBodyParameter("minority",str_business_minority)
                        .setBodyParameter("selectDirectorbank", str_business_radio);

                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.kycBussinessActivity:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String status = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");

                    if (status.equals("true")) {

                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        str_business_exit=jsonObject1.getString("existing");
                        str_business_since=jsonObject1.getString("since");
                        str_business_proposed=jsonObject1.getString("propose");
                        str_business_automobile=jsonObject1.getString("automobile_service");
                        str_business_date=jsonObject1.getString("commencementDate");
                        str_check_box=jsonObject1.getString("ZedRate");
                        str_business_registration_no= jsonObject1.getString("registerNo");
                        str_business_office_address=jsonObject1.getString("registerAddress");
                        str_business_address_associate=jsonObject1.getString("associateName");
                        str_business_presently_banking=jsonObject1.getString("presentlyBanking");
                        str_business_nature_association=jsonObject1.getString("natureAssociation");
                        str_business_registration_act=jsonObject1.getString("registrationAct");
                        str_social_category=jsonObject1.getString("socialCategory");
                        str_business_minority=jsonObject1.getString("minority");
                        str_business_radio=jsonObject1.getString("selectDirectorbank");


                        business_exit.setText(str_business_exit);
                        business_since.setText(str_business_since);
                        business_proposed.setText(str_business_proposed);
                        business_automobile.setText(str_business_automobile);
                        business_date.setText(str_business_date);
                        business_registration_no.setText(str_business_registration_no);
                        business_office_address.setText(str_business_office_address);
                        business_address_associate.setText(str_business_address_associate);
                        business_presently_banking.setText(str_business_presently_banking);
                        business_nature_association.setText(str_business_nature_association);
                        business_registration_act.setText(str_business_registration_act);
                        social_category.setText(str_social_category);
                        business_minority.setText(str_business_minority);

                        if (str_business_radio.equals("Yes")){
                            yes_business.setChecked(true);
                        }else if (str_business_radio.equals("No")){
                            no_business.setChecked(true);
                        }

                        if (str_check_box.equals("Bronze")){
                            silver.setChecked(false);
                            gold.setChecked(false);
                            diamond.setChecked(false);
                            bronze.setChecked(true);
                            platinum.setChecked(false);

                        }else if (str_check_box.equals("Silver")){
                            silver.setChecked(true);
                            gold.setChecked(false);
                            diamond.setChecked(false);
                            bronze.setChecked(false);
                            platinum.setChecked(false);

                        } else if (str_check_box.equals("Gold")){
                            silver.setChecked(false);
                            gold.setChecked(true);
                            diamond.setChecked(false);
                            bronze.setChecked(false);
                            platinum.setChecked(false);

                        } else if (str_check_box.equals("Diamond")){
                            silver.setChecked(false);
                            gold.setChecked(false);
                            diamond.setChecked(true);
                            bronze.setChecked(false);
                            platinum.setChecked(false);

                        } else if (str_check_box.equals("Platinum")){
                            silver.setChecked(false);
                            gold.setChecked(false);
                            diamond.setChecked(false);
                            bronze.setChecked(false);
                            platinum.setChecked(true);
                        }

                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("str_business_exit", str_business_exit);
                        editor.putString("str_business_since", str_business_since);
                        editor.putString("str_business_proposed", str_business_proposed);
                        editor.putString("str_business_automobile", str_business_automobile);
                        editor.putString("str_business_date", str_business_date);
                        editor.putString("str_business_registration_no", str_business_registration_no);
                        editor.putString("str_business_office_address", str_business_office_address);
                        editor.putString("str_business_address_associate", str_business_address_associate);
                        editor.putString("str_business_presently_banking", str_business_presently_banking);
                        editor.putString("str_business_nature_association", str_business_nature_association);
                        editor.putString("str_business_registration_act", str_business_registration_act);
                        editor.putString("str_social_category", str_social_category);
                        editor.putString("str_business_minority", str_business_minority);
                        editor.putString("str_business_radio", str_business_radio);
                        editor.putString("str_check_box", str_check_box);

                        editor.commit();
                        Intent intent=new Intent(Business_kyc_details.this,Banking_details_1.class);
                        startActivity(intent);
                        finish();

                    } else {


                        String fail_status = jsonObject.getString("success");

                        if (fail_status.equals("false")) {


                            Toast.makeText(Business_kyc_details.this, msg, Toast.LENGTH_SHORT).show();
                        }
                        progress.dismiss();
                    }
                } catch (JSONException e1) {


                    Toast.makeText(Business_kyc_details.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        Toast.makeText(Business_kyc_details.this, jsonstring, Toast.LENGTH_SHORT).show();

    }
}