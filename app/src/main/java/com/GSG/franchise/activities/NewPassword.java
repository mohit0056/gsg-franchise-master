package com.gsg.franchise.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.gsg.franchise.R;
import com.gsg.franchise.utill.ApiURL;
import com.gsg.franchise.utill.NetworkCall;
import com.gsg.franchise.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONException;
import org.json.JSONObject;

public class NewPassword extends AppCompatActivity implements NetworkCall.MyNetworkCallBack {

    RelativeLayout submit;
    EditText passEDT, newPassEDT;
    String userid, mypass, mynewpass;
    Boolean cancel;
    SharedPreferences mSharedPreference;
    static final String pref_name = "GSG";
    Progress progress;
    NetworkCall networkCall;
    Boolean isLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_password);

        progress = new Progress(NewPassword.this);
        networkCall = new NetworkCall(NewPassword.this, NewPassword.this);
        mSharedPreference = getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        userid = (mSharedPreference.getString("id", ""));

        passEDT = findViewById(R.id.passEDT);
        newPassEDT = findViewById(R.id.newPassEDT);

        submit = findViewById(R.id.submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mypass = passEDT.getText().toString().trim();
                mynewpass = newPassEDT.getText().toString().trim();
                passEDT.setError(null);
                newPassEDT.setError(null);

                if (TextUtils.isEmpty(mypass)) {
                    passEDT.setError("Password is required");
                    cancel = true;

                } else if (TextUtils.isEmpty(mynewpass)) {
                    newPassEDT.setError("New Password is required");
                    cancel = true;
                }else if (!mynewpass.equals(mypass)) {
                    newPassEDT.setError("Both passwords are different... ");
                    cancel = true;
                }else if (!mypass.equals(mynewpass)) {
                    passEDT.setError("Both passwords are different... ");
                    cancel = true;
                } else {
                    changePassword();
                }
            }
        });

    }

    private void changePassword() {
        networkCall.NetworkAPICall(ApiURL.change_password, true);

    }

    public void back(View view) {
        super.onBackPressed();
    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.change_password:
                ion = (Builders.Any.B) Ion.with(NewPassword.this)
                        .load("POST", ApiURL.change_password)
                        .setHeader("token", ApiURL.token)
                        .setBodyParameter("id", userid)
                        .setBodyParameter("password", mypass)
                        .setBodyParameter("confirm_password", mynewpass);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.change_password:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");

                    if (succes.equals("true")) {


                        Toast.makeText(this, "Password Changed Successfully", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(NewPassword.this, MainActivity.class);
                        startActivity(intent);
                        finish();

                    } else {

                        String status_fail = jsonObject.getString("success");
                        if (status_fail.equals("false")) {
                            Toast.makeText(NewPassword.this, msg.toString(), Toast.LENGTH_SHORT).show();
                            progress.dismiss();
                        }

                    }
                } catch (JSONException e1) {


                    Toast.makeText(NewPassword.this, jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();
                }


                break;
        }

    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }
}