package com.gsg.franchise.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.gsg.franchise.R;
import com.gsg.franchise.utill.ApiURL;
import com.gsg.franchise.utill.NetworkCall;
import com.gsg.franchise.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONException;
import org.json.JSONObject;

public class ForgotPassword extends AppCompatActivity implements NetworkCall.MyNetworkCallBack{

    EditText mobileEDT;
    String myMobile, userid;
    RelativeLayout btnmobileLogin;
    Boolean isLogin;
    String OTP;
    RelativeLayout sendOtp;
    Boolean cancel;
    SharedPreferences mSharedPreference;
    static final String pref_name = "GSG";
    Progress progress;
    NetworkCall networkCall;

    final String regexStr = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);


        progress = new Progress(ForgotPassword.this);
        networkCall = new NetworkCall(ForgotPassword.this, ForgotPassword.this);
        mSharedPreference = getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        userid = (mSharedPreference.getString("id", ""));

        mobileEDT = (EditText) findViewById(R.id.mobileEDT);
        sendOtp = findViewById(R.id.sendOtp);


        sendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myMobile = mobileEDT.getText().toString().trim();
                mobileEDT.setError(null);

                 if (TextUtils.isEmpty(myMobile)) {
                    mobileEDT.setError("Phone number is required");
                    cancel = true;
                }

//                 else if (!myMobile.matches(regexStr)) {
//                     mobileEDT.setError("Please Enter Valid Phone no.");
//                     cancel = true;
//                 }

                 else{
//                     Intent intent = new Intent(ForgotPassword.this, OTP.class);
//                     intent.putExtra("myMobile", myMobile);
//                     startActivity(intent);
//                     finish();
                   sendOTP();
                 }
            }
        });

    }

    private void sendOTP() {

        networkCall.NetworkAPICall(ApiURL.send_otp, true);

    }

    public void back(View view) {
        super.onBackPressed();
    }



    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.send_otp:
                ion = (Builders.Any.B) Ion.with(ForgotPassword.this)
                        .load("POST", ApiURL.send_otp)
                        .setHeader("token", ApiURL.token)
                        .setBodyParameter("id", userid)
                        .setBodyParameter("mobile", myMobile);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.send_otp:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");

                    if (succes.equals("true")) {

                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        OTP = jsonObject1.getString("otp");
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("otp", OTP);

                        editor.commit();

                        Toast.makeText(this, "Your OTP is"+" "+OTP, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(ForgotPassword.this, OTP.class);
                        startActivity(intent);
                        finish();

                    } else {

                        String status_fail = jsonObject.getString("success");
                        if (status_fail.equals("false")) {
                   Toast.makeText(ForgotPassword.this, ""+msg, Toast.LENGTH_SHORT).show();

//                            Toast.makeText(ForgotPassword.this, "Otp set Successfully", Toast.LENGTH_SHORT).show();
                            progress.dismiss();
                        }

                    }
                } catch (JSONException e1) {
//                    Toast.makeText(ForgotPassword.this, "Otp set Successfully", Toast.LENGTH_SHORT).show();
                }


                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }
}