package com.gsg.franchise.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Toast;

import com.av.smoothviewpager.Smoolider.SmoothViewpager;
import com.google.gson.Gson;
import com.gsg.franchise.R;

import com.gsg.franchise.adapter.StaticFranchiseAdapter;
import com.gsg.franchise.models.StaticFranchiseModel;
import com.gsg.franchise.utill.ApiURL;
import com.gsg.franchise.utill.NetworkCall;
import com.gsg.franchise.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class Static_Franchise extends AppCompatActivity implements NetworkCall.MyNetworkCallBack, StaticFranchiseAdapter.pageonClick {
    ViewPager viewPager;
    Progress progress;
    NetworkCall networkCall;
    Timer timer;
    SmoothViewpager viewPagerBanners;

    ArrayList<StaticFranchiseModel> arrStaticFranchiseModel = new ArrayList<>();
    StaticFranchiseAdapter staticFranchiseAdapter;
    StaticFranchiseModel staticFranchiseModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static_franchise);

        progress = new Progress(Static_Franchise.this);
        networkCall = new NetworkCall(Static_Franchise.this, Static_Franchise.this);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (!prefs.getBoolean("firstTimee", false)) {
            // <---- run your one time code here
            timer = new Timer();
            timer.scheduleAtFixedRate(new MyTimerTask(), 1000, 500);
            // mark first time has ran.
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("firstTimee", true);
            editor.commit();

        }
        getData();
    }

    private void getData() {

        networkCall.NetworkAPICall(ApiURL.Static_Franchise, true);

    }
    public class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            Static_Franchise.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (viewPagerBanners.getCurrentItem() == 0) {
                        viewPagerBanners.setCurrentItem(1);
                    } else if (viewPagerBanners.getCurrentItem() == 1) {
                        viewPagerBanners.setCurrentItem(0);
                        timer.cancel();

                    }

                }
            });
        }
    }



    public void back(View view) {
        super.onBackPressed();
    }

    public void zoomImage(View view) {


    }

    @Override
    public void viewPageImageClick(String page, int position) {

    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.Static_Franchise:
                ion = (Builders.Any.B) Ion.with(Static_Franchise.this)
                        .load("POST", ApiURL.Static_Franchise)
                        .setHeader("token", ApiURL.token)
                        .setBodyParameter("type", "static")
                ;
                break;


        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.Static_Franchise:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String success = jsonObject.getString("success");
                    if (success.equals("true")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            staticFranchiseModel = new Gson().fromJson(jsonArray.optJSONObject(i).toString(), StaticFranchiseModel.class);
                            arrStaticFranchiseModel.add(staticFranchiseModel);
                        }

                        viewPagerBanners =  findViewById(R.id.viewpager);

                        viewPagerBanners.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                            @Override
                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                                for (int i = 0; i < arrStaticFranchiseModel.size(); i++) {

                                }

                            }

                            @Override
                            public void onPageSelected(int position) {

                            }

                            @Override
                            public void onPageScrollStateChanged(int state) {

                            }
                        });

                        StaticFranchiseAdapter staticFranchiseAdapter = new StaticFranchiseAdapter(this, arrStaticFranchiseModel, this, "offer");
                        viewPagerBanners.setAdapter(staticFranchiseAdapter);
                        staticFranchiseAdapter.notifyDataSetChanged();

                    } else {

                        progress.dismiss();
                    }
                } catch (JSONException e1) {
                    Toast.makeText(Static_Franchise.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;


        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }
}