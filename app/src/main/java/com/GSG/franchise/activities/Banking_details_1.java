package com.gsg.franchise.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.gsg.franchise.R;
import com.gsg.franchise.utill.ApiURL;
import com.gsg.franchise.utill.NetworkCall;
import com.gsg.franchise.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONException;
import org.json.JSONObject;

public class Banking_details_1 extends AppCompatActivity implements NetworkCall.MyNetworkCallBack {
     Button next_btn;
    ImageView back_image;
    Boolean cancel;
    NetworkCall networkCall;
    Progress progress;
    SharedPreferences mSharedPreference;
    TextInputEditText baking_credit,limit_edit,outstanding_edit,presently_banking,securities_banking,rate_interest_banking,repayment_banking,
                      limit_cash,out_standing_cash,presently_cash,securities_cash,rate_cash,repayment_cash,
            limit_term,out_standing_term,presently_term,securities_term,rate_term,repayment_term,
            limit_lg_bg,out_standing_lg_bg,presently_lg_bg,securities_lg_bg,rate_lg_bg,repayment_lg_bg,
            limit_other,out_standing_other,presently_other,securities_other,rate_other,repayment_other,
            limit_total,out_standing_total,presently_total,securities_total,rate_total,repayment_total,customer_id,
            amount_cash,amount_purpose,amount_primary_security,amount_security_offered,amount_team_loan,purpose_term_loan,primary_security_term_loan,security_offered_term_loan,
            amount_lg_bg,purpose_lg_bg,primary_security_lg_Bg,security_offered_lg_bg,
            amount_other,purpose_other,primary_Security_other,security_offered_other,
            amount_total,purpose_total,primary_Security_total,security_offered_total,expected_employment;

    String userid,str_baking_credit="",str_limit_edit,str_outstanding_edit,str_presently_banking,str_securities_banking,str_rate_interest_banking,str_repayment_banking,
           str_limit_cash,str_out_standing_cash,str_presently_cash,str_securities_cash,str_rate_cash,str_repayment_cash,
           str_limit_term,str_out_standing_term,str_presently_term,str_securities_term,str_rate_term,str_repayment_term,
           str_limit_lg_bg,str_out_standing_lg_bg,str_presently_lg_bg,str_securities_lg_bg,str_rate_lg_bg,str_repayment_lg_bg,
            str_limit_other,str_out_standing_other,str_presently_other,str_securities_other,str_rate_other,str_repayment_other,
            str_limit_total,str_out_standing_total,str_presently_total,str_securities_total,str_rate_total,str_repayment_total,str_customer_id,
            str_amount_cash,str_amount_purpose,str_amount_primary_security,str_amount_security_offered,str_amount_team_loan,str_purpose_term_loan,str_primary_security_term_loan,str_security_offered_term_loan,
            str_amount_lg_bg,str_purpose_lg_bg,str_primary_security_lg_Bg,str_security_offered_lg_bg,
            str_amount_other,str_purpose_other,str_primary_Security_other,str_security_offered_other,
           str_amount_total,str_purpose_total,str_primary_Security_total,str_security_offered_total,str_expected_employment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banking_details_1);

        progress = new Progress(Banking_details_1.this);
        networkCall = new NetworkCall(Banking_details_1.this, Banking_details_1.this);

        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        userid =mSharedPreference.getString("id", "");

        str_baking_credit =mSharedPreference.getString("str_baking_credit", "");
        str_securities_term =mSharedPreference.getString("str_securities_term", "");
        str_limit_edit =mSharedPreference.getString("str_limit_edit", "");
        str_outstanding_edit =mSharedPreference.getString("str_outstanding_edit", "");
        str_presently_banking =mSharedPreference.getString("str_presently_banking", "");
        str_securities_banking =mSharedPreference.getString("str_securities_banking", "");
        str_rate_interest_banking =mSharedPreference.getString("str_rate_interest_banking", "");
        str_repayment_banking =mSharedPreference.getString("str_repayment_banking", "");
        str_limit_cash =mSharedPreference.getString("str_limit_cash", "");
        str_out_standing_cash =mSharedPreference.getString("str_out_standing_cash", "");
        str_presently_cash =mSharedPreference.getString("str_presently_cash", "");
        str_securities_cash =mSharedPreference.getString("str_securities_cash", "");
        str_rate_cash =mSharedPreference.getString("str_rate_cash", "");
        str_repayment_cash =mSharedPreference.getString("str_repayment_cash", "");
        str_limit_term =mSharedPreference.getString("str_limit_term", "");
        str_out_standing_term =mSharedPreference.getString("str_out_standing_term", "");
        str_presently_term =mSharedPreference.getString("str_presently_term", "");
        str_rate_term =mSharedPreference.getString("str_rate_term", "");
        str_repayment_term =mSharedPreference.getString("str_repayment_term", "");
        str_limit_lg_bg =mSharedPreference.getString("str_limit_lg_bg", "");
        str_out_standing_lg_bg =mSharedPreference.getString("str_out_standing_lg_bg", "");
        str_presently_lg_bg =mSharedPreference.getString("str_presently_lg_bg", "");
        str_securities_lg_bg =mSharedPreference.getString("str_securities_lg_bg", "");
        str_rate_lg_bg =mSharedPreference.getString("str_rate_lg_bg", "");
        str_repayment_lg_bg =mSharedPreference.getString("str_repayment_lg_bg", "");
        str_limit_other =mSharedPreference.getString("str_limit_other", "");
        str_out_standing_other =mSharedPreference.getString("str_out_standing_other", "");
        str_presently_other =mSharedPreference.getString("str_presently_other", "");
        str_securities_other =mSharedPreference.getString("str_securities_other", "");
        str_rate_other =mSharedPreference.getString("str_rate_other", "");
        str_repayment_other =mSharedPreference.getString("str_repayment_other", "");
        str_limit_total =mSharedPreference.getString("str_limit_total", "");
        str_out_standing_total =mSharedPreference.getString("str_out_standing_total", "");
        str_presently_total =mSharedPreference.getString("str_presently_total", "");
        str_securities_total =mSharedPreference.getString("str_securities_total", "");
        str_rate_total =mSharedPreference.getString("str_rate_total", "");
        str_repayment_total =mSharedPreference.getString("str_repayment_total", "");
        str_customer_id =mSharedPreference.getString("str_customer_id", "");
        str_amount_cash =mSharedPreference.getString("str_amount_cash", "");
        str_amount_purpose =mSharedPreference.getString("str_amount_purpose", "");
        str_amount_primary_security =mSharedPreference.getString("str_amount_primary_security", "");
        str_amount_security_offered =mSharedPreference.getString("str_amount_security_offered", "");
        str_amount_team_loan =mSharedPreference.getString("str_amount_team_loan", "");
        str_purpose_term_loan =mSharedPreference.getString("str_purpose_term_loan", "");
        str_primary_security_term_loan =mSharedPreference.getString("str_primary_security_term_loan", "");
        str_security_offered_term_loan =mSharedPreference.getString("str_security_offered_term_loan", "");
        str_amount_lg_bg =mSharedPreference.getString("str_amount_lg_bg", "");
        str_purpose_lg_bg =mSharedPreference.getString("str_purpose_lg_bg", "");
        str_primary_security_lg_Bg =mSharedPreference.getString("str_primary_security_lg_Bg", "");
        str_security_offered_lg_bg =mSharedPreference.getString("str_security_offered_lg_bg", "");
        str_amount_other =mSharedPreference.getString("str_amount_other", "");
        str_purpose_other =mSharedPreference.getString("str_purpose_other", "");
        str_primary_Security_other =mSharedPreference.getString("str_primary_Security_other", "");
        str_security_offered_other =mSharedPreference.getString("str_security_offered_other", "");
        str_expected_employment =mSharedPreference.getString("str_expected_employment", "");

        next_btn=findViewById(R.id.next_btn);
        back_image=findViewById(R.id.back_image);
        baking_credit=findViewById(R.id.baking_credit);
        limit_edit=findViewById(R.id.limit_edit);
        outstanding_edit=findViewById(R.id.outstanding_edit);
        presently_banking=findViewById(R.id.presently_banking);
        securities_banking=findViewById(R.id.securities_banking);
        rate_interest_banking=findViewById(R.id.rate_interest_banking);
        repayment_banking=findViewById(R.id.repayment_banking);

        limit_cash=findViewById(R.id.limit_cash);
        out_standing_cash=findViewById(R.id.out_standing_cash);
        presently_cash=findViewById(R.id.presently_cash);
        securities_cash=findViewById(R.id.securities_cash);
        rate_cash=findViewById(R.id.rate_cash);
        repayment_cash=findViewById(R.id.repayment_cash);

        limit_term=findViewById(R.id.limit_term);
        out_standing_term=findViewById(R.id.out_standing_term);
        presently_term=findViewById(R.id.presently_term);
        securities_term=findViewById(R.id.securities_term);
        rate_term=findViewById(R.id.rate_term);
        repayment_term=findViewById(R.id.repayment_term);

        limit_lg_bg=findViewById(R.id.limit_lg_bg);
        out_standing_lg_bg=findViewById(R.id.out_standing_lg_bg);
        presently_lg_bg=findViewById(R.id.presently_lg_bg);
        securities_lg_bg=findViewById(R.id.securities_lg_bg);
        rate_lg_bg=findViewById(R.id.rate_lg_bg);
        repayment_lg_bg=findViewById(R.id.repayment_lg_bg);


        limit_other=findViewById(R.id.limit_other);
        out_standing_other=findViewById(R.id.out_standing_other);
        presently_other=findViewById(R.id.presently_other);
        securities_other=findViewById(R.id.securities_other);
        rate_other=findViewById(R.id.rate_other);
        repayment_other=findViewById(R.id.repayment_other);

        limit_total=findViewById(R.id.limit_total);
        out_standing_total=findViewById(R.id.out_standing_total);
        presently_total=findViewById(R.id.presently_total);
        securities_total=findViewById(R.id.securities_total);
        rate_total=findViewById(R.id.rate_total);
        repayment_total=findViewById(R.id.repayment_total);
        customer_id=findViewById(R.id.customer_id);

        amount_cash=findViewById(R.id.amount_cash);
        amount_purpose=findViewById(R.id.amount_purpose);
        amount_primary_security=findViewById(R.id.amount_primary_security);
        amount_security_offered=findViewById(R.id.amount_security_offered);
        amount_team_loan=findViewById(R.id.amount_team_loan);
        purpose_term_loan=findViewById(R.id.purpose_term_loan);
        primary_security_term_loan=findViewById(R.id.primary_security_term_loan);
        security_offered_term_loan=findViewById(R.id.security_offered_term_loan);

        amount_lg_bg=findViewById(R.id.amount_lg_bg);
        purpose_lg_bg=findViewById(R.id.purpose_lg_bg);
        primary_security_lg_Bg=findViewById(R.id.primary_security_lg_Bg);
        security_offered_lg_bg=findViewById(R.id.security_offered_lg_bg);

        amount_other=findViewById(R.id.amount_other);
        purpose_other=findViewById(R.id.purpose_other);
        primary_Security_other=findViewById(R.id.primary_Security_other);
        security_offered_other=findViewById(R.id.security_offered_other);

        amount_total=findViewById(R.id.amount_total);
        purpose_total=findViewById(R.id.purpose_total);
        primary_Security_total=findViewById(R.id.primary_Security_total);
        security_offered_total=findViewById(R.id.security_offered_total);
        expected_employment=findViewById(R.id.expected_employment);


        if (!str_baking_credit.equals("")){
            baking_credit.setText(str_baking_credit);
        }
        if (!str_limit_edit.equals("")){
            limit_edit.setText(str_limit_edit);
        }
        if (!str_outstanding_edit.equals("")){
            outstanding_edit.setText(str_outstanding_edit);
        }
        if (!str_presently_banking.equals("")){
            presently_banking.setText(str_presently_banking);
        }
        if (!str_securities_banking.equals("")){
            securities_banking.setText(str_securities_banking);
        }
        if (!str_rate_interest_banking.equals("")){
            rate_interest_banking.setText(str_rate_interest_banking);
        }
        if (!str_repayment_banking.equals("")){
            repayment_banking.setText(str_repayment_banking);
        }

        if (!str_limit_cash.equals("")){
            limit_cash.setText(str_limit_cash);
        }
        if (!str_out_standing_cash.equals("")){
            out_standing_cash.setText(str_out_standing_cash);
        }
        if (!str_presently_cash.equals("")){
            presently_cash.setText(str_presently_cash);
        }
        if (!str_securities_cash.equals("")){
            securities_cash.setText(str_securities_cash);
        }
        if (!str_rate_cash.equals("")){
            rate_cash.setText(str_rate_cash);
        }
        if (!str_repayment_cash.equals("")){
            repayment_cash.setText(str_repayment_cash);;
        }
        if (!str_limit_term.equals("")){
            limit_term.setText(str_limit_term);
        }
        if (!str_out_standing_term.equals("")){
            out_standing_term.setText(str_out_standing_term);
        }
        if (!str_presently_term.equals("")){
            presently_term.setText(str_presently_term);
        }
        if (!str_securities_term.equals("")){
            securities_term.setText(str_securities_term);
        }
        if (!str_rate_term.equals("")){
            rate_term.setText(str_rate_term);
        }
        if (!str_repayment_term.equals("")){
            repayment_term.setText(str_repayment_term);
        }
        if (!str_limit_lg_bg.equals("")){
            limit_lg_bg.setText(str_limit_lg_bg);
        }
        if (!str_out_standing_lg_bg.equals("")){
            out_standing_lg_bg.setText(str_out_standing_lg_bg);
        }

        if (!str_presently_lg_bg.equals("")){
            presently_lg_bg.setText(str_presently_lg_bg);
        }

        if (!str_securities_lg_bg.equals("")){
            securities_lg_bg.setText(str_securities_lg_bg);
        }
        if (!str_rate_lg_bg.equals("")){
            rate_lg_bg.setText(str_rate_lg_bg);
        }
        if (!str_repayment_lg_bg.equals("")){
            repayment_lg_bg.setText(str_repayment_lg_bg);
        }

        if (!str_limit_other.equals("")){
            limit_other.setText(str_limit_other);
        }
        if (!str_out_standing_other.equals("")){
            out_standing_other.setText(str_out_standing_other);
        }
        if (!str_presently_other.equals("")){
            presently_other.setText(str_presently_other);
        }
        if (!str_securities_other.equals("")){
            securities_other.setText(str_securities_other);
        }

        if (!str_rate_other.equals("")){
            rate_other.setText(str_rate_other);
        }
        if (!str_repayment_other.equals("")){
            repayment_other.setText(str_repayment_other);
        }

        if (!str_limit_total.equals("")){
            limit_total.setText(str_limit_total);
        }
        if (!str_out_standing_total.equals("")){
            out_standing_total.setText(str_out_standing_total);
        }

        if (!str_presently_total.equals("")){
            presently_total.setText(str_presently_total);
        }
        if (!str_securities_total.equals("")){
            securities_total.setText(str_securities_total);
        }
        if (!str_rate_total.equals("")){
            rate_total.setText(str_rate_total);
        }

        if (!str_repayment_total.equals("")){
            repayment_total.setText(str_repayment_total);
        }
        if (!str_customer_id.equals("")){
            customer_id.setText(str_customer_id);
        }

        if (!str_amount_cash.equals("")){
            amount_cash.setText(str_amount_cash);
        }
        if (!str_amount_purpose.equals("")){
            amount_purpose.setText(str_amount_purpose);
        }
        if (!str_amount_primary_security.equals("")){
            amount_primary_security.setText(str_amount_primary_security);
        }
        if (!str_amount_security_offered.equals("")){
            amount_security_offered.setText(str_amount_security_offered);
        }

        if (!str_amount_team_loan.equals("")){
            amount_team_loan.setText(str_amount_team_loan);
        }

        if (!str_purpose_term_loan.equals("")){
            purpose_term_loan.setText(str_purpose_term_loan);
        }
        if (!str_primary_security_term_loan.equals("")){
            primary_security_term_loan.setText(str_primary_security_term_loan);
        }
        if (!str_security_offered_term_loan.equals("")){
            security_offered_term_loan.setText(str_security_offered_term_loan);
        }
        if (!str_amount_lg_bg.equals("")){
            amount_lg_bg.setText(str_amount_lg_bg);
        }
        if (!str_purpose_lg_bg.equals("")){
            purpose_lg_bg.setText(str_purpose_lg_bg);
        }
        if (!str_primary_security_lg_Bg.equals("")){
            primary_security_lg_Bg.setText(str_primary_security_lg_Bg);
        }
        if (!str_security_offered_lg_bg.equals("")){
            security_offered_lg_bg.setText(str_security_offered_lg_bg);
        }
        if (!str_amount_other.equals("")){
            amount_other.setText(str_amount_other);
        }
        if (!str_purpose_other.equals("")){
            purpose_other.setText(str_purpose_other);
        }
        if (!str_primary_Security_other.equals("")){
            primary_Security_other.setText(str_primary_Security_other);
        }
        if (!str_security_offered_other.equals("")){
            security_offered_other.setText(str_security_offered_other);
        }
        if (!str_expected_employment.equals("")){
            expected_employment.setText(str_expected_employment);
        }

        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                str_baking_credit = baking_credit.getText().toString().trim();
                str_limit_edit = limit_edit.getText().toString().trim();
                str_outstanding_edit =outstanding_edit.getText().toString().trim();
                str_presently_banking =presently_banking.getText().toString().trim();
                str_securities_banking =securities_banking.getText().toString().trim();
                str_rate_interest_banking=rate_interest_banking.getText().toString().trim();
                str_repayment_banking =repayment_banking.getText().toString().trim();
                str_limit_cash =limit_cash.getText().toString().trim();
                str_out_standing_cash =out_standing_cash.getText().toString().trim();
                str_presently_cash =presently_cash.getText().toString().trim();
                str_securities_cash =securities_cash.getText().toString().trim();
                str_rate_cash =rate_cash.getText().toString().trim();
                str_repayment_cash =repayment_cash.getText().toString().trim();
                str_limit_term =limit_term.getText().toString().trim();
                str_out_standing_term =out_standing_term.getText().toString().trim();
                str_presently_term =presently_term.getText().toString().trim();
                str_securities_term =securities_term.getText().toString().trim();
                str_rate_term =rate_term.getText().toString().trim();
                str_repayment_term =repayment_term.getText().toString().trim();
                str_limit_lg_bg=limit_lg_bg.getText().toString().trim();
                str_out_standing_lg_bg=out_standing_lg_bg.getText().toString().trim();
                str_presently_lg_bg=presently_lg_bg.getText().toString().trim();
                str_securities_lg_bg=securities_lg_bg.getText().toString().trim();
                str_rate_lg_bg=rate_lg_bg.getText().toString().trim();
                str_repayment_lg_bg=repayment_lg_bg.getText().toString().trim();
                str_limit_other=limit_other.getText().toString().trim();
                str_out_standing_other=out_standing_other.getText().toString().trim();
                str_presently_other=presently_other.getText().toString().trim();
                str_securities_other=securities_other.getText().toString().trim();
                str_rate_other=rate_other.getText().toString().trim();
                str_repayment_other=repayment_other.getText().toString().trim();
                str_limit_total=limit_total.getText().toString().trim();
                str_out_standing_total=out_standing_total.getText().toString().trim();
                str_presently_total=presently_total.getText().toString().trim();
                str_securities_total=securities_total.getText().toString().trim();
                str_rate_total=rate_total.getText().toString().trim();
                str_repayment_total=repayment_total.getText().toString().trim();
                str_customer_id=customer_id.getText().toString().trim();
                str_amount_cash=amount_cash.getText().toString().trim();
                str_amount_purpose=amount_purpose.getText().toString().trim();
                str_amount_primary_security=amount_primary_security.getText().toString().trim();
                str_amount_security_offered=amount_security_offered.getText().toString().trim();
                str_amount_team_loan=amount_team_loan.getText().toString().trim();
                str_purpose_term_loan=purpose_term_loan.getText().toString().trim();
                str_primary_security_term_loan=primary_security_term_loan.getText().toString().trim();
                str_security_offered_term_loan=security_offered_term_loan.getText().toString().trim();
                str_amount_lg_bg=amount_lg_bg.getText().toString().trim();
                str_purpose_lg_bg=purpose_lg_bg.getText().toString().trim();
                str_primary_security_lg_Bg=primary_security_lg_Bg.getText().toString().trim();
                str_security_offered_lg_bg=security_offered_lg_bg.getText().toString().trim();
                str_amount_other=amount_other.getText().toString().trim();
                str_purpose_other=purpose_other.getText().toString().trim();
                str_primary_Security_other=primary_Security_other.getText().toString().trim();
                str_security_offered_other=security_offered_other.getText().toString().trim();
                str_amount_total=amount_total.getText().toString().trim();
                str_purpose_total=purpose_total.getText().toString().trim();
                str_primary_Security_total=primary_Security_total.getText().toString().trim();
                str_security_offered_total=security_offered_total.getText().toString().trim();
                str_expected_employment=expected_employment.getText().toString().trim();

//                baking_credit.setError(null);
//                limit_edit.setError(null);
//                outstanding_edit.setError(null);
//                presently_banking.setError(null);
//                securities_banking.setError(null);
//                rate_interest_banking.setError(null);
//                repayment_banking.setError(null);
//                limit_cash.setError(null);
//                out_standing_cash.setError(null);
//                presently_cash.setError(null);
//                securities_cash.setError(null);
//                rate_cash.setError(null);
//                repayment_cash.setError(null);
//                limit_term.setError(null);
//                out_standing_term.setError(null);
//                presently_term.setError(null);
//                securities_term.setError(null);
//                rate_term.setError(null);
//                repayment_term.setError(null);
//                limit_lg_bg.setError(null);
//                out_standing_lg_bg.setError(null);
//                presently_lg_bg.setError(null);
//                securities_lg_bg.setError(null);
//                rate_lg_bg.setError(null);
//                repayment_lg_bg.setError(null);
//                limit_other.setError(null);
//                out_standing_other.setError(null);
//                presently_other.setError(null);
//                securities_other.setError(null);
//                rate_other.setError(null);
//                repayment_other.setError(null);
//                limit_total.setError(null);
//                out_standing_total.setError(null);
//                presently_total.setError(null);
//                securities_total.setError(null);
//                rate_total.setError(null);
//                repayment_total.setError(null);
//                customer_id.setError(null);
//                amount_cash.setError(null);
//                amount_purpose.setError(null);
//                amount_primary_security.setError(null);
//                amount_security_offered.setError(null);
//                amount_team_loan.setError(null);
//                purpose_term_loan.setError(null);
//                primary_security_term_loan.setError(null);
//                security_offered_term_loan.setError(null);
//                 amount_lg_bg.setError(null);
//                purpose_lg_bg.setError(null);
//                primary_security_lg_Bg.setError(null);
//                security_offered_lg_bg.setError(null);
//                amount_other.setError(null);
//                purpose_other.setError(null);
//                primary_Security_other.setError(null);
//                security_offered_other.setError(null);
//                amount_total.setError(null);
//                purpose_total.setError(null);
//                primary_Security_total.setError(null);
//                security_offered_total.setError(null);
//                expected_employment.setError(null);

//                if (TextUtils.isEmpty(str_baking_credit)) {
//                    Toast.makeText(Banking_details_1.this, "Enter Here is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_limit_edit)) {
//                    Toast.makeText(Banking_details_1.this, "Limit (in Lakh) required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_outstanding_edit)) {
//                    Toast.makeText(Banking_details_1.this, "Outstanding as on is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_presently_banking)) {
//                    Toast.makeText(Banking_details_1.this, "Presently Banking With is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_securities_banking)) {
//                    Toast.makeText(Banking_details_1.this, "Securities is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_rate_interest_banking)) {
//                    Toast.makeText(Banking_details_1.this, "Rate of Interest is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_repayment_banking)) {
//                    Toast.makeText(Banking_details_1.this, "Repayment Terms is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_limit_cash)) {
//                    Toast.makeText(Banking_details_1.this, "Limit (in lakh) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_out_standing_cash)) {
//                    Toast.makeText(Banking_details_1.this, "Outstanding as on is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_presently_cash)) {
//                    Toast.makeText(Banking_details_1.this, "Presently Banking With is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_securities_cash)) {
//                    Toast.makeText(Banking_details_1.this, "Securities is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_rate_cash)) {
//                    Toast.makeText(Banking_details_1.this, "Rate of Interest is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_repayment_cash)) {
//                    Toast.makeText(Banking_details_1.this, "Repayment Terms is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_limit_term)) {
//                    Toast.makeText(Banking_details_1.this, "Limit (in lakh) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_out_standing_term)) {
//                    Toast.makeText(Banking_details_1.this, "Outstanding as on is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_presently_term)) {
//                    Toast.makeText(Banking_details_1.this, "Presently Banking With is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_securities_term)) {
//                    Toast.makeText(Banking_details_1.this, "Securities is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_rate_term)) {
//                    Toast.makeText(Banking_details_1.this, "Rate of Interest is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_repayment_term)) {
//                    Toast.makeText(Banking_details_1.this, "Repayment Terms is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_limit_lg_bg)) {
//                    Toast.makeText(Banking_details_1.this, "Limit (in lakh) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_out_standing_lg_bg)) {
//                    Toast.makeText(Banking_details_1.this, "Outstanding as on is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_presently_lg_bg)) {
//                    Toast.makeText(Banking_details_1.this, "Presently Banking With is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_securities_lg_bg)) {
//                    Toast.makeText(Banking_details_1.this, "Securities is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_rate_lg_bg)) {
//                    Toast.makeText(Banking_details_1.this, "Rate of Interest is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_repayment_lg_bg)) {
//                    Toast.makeText(Banking_details_1.this, "Repayment Terms is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_limit_other)) {
//                    Toast.makeText(Banking_details_1.this, "Limit (in lakh) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }
//                else if (TextUtils.isEmpty(str_out_standing_other)) {
//                    Toast.makeText(Banking_details_1.this, "Outstanding as on is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }
//                else if (TextUtils.isEmpty(str_presently_other)) {
//                    Toast.makeText(Banking_details_1.this, "Presently Banking With is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }
//                else if (TextUtils.isEmpty(str_securities_other)) {
//                    Toast.makeText(Banking_details_1.this, "Securities is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }
//                else if (TextUtils.isEmpty(str_rate_other)) {
//                    Toast.makeText(Banking_details_1.this, "Rate of Interest is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }
//                else if (TextUtils.isEmpty(str_repayment_other)) {
//                    Toast.makeText(Banking_details_1.this, "Repayment Terms is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                } else if (TextUtils.isEmpty(str_limit_total)) {
//                    Toast.makeText(Banking_details_1.this, "Limit (in lakh) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_out_standing_total)) {
//                    Toast.makeText(Banking_details_1.this, "Outstanding as on is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_presently_total)) {
//                    Toast.makeText(Banking_details_1.this, "Presently Banking With is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_securities_total)) {
//                    Toast.makeText(Banking_details_1.this, "Securities is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_rate_total)) {
//                    Toast.makeText(Banking_details_1.this, "Rate of Interest is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_repayment_total)) {
//                    Toast.makeText(Banking_details_1.this, "Repayment Terms is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_customer_id)) {
//                    Toast.makeText(Banking_details_1.this, "Customer ID is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_amount_cash)) {
//                    Toast.makeText(Banking_details_1.this, "Amount (in Lakh) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_amount_purpose)) {
//                    Toast.makeText(Banking_details_1.this, "Purpose for which Required is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_amount_primary_security)) {
//                    Toast.makeText(Banking_details_1.this, "Primary Security (Details with approx. value to be mentioned) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_amount_security_offered)) {
//                    Toast.makeText(Banking_details_1.this, "Whether Collateral Security Offered is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_amount_team_loan)) {
//                    Toast.makeText(Banking_details_1.this, "Amount (in Lakh) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_purpose_term_loan)) {
//                    Toast.makeText(Banking_details_1.this, "Purpose for which Required is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_primary_security_term_loan)) {
//                    Toast.makeText(Banking_details_1.this, "Primary Security (Details with approx. value to be mentioned) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_security_offered_term_loan)) {
//                    Toast.makeText(Banking_details_1.this, "Whether Collateral Security Offered is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_amount_lg_bg)) {
//                    Toast.makeText(Banking_details_1.this, "Amount (in Lakh) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }
//                else if (TextUtils.isEmpty(str_purpose_lg_bg)) {
//                    Toast.makeText(Banking_details_1.this, "Purpose for which Required is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }
//                else if (TextUtils.isEmpty(str_primary_security_lg_Bg)) {
//                    Toast.makeText(Banking_details_1.this, "Primary Security  is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }
//                else if (TextUtils.isEmpty(str_security_offered_lg_bg)) {
//                    Toast.makeText(Banking_details_1.this, "Whether Collateral Security Offered  is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_amount_other)) {
//                    Toast.makeText(Banking_details_1.this, "Amount (in Lakh) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_purpose_other)) {
//                    Toast.makeText(Banking_details_1.this, "Purpose for which Required is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_primary_Security_other)) {
//                    Toast.makeText(Banking_details_1.this, "Primary Security (Details with approx. value to be mentioned) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_security_offered_other)) {
//                    Toast.makeText(Banking_details_1.this, "Whether Collateral Security Offered (If yes, then provide details on column-23 20)(Yes/No) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }
//                else if (TextUtils.isEmpty(str_amount_total)) {
//                    Toast.makeText(Banking_details_1.this, "Amount (in Lakh) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }
//                else if (TextUtils.isEmpty(str_purpose_total)) {
//                    Toast.makeText(Banking_details_1.this, "Purpose for which Required is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }
//                else if (TextUtils.isEmpty(str_primary_Security_total)) {
//                    Toast.makeText(Banking_details_1.this, "Primary Security (Details with approx. value to be mentioned) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }
//                else if (TextUtils.isEmpty(str_security_offered_other)) {
//                    Toast.makeText(Banking_details_1.this, "Whether Collateral Security Offered (If yes, then provide details on column-23 20)(Yes/No) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_expected_employment)) {
//                    Toast.makeText(Banking_details_1.this, "Expected Employment is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else {
                    baking_credit.setText(str_baking_credit);
                    limit_edit.setText(str_limit_edit);
                    outstanding_edit.setText(str_outstanding_edit);
                    presently_banking.setText(str_presently_banking);
                    securities_banking.setText(str_securities_banking);
                    rate_interest_banking.setText(str_rate_interest_banking);
                    repayment_banking.setText(str_repayment_banking);
                    limit_cash.setText(str_limit_cash);
                    out_standing_cash.setText(str_out_standing_cash);
                    presently_cash.setText(str_presently_cash);
                    securities_cash.setText(str_securities_cash);
                    rate_cash.setText(str_rate_cash);
                    repayment_cash.setText(str_repayment_cash);
                    limit_term.setText(str_limit_term);
                    out_standing_term.setText(str_out_standing_term);
                    presently_term.setText(str_presently_term);
                    securities_term.setText(str_securities_term);
                    rate_term.setText(str_rate_term);
                    repayment_term.setText(str_repayment_term);
                    limit_lg_bg.setText(str_limit_lg_bg);
                    out_standing_lg_bg.setText(str_out_standing_lg_bg);
                    presently_lg_bg.setText(str_presently_lg_bg);
                    securities_lg_bg.setText(str_securities_lg_bg);
                    rate_lg_bg.setText(str_rate_lg_bg);
                    repayment_lg_bg.setText(str_repayment_lg_bg);
                    limit_other.setText(str_limit_other);
                    out_standing_other.setText(str_out_standing_other);
                    presently_other.setText(str_presently_other);
                    securities_other.setText(str_securities_other);
                    rate_other.setText(str_rate_other);
                    repayment_other.setText(str_repayment_other);
                    limit_total.setText(str_limit_total);
                    out_standing_total.setText(str_out_standing_total);
                    presently_total.setText(str_presently_total);
                    securities_total.setText(str_securities_total);
                    rate_total.setText(str_rate_total);
                    repayment_total.setText(str_repayment_total);
                    customer_id.setText(str_customer_id);
                    amount_cash.setText(str_amount_cash);
                    amount_purpose.setText(str_amount_purpose);
                    amount_primary_security.setText(str_amount_primary_security);
                    amount_security_offered.setText(str_amount_security_offered);
                    amount_team_loan.setText(str_amount_team_loan);
                    purpose_term_loan.setText(str_purpose_term_loan);
                    primary_security_term_loan.setText(str_primary_security_term_loan);
                    security_offered_term_loan.setText(str_security_offered_term_loan);
                    amount_lg_bg.setText(str_amount_lg_bg);
                    purpose_lg_bg.setText(str_purpose_lg_bg);
                    primary_security_lg_Bg.setText(str_primary_security_lg_Bg);
                    security_offered_lg_bg.setText(str_security_offered_lg_bg);
                    amount_other.setText(str_amount_other);
                    purpose_other.setText(str_purpose_other);
                    primary_Security_other.setText(str_primary_Security_other);
                    security_offered_other.setText(str_security_offered_other);
                    expected_employment.setText(str_expected_employment);


                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("str_baking_credit", str_baking_credit);
                    editor.putString("str_limit_edit", str_limit_edit);
                    editor.putString("str_outstanding_edit", str_outstanding_edit);
                    editor.putString("str_presently_banking", str_presently_banking);
                    editor.putString("str_securities_banking", str_securities_banking);
                    editor.putString("str_rate_interest_banking", str_rate_interest_banking);
                    editor.putString("str_repayment_banking", str_repayment_banking);
                    editor.putString("str_limit_cash", str_limit_cash);
                    editor.putString("str_out_standing_cash", str_out_standing_cash);
                    editor.putString("str_presently_cash", str_presently_cash);
                    editor.putString("str_securities_cash", str_securities_cash);
                    editor.putString("str_rate_cash", str_rate_cash);
                    editor.putString("str_repayment_cash", str_repayment_cash);
                    editor.putString("str_limit_term", str_limit_term);
                    editor.putString("str_out_standing_term", str_out_standing_term);
                    editor.putString("str_presently_term", str_presently_term);
                    editor.putString("str_securities_term", str_securities_term);
                    editor.putString("str_rate_term", str_rate_term);
                    editor.putString("str_repayment_term", str_repayment_term);
                    editor.putString("str_limit_lg_bg", str_limit_lg_bg);
                    editor.putString("str_out_standing_lg_bg", str_out_standing_lg_bg);
                    editor.putString("str_presently_lg_bg", str_presently_lg_bg);
                    editor.putString("str_securities_lg_bg", str_securities_lg_bg);
                    editor.putString("str_rate_lg_bg", str_rate_lg_bg);
                    editor.putString("str_repayment_lg_bg", str_repayment_lg_bg);
                    editor.putString("str_limit_other", str_limit_other);
                    editor.putString("str_out_standing_other", str_out_standing_other);
                    editor.putString("str_presently_other", str_presently_other);
                    editor.putString("str_securities_other", str_securities_other);
                    editor.putString("str_rate_other", str_rate_other);
                    editor.putString("str_repayment_other", str_repayment_other);
                    editor.putString("str_limit_total", str_limit_total);
                    editor.putString("str_out_standing_total", str_out_standing_total);
                    editor.putString("str_presently_total", str_presently_total);
                    editor.putString("str_securities_total", str_securities_total);
                    editor.putString("str_rate_total", str_rate_total);
                    editor.putString("str_repayment_total", str_repayment_total);
                    editor.putString("str_customer_id", str_customer_id);
                    editor.putString("str_amount_cash", str_amount_cash);
                    editor.putString("str_amount_purpose", str_amount_purpose);
                    editor.putString("str_amount_primary_security", str_amount_primary_security);
                    editor.putString("str_amount_security_offered", str_amount_security_offered);
                    editor.putString("str_amount_team_loan", str_amount_team_loan);
                    editor.putString("str_purpose_term_loan", str_purpose_term_loan);
                    editor.putString("str_primary_security_term_loan", str_primary_security_term_loan);
                    editor.putString("str_security_offered_term_loan", str_security_offered_term_loan);
                    editor.putString("str_amount_lg_bg", str_amount_lg_bg);
                    editor.putString("str_purpose_lg_bg", str_purpose_lg_bg);
                    editor.putString("str_primary_security_lg_Bg", str_primary_security_lg_Bg);
                    editor.putString("str_security_offered_lg_bg", str_security_offered_lg_bg);
                    editor.putString("str_amount_other", str_amount_other);
                    editor.putString("str_purpose_other", str_purpose_other);
                    editor.putString("str_primary_Security_other", str_primary_Security_other);
                    editor.putString("str_security_offered_other", str_security_offered_other);
                    editor.putString("str_expected_employment", str_expected_employment);
                    editor.commit();

                    kycBank_Details_Submit();
//                }

            }
        });

        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Banking_details_1.this,Business_kyc_details.class);
                startActivity(intent);
                Banking_details_1.this.finish();
            }
        });
    }

    private void kycBank_Details_Submit() {
        networkCall.NetworkAPICall(ApiURL.kycBankDetailsSubmit, true);

    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent(Banking_details_1.this,Business_kyc_details.class);
        startActivity(intent);
        Banking_details_1.this.finish();
    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.kycBankDetailsSubmit:
                ion = (Builders.Any.B) Ion.with(Banking_details_1.this)
                        .load("POST", ApiURL.kycBankDetailsSubmit)
                        .setHeader("token", ApiURL.token)
                        .setBodyParameter("user_id", userid)
                        .setBodyParameter("bank/creditfacility/Rs", str_baking_credit)
                        .setBodyParameter("currentAccount", str_baking_credit)
                        .setBodyParameter("limit/inlakh", str_limit_edit)
                        .setBodyParameter("outStanding", str_outstanding_edit)
                        .setBodyParameter("presentlyBanking", str_presently_banking)
                        .setBodyParameter("securities", str_securities_banking)
                        .setBodyParameter("rateOfinterest", str_rate_interest_banking)
                        .setBodyParameter("repaymentTerms", str_repayment_banking)
                        .setBodyParameter("cashcredit", "currentacount")
                        .setBodyParameter("limit/inlakh", str_limit_cash)
                        .setBodyParameter("outStanding", str_out_standing_cash)
                        .setBodyParameter("presentlyBanking", str_presently_cash)
                        .setBodyParameter("securities", str_securities_cash)
                        .setBodyParameter("rateOfinterest", str_rate_cash)
                        .setBodyParameter("repaymentTerms", str_repayment_cash)
                        .setBodyParameter("termLoan", "termloan")
                        .setBodyParameter("limit/inlakh", str_limit_term)
                        .setBodyParameter("outStanding", str_out_standing_term)
                        .setBodyParameter("presentlyBanking", str_presently_term)
                        .setBodyParameter("securities", str_securities_term)
                        .setBodyParameter("rateOfinterest", str_rate_term)
                        .setBodyParameter("repaymentTerms", str_repayment_term)
                        .setBodyParameter("lG/BG", "LG/BG")
                        .setBodyParameter("limit/inlakh", str_limit_lg_bg)
                        .setBodyParameter("outStanding", str_out_standing_lg_bg)
                        .setBodyParameter("presentlyBanking", str_presently_lg_bg)
                        .setBodyParameter("securities", str_securities_lg_bg)
                        .setBodyParameter("rateOfinterest", str_rate_lg_bg)
                        .setBodyParameter("repaymentTerms", str_repayment_lg_bg)
                        .setBodyParameter("other", "other")
                        .setBodyParameter("limit/inlakh", str_limit_other)
                        .setBodyParameter("outStanding", str_out_standing_other)
                        .setBodyParameter("presentlyBanking", str_presently_other)
                        .setBodyParameter("securities", str_securities_other)
                        .setBodyParameter("rateOfinterest", str_rate_other)
                        .setBodyParameter("repaymentTerms", str_repayment_other)
                        .setBodyParameter("total", "total")
                        .setBodyParameter("limit/inlakh", str_limit_total)
                        .setBodyParameter("outStanding", str_out_standing_total)
                        .setBodyParameter("presentlyBanking", str_presently_total)
                        .setBodyParameter("securities", str_securities_total)
                        .setBodyParameter("rateOfinterest", str_rate_total)
                        .setBodyParameter("repaymentTerms", str_repayment_total)
                        .setBodyParameter("customer_id", str_customer_id)
                        .setBodyParameter("cashcredit", "cashcredit")
                        .setBodyParameter("amount/lakh", str_amount_cash)
                        .setBodyParameter("purpose", str_amount_purpose)
                        .setBodyParameter("primarySecurity", str_amount_primary_security)
                        .setBodyParameter("collateralSecurity", str_amount_security_offered)
                        .setBodyParameter("termloan", "termloan")
                        .setBodyParameter("amount/lakh", str_amount_team_loan)
                        .setBodyParameter("purpose", str_purpose_term_loan)
                        .setBodyParameter("primarySecurity", str_primary_security_term_loan)
                        .setBodyParameter("collateralSecurity", str_security_offered_term_loan)
                        .setBodyParameter("LGBG", "lgbg")
                        .setBodyParameter("amount/lakh", str_amount_lg_bg)
                        .setBodyParameter("purpose", str_purpose_lg_bg)
                        .setBodyParameter("primarySecurity", str_primary_security_lg_Bg)
                        .setBodyParameter("collateralSecurity", str_security_offered_lg_bg)
                        .setBodyParameter("other", "other")
                        .setBodyParameter("amount/lakh", str_amount_other)
                        .setBodyParameter("purpose", str_purpose_other)
                        .setBodyParameter("primarySecurity", str_primary_Security_other)
                        .setBodyParameter("collateralSecurity", str_security_offered_other)
                        .setBodyParameter("total", "total")
                        .setBodyParameter("amount/lakh", str_amount_total)
                        .setBodyParameter("purpose", str_purpose_total)
                        .setBodyParameter("primarySecurity", str_primary_Security_total)
                        .setBodyParameter("collateralSecurity", str_security_offered_total)
                        .setBodyParameter("expectedEmployment", str_expected_employment);

                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.kycBankDetailsSubmit:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String status = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");

                    if (status.equals("true")) {
                        Intent intent=new Intent(Banking_details_1.this,Banking_details_2.class);
                        startActivity(intent);
                        Banking_details_1.this.finish();
                    } else {
                        String fail_status = jsonObject.getString("success");
                        if (fail_status.equals("false")) {
                            Toast.makeText(Banking_details_1.this, msg, Toast.LENGTH_SHORT).show();
                        }
                        progress.dismiss();
                    }
                } catch (JSONException e1) {
                    Toast.makeText(Banking_details_1.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        Toast.makeText(Banking_details_1.this, jsonstring, Toast.LENGTH_SHORT).show();

    }
}