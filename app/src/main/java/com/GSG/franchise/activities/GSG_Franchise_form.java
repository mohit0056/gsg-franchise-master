package com.gsg.franchise.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.gsg.franchise.R;
import com.gsg.franchise.utill.ApiURL;
import com.gsg.franchise.utill.NetworkCall;
import com.gsg.franchise.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class GSG_Franchise_form extends AppCompatActivity implements NetworkCall.MyNetworkCallBack {

    EditText name, email, address, gender, phone, occupaion, dob, adharNum, panNum;
    RelativeLayout submitForm;
    Boolean isLogin;
    Boolean cancel;
    SimpleDateFormat sdf;
    public final static String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
            "[a-zA-Z0-9_+&*-]+)*@" +
            "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
            "A-Z]{2,7}$";
    static final String pref_name = "GSG";
    Progress progress;
    NetworkCall networkCall;
    Spinner sp1;
    SharedPreferences mSharedPreference;
    String myname, myphone, myemail, myaddress, mygender, user_id, myoccupation, mydob, myadharNum, mypanNum;
    final String adharRegex = "^[2-9]{1}[0-9]{3}[0-9]{4}[0-9]{4}$";
    final String panRegex = "^[A-Z]{5}[0-9]{4}[A-Z]{1}$";

    final String regexStr = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";
    String[] product;
    String s1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gsg_franchise_form);

        mSharedPreference = getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        user_id = (mSharedPreference.getString("id", ""));
        progress = new Progress(GSG_Franchise_form.this);
        networkCall = new NetworkCall(GSG_Franchise_form.this, GSG_Franchise_form.this);
        sp1 = (Spinner) findViewById(R.id.gender_spinner);

        address = (EditText) findViewById(R.id.addressEDT);
        email = (EditText) findViewById(R.id.emailEDT);
        phone = (EditText) findViewById(R.id.phnumEDT);
        name = (EditText) findViewById(R.id.nameEDT);
//        gender = (EditText) findViewById(R.id.genderEDT);
        occupaion = findViewById(R.id.occupationEDT);
        submitForm = findViewById(R.id.submitForm);
        dob = findViewById(R.id.dob);
        dob.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(
                        GSG_Franchise_form.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker,
                                          int selectedyear, int selectedmonth,
                                          int selectedday) {

                        mcurrentDate.set(Calendar.YEAR, selectedyear);
                        mcurrentDate.set(Calendar.MONTH, selectedmonth);
                        mcurrentDate.set(Calendar.DAY_OF_MONTH,
                                selectedday);
                        sdf = new SimpleDateFormat(
                                getResources().getString(
                                        R.string.datecardformat),
                                Locale.US);

                       dob.setText(sdf.format(mcurrentDate
                                .getTime()));
                    }
                }, mYear, mMonth, mDay);

//mdob = dob.getText().toString();
                mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker.show();

            }
        });

        adharNum = findViewById(R.id.adhaarEDT);
        panNum = findViewById(R.id.pancardEDT);

        product = getResources().getStringArray(R.array.Gender);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, product);
        sp1.setAdapter(adapter);


        sp1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mygender = String.valueOf(product[i]);
//                if (i == 3) {
//
//
//                } else {
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        submitForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myname = name.getText().toString().trim();
                myemail = email.getText().toString().trim();
                myphone = phone.getText().toString().trim();
                myaddress = address.getText().toString().trim();
                mydob = dob.getText().toString();

//                mygender = gender.getText().toString().trim();


                myoccupation = occupaion.getText().toString().trim();

                myadharNum = adharNum.getText().toString().trim();
                mypanNum = panNum.getText().toString().trim();

                name.setError(null);
                email.setError(null);
                phone.setError(null);
                //    gender.setError(null);
                address.setError(null);
                occupaion.setError(null);
                dob.setError(null);
                adharNum.setError(null);
                panNum.setError(null);

                if (TextUtils.isEmpty(myname)) {
                    name.setError("Your Name is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(myemail)) {
                    email.setError("Your Email is required");
                    cancel = true;
                } else if (!myemail.matches(emailRegex)) {
                    email.setError("Please Enter Valid Email");
                    cancel = true;
                } else if (TextUtils.isEmpty(myphone)) {
                    phone.setError("Phone number is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(mygender)) {
                    gender.setError("Gender is required");
                    cancel = true;
                } else if (!myphone.matches(regexStr)) {
                    phone.setError("Please Enter Valid Phone no.");
                    cancel = true;
                } else if (TextUtils.isEmpty(myaddress)) {
                    address.setError("Address is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(myoccupation)) {
                    occupaion.setError("Occupation is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(mydob)) {
                    dob.setError("Date of Birth is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(myadharNum)) {
                    adharNum.setError("Aadhaar Number is required");
                    cancel = true;
                }else if (!myadharNum.matches(adharRegex)) {
                    adharNum.setError("Please Enter Valid Aadhaar Number");
                    cancel = true;
                } else if (TextUtils.isEmpty(mypanNum)) {
                    panNum.setError("Pan Card Number is required");
                    cancel = true;
                } else if (!mypanNum.matches(panRegex)) {
                    panNum.setError("Please Enter Valid Pan Card Number");
                    cancel = true;
                }else {

                    SubmitForm();

                }
            }
        });


    }

    private void SubmitForm() {

        networkCall.NetworkAPICall(ApiURL.franchise_form, true);

    }

    public void back(View view) {
        super.onBackPressed();
    }


    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.franchise_form:
                ion = (Builders.Any.B) Ion.with(GSG_Franchise_form.this)
                        .load("POST", ApiURL.franchise_form)
                        .setHeader("token", ApiURL.token)
                        .setBodyParameter("user_id", myname)
                        .setBodyParameter("email", myemail)
                        .setBodyParameter("user_id", user_id)
                        .setBodyParameter("phone", myphone)
                        .setBodyParameter("address", myaddress)
                        .setBodyParameter("gender", mygender)
                        .setBodyParameter("dob", mydob)
                        .setBodyParameter("adharnumber", myadharNum)
                        .setBodyParameter("pannumber", mypanNum)
                        .setBodyParameter("occupation", myoccupation);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.franchise_form:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");

                    if (succes.equals("true")) {

                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");

//                        myname =jsonObject1.getString("username");
//                        myemail =jsonObject1.getString("email");
//                        myphone =jsonObject1.getString("phone");
                        myaddress=jsonObject1.getString("address");
                        mydob = jsonObject1.getString("dob");
                        myoccupation=jsonObject1.getString("occupation");
                        mygender=jsonObject1.getString("gender");
                        mypanNum=jsonObject1.getString("pannumber");
                        myadharNum=jsonObject1.getString("adharnumber");

                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = prefs.edit();
//                        editor.putString("name", myname);
//                        editor.putString("email", myemail);
//                        editor.putString("mobile", myphone);
                        editor.putString("address", myaddress);
                        editor.putString("dob", mydob);
                        editor.putString("occupation", myoccupation);
                        editor.putString("gender", mygender);
                        editor.putString("pannumber", mypanNum);
                        editor.putString("adharnumber", myadharNum);
                        editor.commit();

                        Intent uploadDoc = new Intent(GSG_Franchise_form.this, UploadDocument.class);
                        startActivity(uploadDoc);

                    } else {

                        String status_fail = jsonObject.getString("success");
                        if (status_fail.equals("false")) {
                            Toast.makeText(GSG_Franchise_form.this, msg.toString(), Toast.LENGTH_SHORT).show();
                            progress.dismiss();

                        }

                    }

                } catch (JSONException e1) {

                    Toast.makeText(GSG_Franchise_form.this, jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();

                }

                break;

        }

    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }


}