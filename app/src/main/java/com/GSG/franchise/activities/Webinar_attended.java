package com.gsg.franchise.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.gsg.franchise.R;

public class Webinar_attended extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webinar_attended);
    }

    public void back(View view) {
        super.onBackPressed();
    }
}