package com.gsg.franchise.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.gsg.franchise.fragments.Profile;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.gsg.franchise.R;
import com.gsg.franchise.fragments.Franchise;
import com.gsg.franchise.fragments.Home;
import com.gsg.franchise.fragments.Webinar;

public class MainActivity extends AppCompatActivity {
    BottomNavigationView bottomBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomBar = (BottomNavigationView) findViewById(R.id.bottomBar);
//        bottomBar.setOnNavigationItemSelectedListener(MainActivity.this);
        bottomBar.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        openFragment(Home.newInstance("", ""));

    }

    public void openFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.item0:
                    openFragment(Home.newInstance("", ""));

                    break;
                case R.id.item1:
                    Intent intent = new Intent(MainActivity.this, WebinarForm.class);
                    startActivity(intent);
                    break;

                case R.id.item2:
                    Intent intent2 = new Intent(MainActivity.this, KYC_details_1.class);
                    startActivity(intent2);
                    break;


                case R.id.item3:
                    openFragment(Profile.newInstance("", ""));
                    break;
            }
            return false;
        }
    };


    @Override
    public void onBackPressed() {
        MainActivity.this.finish();
    }
}