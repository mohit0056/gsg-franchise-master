package com.gsg.franchise.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.gsg.franchise.R;
import com.gsg.franchise.utill.ApiURL;
import com.gsg.franchise.utill.NetworkCall;
import com.gsg.franchise.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONException;
import org.json.JSONObject;

public class Banking_details_2 extends AppCompatActivity implements NetworkCall.MyNetworkCallBack{
    Button next_btn;
    ImageView back_image;
    Boolean cancel;
    NetworkCall networkCall;
    Progress progress;
    SharedPreferences mSharedPreference;
    TextInputEditText projected_cash,sales_cash,working_cash,inventory_cash,debtors_cash,creditors_cash,other_current_cash,promotor_cash,type_machine,purpose_required,whether_imported,name_supplier,enter_cost,contribute_promoters,loan_required,
            name_of_guarantor,father_spouse_name,residential_address_banking,telephone_no_banking,mobile_no,net_worth,aadhar_no_banking,pan_no_banking,name_of_owner,Collateral_security,Collateral_security_details,Collateral_security_value,
            past_year,past_year_two,present_year,next_year,first_year_projection,second_year_projection,third_year_projection,fourth_year_projection,fifth_year_projection,six_year_projection,seventh_year_projection,installed_capacity,utilized_capacity,
            past_year_net,past_year_two_net,present_year_net,next_year_net,first_year_projection_net,second_year_projection_net,third_year_projection_net,fourth_year_projection_net,fifth_year_projection_net,six_year_projection_net,seventh_year_projection_net,installed_capacity_net,utilized_capacity_net,
            past_year_capital,past_year_two_capital,present_year_capital,next_year_capital,first_year_projection_capital,second_year_projection_capital,third_year_projection_capital,fourth_year_projection_capital,fifth_year_projection_capital,six_year_projection_capital,seventh_year_projection_capital,installed_capacity_capital,utilized_capacity_capital,
            past_year_repayment,past_year_two_repayment,present_year_repayment,next_year_repayment,first_year_projection_repayment,second_year_projection_repayment,third_year_projection_repayment,fourth_year_projection_repayment,fifth_year_projection_repayment,six_year_projection_repayment,seventh_year_projection_repayment,installed_capacity_repayment,utilized_capacity_repayment;


    String userid,str_projected_cash,str_sales_cash,str_working_cash,str_inventory_cash,str_debtors_cash,str_creditors_cash,str_other_current_cash,str_promotor_cash,str_type_machine,str_purpose_required,str_whether_imported,str_name_supplier,str_enter_cost,str_contribute_promoters,str_loan_required,
             str_name_of_guarantor,str_father_spouse_name,str_residential_address_banking,str_telephone_no_banking,str_mobile_no,str_net_worth,str_aadhar_no_banking,str_pan_no_banking,str_name_of_owner,str_Collateral_security,str_Collateral_security_details,str_Collateral_security_value,
           str_past_year,str_past_year_two,str_present_year,str_next_year,str_first_year_projection,str_second_year_projection,str_third_year_projection,str_fourth_year_projection,str_fifth_year_projection,str_six_year_projection,str_seventh_year_projection,str_installed_capacity,str_utilized_capacity,
          str_past_year_net,str_past_year_two_net,str_present_year_net,str_next_year_net,str_first_year_projection_net,str_second_year_projection_net,str_third_year_projection_net,str_fourth_year_projection_net,str_fifth_year_projection_net,str_six_year_projection_net,str_seventh_year_projection_net,str_installed_capacity_net,str_utilized_capacity_net,
            str_past_year_capital,str_past_year_two_capital,str_present_year_capital,str_next_year_capital,str_first_year_projection_capital,str_second_year_projection_capital,str_third_year_projection_capital,str_fourth_year_projection_capital,str_fifth_year_projection_capital,str_six_year_projection_capital,str_seventh_year_projection_capital,str_installed_capacity_capital,str_utilized_capacity_capital,
            str_past_year_repayment,str_past_year_two_repayment,str_present_year_repayment,str_next_year_repayment,str_first_year_projection_repayment,str_second_year_projection_repayment,str_third_year_projection_repayment,str_fourth_year_projection_repayment,str_fifth_year_projection_repayment,str_six_year_projection_repayment,str_seventh_year_projection_repayment,str_installed_capacity_repayment,str_utilized_capacity_repayment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banking_details_2);

        progress = new Progress(Banking_details_2.this);
        networkCall = new NetworkCall(Banking_details_2.this, Banking_details_2.this);

        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        userid =mSharedPreference.getString("id", "");

        next_btn=findViewById(R.id.next_btn);
        back_image=findViewById(R.id.back_image);
        projected_cash=findViewById(R.id.projected_cash);
        sales_cash=findViewById(R.id.sales_cash);
        working_cash=findViewById(R.id.working_cash);
        inventory_cash=findViewById(R.id.inventory_cash);
        debtors_cash=findViewById(R.id.debtors_cash);
        creditors_cash=findViewById(R.id.creditors_cash);
        other_current_cash=findViewById(R.id.other_current_cash);
        promotor_cash=findViewById(R.id.promotor_cash);
        type_machine=findViewById(R.id.type_machine);
        purpose_required=findViewById(R.id.purpose_required);
        whether_imported=findViewById(R.id.whether_imported);
        name_supplier=findViewById(R.id.name_supplier);
        enter_cost=findViewById(R.id.enter_cost);
        contribute_promoters=findViewById(R.id.contribute_promoters);
        loan_required=findViewById(R.id.loan_required);
        name_of_guarantor=findViewById(R.id.name_of_guarantor);
        father_spouse_name=findViewById(R.id.father_spouse_name);
        residential_address_banking=findViewById(R.id.residential_address_banking);
        telephone_no_banking=findViewById(R.id.telephone_no_banking);
        mobile_no=findViewById(R.id.mobile_no);
        net_worth=findViewById(R.id.net_worth);
        aadhar_no_banking=findViewById(R.id.aadhar_no_banking);
        pan_no_banking=findViewById(R.id.pan_no_banking);
        name_of_owner=findViewById(R.id.name_of_owner);
        Collateral_security=findViewById(R.id.Collateral_security);
        Collateral_security_details=findViewById(R.id.Collateral_security_details);
        Collateral_security_value=findViewById(R.id.Collateral_security_value);
        past_year=findViewById(R.id.past_year);
        past_year_two=findViewById(R.id.past_year_two);
        present_year=findViewById(R.id.present_year);
        next_year=findViewById(R.id.next_year);
        first_year_projection=findViewById(R.id.first_year_projection);
        second_year_projection=findViewById(R.id.second_year_projection);
        third_year_projection=findViewById(R.id.third_year_projection);
        fourth_year_projection=findViewById(R.id.fourth_year_projection);
        fifth_year_projection=findViewById(R.id.fifth_year_projection);
        six_year_projection=findViewById(R.id.six_year_projection);
        seventh_year_projection=findViewById(R.id.seventh_year_projection);
        installed_capacity=findViewById(R.id.installed_capacity);
        utilized_capacity=findViewById(R.id.utilized_capacity);
        past_year_net=findViewById(R.id.past_year_net);
        past_year_two_net=findViewById(R.id.past_year_two_net);
        present_year_net=findViewById(R.id.present_year_net);
        next_year_net=findViewById(R.id.next_year_net);
        first_year_projection_net=findViewById(R.id.first_year_projection_net);
        second_year_projection_net=findViewById(R.id.second_year_projection_net);
        third_year_projection_net=findViewById(R.id.third_year_projection_net);
        fourth_year_projection_net=findViewById(R.id.fourth_year_projection_net);
        fifth_year_projection_net=findViewById(R.id.fifth_year_projection_net);
        six_year_projection_net=findViewById(R.id.six_year_projection_net);
        seventh_year_projection_net=findViewById(R.id.seventh_year_projection_net);
        installed_capacity_net=findViewById(R.id.installed_capacity_net);
        utilized_capacity_net=findViewById(R.id.utilized_capacity_net);
        past_year_capital=findViewById(R.id.past_year_capital);
        past_year_two_capital=findViewById(R.id.past_year_two_capital);
        present_year_capital=findViewById(R.id.present_year_capital);
        next_year_capital=findViewById(R.id.next_year_capital);
        first_year_projection_capital=findViewById(R.id.first_year_projection_capital);
        second_year_projection_capital=findViewById(R.id.second_year_projection_capital);
        third_year_projection_capital=findViewById(R.id.third_year_projection_capital);
        fourth_year_projection_capital=findViewById(R.id.fourth_year_projection_capital);
        fifth_year_projection_capital=findViewById(R.id.fifth_year_projection_capital);
        six_year_projection_capital=findViewById(R.id.six_year_projection_capital);
        seventh_year_projection_capital=findViewById(R.id.seventh_year_projection_capital);
        installed_capacity_capital=findViewById(R.id.installed_capacity_capital);
        utilized_capacity_capital=findViewById(R.id.utilized_capacity_capital);
        past_year_repayment=findViewById(R.id.past_year_repayment);
        past_year_two_repayment=findViewById(R.id.past_year_two_repayment);
        present_year_repayment=findViewById(R.id.present_year_repayment);
        next_year_repayment=findViewById(R.id.next_year_repayment);
        first_year_projection_repayment=findViewById(R.id.first_year_projection_repayment);
        second_year_projection_repayment=findViewById(R.id.second_year_projection_repayment);
        third_year_projection_repayment=findViewById(R.id.third_year_projection_repayment);
        fourth_year_projection_repayment=findViewById(R.id.fourth_year_projection_repayment);
        fifth_year_projection_repayment=findViewById(R.id.fifth_year_projection_repayment);
        six_year_projection_repayment=findViewById(R.id.six_year_projection_repayment);
        seventh_year_projection_repayment=findViewById(R.id.seventh_year_projection_repayment);
        installed_capacity_repayment=findViewById(R.id.installed_capacity_repayment);
        utilized_capacity_repayment=findViewById(R.id.utilized_capacity_repayment);

        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_projected_cash= projected_cash.getText().toString().trim();
                str_sales_cash = sales_cash.getText().toString().trim();
                str_working_cash =working_cash.getText().toString().trim();
                str_inventory_cash =inventory_cash.getText().toString().trim();
                str_debtors_cash =debtors_cash.getText().toString().trim();
                str_creditors_cash=creditors_cash.getText().toString().trim();
                str_other_current_cash =other_current_cash.getText().toString().trim();
                str_promotor_cash =promotor_cash.getText().toString().trim();
                str_type_machine =type_machine.getText().toString().trim();
                str_purpose_required =purpose_required.getText().toString().trim();
                str_whether_imported =whether_imported.getText().toString().trim();
                str_name_supplier =name_supplier.getText().toString().trim();
                str_enter_cost =enter_cost.getText().toString().trim();
                str_contribute_promoters =contribute_promoters.getText().toString().trim();
                str_loan_required =loan_required.getText().toString().trim();
                str_name_of_guarantor =name_of_guarantor.getText().toString().trim();
                str_father_spouse_name =father_spouse_name.getText().toString().trim();
                str_residential_address_banking =residential_address_banking.getText().toString().trim();
                str_telephone_no_banking =telephone_no_banking.getText().toString().trim();
                str_mobile_no =mobile_no.getText().toString().trim();
                str_net_worth=net_worth.getText().toString().trim();
                str_aadhar_no_banking=aadhar_no_banking.getText().toString().trim();
                str_pan_no_banking=pan_no_banking.getText().toString().trim();
                str_name_of_owner=name_of_owner.getText().toString().trim();
                str_Collateral_security=Collateral_security.getText().toString().trim();
                str_Collateral_security_details=Collateral_security_details.getText().toString().trim();
                str_Collateral_security_value=Collateral_security_value.getText().toString().trim();
                str_past_year=past_year.getText().toString().trim();
                str_past_year_two=past_year_two.getText().toString().trim();
                str_present_year=present_year.getText().toString().trim();
                str_next_year=next_year.getText().toString().trim();
                str_first_year_projection=first_year_projection.getText().toString().trim();
                str_second_year_projection=second_year_projection.getText().toString().trim();
                str_third_year_projection=third_year_projection.getText().toString().trim();
                str_fourth_year_projection=fourth_year_projection.getText().toString().trim();
                str_fifth_year_projection=fifth_year_projection.getText().toString().trim();
                str_six_year_projection=six_year_projection.getText().toString().trim();
                str_seventh_year_projection=seventh_year_projection.getText().toString().trim();
                str_installed_capacity=installed_capacity.getText().toString().trim();
                str_utilized_capacity=utilized_capacity.getText().toString().trim();
                str_past_year_net=past_year_net.getText().toString().trim();
                str_past_year_two_net=past_year_two_net.getText().toString().trim();
                str_present_year_net=present_year_net.getText().toString().trim();
                str_next_year_net=next_year_net.getText().toString().trim();
                str_first_year_projection_net=first_year_projection_net.getText().toString().trim();
                str_second_year_projection_net=second_year_projection_net.getText().toString().trim();
                str_third_year_projection_net=third_year_projection_net.getText().toString().trim();
                str_fourth_year_projection_net=fourth_year_projection_net.getText().toString().trim();
                str_fifth_year_projection_net=fifth_year_projection_net.getText().toString().trim();
                str_six_year_projection_net=six_year_projection_net.getText().toString().trim();
                str_seventh_year_projection_net=seventh_year_projection_net.getText().toString().trim();
                str_installed_capacity_net=installed_capacity_net.getText().toString().trim();
                str_utilized_capacity_net=utilized_capacity_net.getText().toString().trim();
                str_past_year_capital=past_year_capital.getText().toString().trim();
                str_past_year_two_capital=past_year_two_capital.getText().toString().trim();
                str_present_year_capital=present_year_capital.getText().toString().trim();
                str_next_year_capital=next_year_capital.getText().toString().trim();
                str_first_year_projection_capital=first_year_projection_capital.getText().toString().trim();
                str_second_year_projection_capital=second_year_projection_capital.getText().toString().trim();
                str_third_year_projection_capital=third_year_projection_capital.getText().toString().trim();
                str_fourth_year_projection_capital=fourth_year_projection_capital.getText().toString().trim();
                str_fifth_year_projection_capital=fifth_year_projection_capital.getText().toString().trim();
                str_six_year_projection_capital=six_year_projection_capital.getText().toString().trim();
                str_seventh_year_projection_capital=seventh_year_projection_capital.getText().toString().trim();
                str_installed_capacity_capital=installed_capacity_capital.getText().toString().trim();
                str_utilized_capacity_capital=utilized_capacity_capital.getText().toString().trim();
                str_past_year_repayment=past_year_repayment.getText().toString().trim();
                str_past_year_two_repayment=past_year_two_repayment.getText().toString().trim();
                str_present_year_repayment=present_year_repayment.getText().toString().trim();
                str_next_year_repayment=next_year_repayment.getText().toString().trim();
                str_first_year_projection_repayment=first_year_projection_repayment.getText().toString().trim();
                str_second_year_projection_repayment=second_year_projection_repayment.getText().toString().trim();
                str_third_year_projection_repayment=third_year_projection_repayment.getText().toString().trim();
                str_fourth_year_projection_repayment=fourth_year_projection_repayment.getText().toString().trim();
                str_fifth_year_projection_repayment=fifth_year_projection_repayment.getText().toString().trim();
                str_six_year_projection_repayment=six_year_projection_repayment.getText().toString().trim();
                str_seventh_year_projection_repayment=seventh_year_projection_repayment.getText().toString().trim();
                str_installed_capacity_repayment=installed_capacity_repayment.getText().toString().trim();
                str_utilized_capacity_repayment=utilized_capacity_repayment.getText().toString().trim();

//                projected_cash.setError(null);
//                sales_cash.setError(null);
//                working_cash.setError(null);
//                inventory_cash.setError(null);
//                debtors_cash.setError(null);
//                creditors_cash.setError(null);
//                other_current_cash.setError(null);
//                promotor_cash.setError(null);
//                type_machine.setError(null);
//                purpose_required.setError(null);
//                whether_imported.setError(null);
//                name_supplier.setError(null);
//                enter_cost.setError(null);
//                contribute_promoters.setError(null);
//                loan_required.setError(null);
//                name_of_guarantor.setError(null);
//                father_spouse_name.setError(null);
//                residential_address_banking.setError(null);
//                telephone_no_banking.setError(null);
//                mobile_no.setError(null);
//                net_worth.setError(null);
//                aadhar_no_banking.setError(null);
//                pan_no_banking.setError(null);
//                name_of_owner.setError(null);
//                Collateral_security.setError(null);
//                Collateral_security_details.setError(null);
//                Collateral_security_value.setError(null);
//                past_year.setError(null);
//                past_year_two.setError(null);
//                present_year.setError(null);
//                next_year.setError(null);
//                first_year_projection.setError(null);
//                second_year_projection.setError(null);
//                third_year_projection.setError(null);
//                fourth_year_projection.setError(null);
//                fifth_year_projection.setError(null);
//                six_year_projection.setError(null);
//                seventh_year_projection.setError(null);
//                installed_capacity.setError(null);
//                utilized_capacity.setError(null);
//                past_year_net.setError(null);
//                past_year_two_net.setError(null);
//                present_year_net.setError(null);
//                next_year_net.setError(null);
//                first_year_projection_net.setError(null);
//                second_year_projection_net.setError(null);
//                third_year_projection_net.setError(null);
//                fourth_year_projection_net.setError(null);
//                fifth_year_projection_net.setError(null);
//                six_year_projection_net.setError(null);
//                seventh_year_projection_net.setError(null);
//                installed_capacity_net.setError(null);
//                utilized_capacity_net.setError(null);
//                past_year_capital.setError(null);
//                past_year_two_capital.setError(null);
//                present_year_capital.setError(null);
//                next_year_capital.setError(null);
//                first_year_projection_capital.setError(null);
//                second_year_projection_capital.setError(null);
//                third_year_projection_capital.setError(null);
//                fourth_year_projection_capital.setError(null);
//                fifth_year_projection_capital.setError(null);
//                six_year_projection_capital.setError(null);
//                seventh_year_projection_capital.setError(null);
//                installed_capacity_capital.setError(null);
//                utilized_capacity_capital.setError(null);
//                past_year_repayment.setError(null);
//                past_year_two_repayment.setError(null);
//                present_year_repayment.setError(null);
//                next_year_repayment.setError(null);
//                first_year_projection_repayment.setError(null);
//                second_year_projection_repayment.setError(null);
//                third_year_projection_repayment.setError(null);
//                fourth_year_projection_repayment.setError(null);
//                fifth_year_projection_repayment.setError(null);
//                six_year_projection_repayment.setError(null);
//                seventh_year_projection_repayment.setError(null);
//                installed_capacity_repayment.setError(null);
//                utilized_capacity_repayment.setError(null);
//
//                if (TextUtils.isEmpty(str_projected_cash)) {
//                    Toast.makeText(Banking_details_2.this, "Projected is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_sales_cash)) {
//                    Toast.makeText(Banking_details_2.this, "Sales required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_working_cash)) {
//                    Toast.makeText(Banking_details_2.this, "Working Cycle in Months is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_inventory_cash)) {
//                    Toast.makeText(Banking_details_2.this, "Inventory is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_debtors_cash)) {
//                    Toast.makeText(Banking_details_2.this, "Debtors is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_creditors_cash)) {
//                    Toast.makeText(Banking_details_2.this, "Creditors is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_other_current_cash)) {
//                    Toast.makeText(Banking_details_2.this, "Other current assets is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_promotor_cash)) {
//                    Toast.makeText(Banking_details_2.this, "Promoters Contribution is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_type_machine)) {
//                    Toast.makeText(Banking_details_2.this, "Type of Machine/ Equipment is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_purpose_required)) {
//                    Toast.makeText(Banking_details_2.this, "Purpose for which Required is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_whether_imported)) {
//                    Toast.makeText(Banking_details_2.this, "Whether Imported or Indigenous is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_name_supplier)) {
//                    Toast.makeText(Banking_details_2.this, "Name of Supplier is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_enter_cost)) {
//                    Toast.makeText(Banking_details_2.this, "Enter Cost is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_contribute_promoters)) {
//                    Toast.makeText(Banking_details_2.this, "Contribution being made by the promoters is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_loan_required)) {
//                    Toast.makeText(Banking_details_2.this, "Loan Required is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_name_of_guarantor)) {
//                    Toast.makeText(Banking_details_2.this, "Name of Guarantor is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_father_spouse_name)) {
//                    Toast.makeText(Banking_details_2.this, "Father / Spouse name is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_residential_address_banking)) {
//                    Toast.makeText(Banking_details_2.this, "Residential Address is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_telephone_no_banking)) {
//                    Toast.makeText(Banking_details_2.this, "Telephone No. (Residence) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_mobile_no)) {
//                    Toast.makeText(Banking_details_2.this, "Mobile No. is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_net_worth)) {
//                    Toast.makeText(Banking_details_2.this, "Net worth (Rs. in lakh) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_aadhar_no_banking)) {
//                    Toast.makeText(Banking_details_2.this, "Aadhar No. is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_pan_no_banking)) {
//                    Toast.makeText(Banking_details_2.this, "PAN No. is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_name_of_owner)) {
//                    Toast.makeText(Banking_details_2.this, "Name of owner of Collateral is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_Collateral_security)) {
//                    Toast.makeText(Banking_details_2.this, "Collateral Security Nature is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_Collateral_security_details)) {
//                    Toast.makeText(Banking_details_2.this, "Collateral Security Details is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_Collateral_security_value)) {
//                    Toast.makeText(Banking_details_2.this, "Collateral Security Value (Rs. in Lakh) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }
//                else if (TextUtils.isEmpty(str_past_year)) {
//                    Toast.makeText(Banking_details_2.this, "Past Year (1 Actual) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }
//                else if (TextUtils.isEmpty(str_past_year_two)) {
//                    Toast.makeText(Banking_details_2.this, "Past Year (2 Actual) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }
//                else if (TextUtils.isEmpty(str_present_year)) {
//                    Toast.makeText(Banking_details_2.this, "Present Year (Estimate) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }
//                else if (TextUtils.isEmpty(str_next_year)) {
//                    Toast.makeText(Banking_details_2.this, "Next Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }
//                else if (TextUtils.isEmpty(str_first_year_projection)) {
//                    Toast.makeText(Banking_details_2.this, "First Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                } else if (TextUtils.isEmpty(str_second_year_projection)) {
//                    Toast.makeText(Banking_details_2.this, "Second Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_third_year_projection)) {
//                    Toast.makeText(Banking_details_2.this, "Third Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_fourth_year_projection)) {
//                    Toast.makeText(Banking_details_2.this, "Fourth Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_fifth_year_projection)) {
//                    Toast.makeText(Banking_details_2.this, "Fifth Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_six_year_projection)) {
//                    Toast.makeText(Banking_details_2.this, "Six Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_seventh_year_projection)) {
//                    Toast.makeText(Banking_details_2.this, "Seventh Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_installed_capacity)) {
//                    Toast.makeText(Banking_details_2.this, "Installed Capacity is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_utilized_capacity)) {
//                    Toast.makeText(Banking_details_2.this, "Utilized Capacity is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_past_year_net)) {
//                    Toast.makeText(Banking_details_2.this, "Past Year (1 Actual) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_past_year_two_net)) {
//                    Toast.makeText(Banking_details_2.this, "Past Year (2 Actual) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_present_year_net)) {
//                    Toast.makeText(Banking_details_2.this, "Present Year (Estimate) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_next_year_net)) {
//                    Toast.makeText(Banking_details_2.this, "Next Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_first_year_projection_net)) {
//                    Toast.makeText(Banking_details_2.this, "First Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_second_year_projection_net)) {
//                    Toast.makeText(Banking_details_2.this, "Second Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_third_year_projection_net)) {
//                    Toast.makeText(Banking_details_2.this, "Third Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_fourth_year_projection_net)) {
//                    Toast.makeText(Banking_details_2.this, "Fourth Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }
//                else if (TextUtils.isEmpty(str_fifth_year_projection_net)) {
//                    Toast.makeText(Banking_details_2.this, "Fifth Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }
//                else if (TextUtils.isEmpty(str_six_year_projection_net)) {
//                    Toast.makeText(Banking_details_2.this, "Six Year (Projection)  is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }
//                else if (TextUtils.isEmpty(str_seventh_year_projection_net)) {
//                    Toast.makeText(Banking_details_2.this, "Seventh Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_installed_capacity_net)) {
//                    Toast.makeText(Banking_details_2.this, "Utilized Capacity is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_utilized_capacity_net)) {
//                    Toast.makeText(Banking_details_2.this, "Utilized Capacity is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_past_year_capital)) {
//                    Toast.makeText(Banking_details_2.this, "Past Year (1 Actual) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_past_year_two_capital)) {
//                    Toast.makeText(Banking_details_2.this, "Past Year (2 Actual) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }
//                else if (TextUtils.isEmpty(str_present_year_capital)) {
//                    Toast.makeText(Banking_details_2.this, "Present Year (Estimate) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }
//                else if (TextUtils.isEmpty(str_next_year_capital)) {
//                    Toast.makeText(Banking_details_2.this, "Next Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }
//                else if (TextUtils.isEmpty(str_first_year_projection_capital)) {
//                    Toast.makeText(Banking_details_2.this, "First Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }
//                else if (TextUtils.isEmpty(str_second_year_projection_capital)) {
//                    Toast.makeText(Banking_details_2.this, "Second Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_third_year_projection_capital)) {
//                    Toast.makeText(Banking_details_2.this, "Third Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_fourth_year_projection_capital)) {
//                    Toast.makeText(Banking_details_2.this, "Fourth Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_fifth_year_projection_capital)) {
//                    Toast.makeText(Banking_details_2.this, "Fifth Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_six_year_projection_capital)) {
//                    Toast.makeText(Banking_details_2.this, "Six Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_seventh_year_projection_capital)) {
//                    Toast.makeText(Banking_details_2.this, "Seventh Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_installed_capacity_capital)) {
//                    Toast.makeText(Banking_details_2.this, "Installed Capacity is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_utilized_capacity_capital)) {
//                    Toast.makeText(Banking_details_2.this, "Utilized Capacity is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//
//                }else if (TextUtils.isEmpty(str_past_year_repayment)) {
//                    Toast.makeText(Banking_details_2.this, "Past Year (1 Actual) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_past_year_two_repayment)) {
//                    Toast.makeText(Banking_details_2.this, "Past Year (2 Actual) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_present_year_repayment)) {
//                    Toast.makeText(Banking_details_2.this, "Present Year (Estimate) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_next_year_repayment)) {
//                    Toast.makeText(Banking_details_2.this, "Next Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_first_year_projection_repayment)) {
//                    Toast.makeText(Banking_details_2.this, "First Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_second_year_projection_repayment)) {
//                    Toast.makeText(Banking_details_2.this, "Second Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_third_year_projection_repayment)) {
//                    Toast.makeText(Banking_details_2.this, "Third Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_fourth_year_projection_repayment)) {
//                    Toast.makeText(Banking_details_2.this, "Fourth Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_fifth_year_projection_repayment)) {
//                    Toast.makeText(Banking_details_2.this, "Fifth Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//
//                }else if (TextUtils.isEmpty(str_six_year_projection_repayment)) {
//                    Toast.makeText(Banking_details_2.this, "Six Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_seventh_year_projection_repayment)) {
//                    Toast.makeText(Banking_details_2.this, "Seventh Year (Projection) is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_installed_capacity_repayment)) {
//                    Toast.makeText(Banking_details_2.this, "Installed Capacity is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else if (TextUtils.isEmpty(str_utilized_capacity_repayment)) {
//                    Toast.makeText(Banking_details_2.this, "Utilized Capacity is required", Toast.LENGTH_SHORT).show();
//                    cancel = true;
//                }else {
//                    business_exit.setText(str_business_exit);
//                        business_since.setText(str_business_since);
//                        business_proposed.setText(str_business_proposed);
//                        business_automobile.setText(str_business_automobile);
//                    next_year_repayment.setText(str_next_year_repayment);
//                    first_year_projection_repayment.setText(str_first_year_projection_repayment);
//                    second_year_projection_repayment.setText(str_second_year_projection_repayment);
//                    third_year_projection_repayment.setText(str_third_year_projection_repayment);
//                    fifth_year_projection_repayment.setText(str_fifth_year_projection_repayment);
//                    six_year_projection_repayment.setText(str_six_year_projection_repayment);
//                    seventh_year_projection_repayment.setText(str_seventh_year_projection_repayment);
//                    installed_capacity_repayment.setText(str_installed_capacity_repayment);
//                    utilized_capacity_repayment.setText(str_utilized_capacity_repayment);


//                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//                        SharedPreferences.Editor editor = prefs.edit();
//                        editor.putString("str_utilized_capacity_repayment", str_utilized_capacity_repayment);
//                        editor.putString("str_installed_capacity_repayment", str_installed_capacity_repayment);
//                        editor.putString("str_seventh_year_projection_repayment", str_seventh_year_projection_repayment);
//                        editor.putString("str_six_year_projection_repayment", str_six_year_projection_repayment);
//                        editor.putString("str_fifth_year_projection_repayment", str_fifth_year_projection_repayment);
//                        editor.putString("str_third_year_projection_repayment", str_third_year_projection_repayment);
//                        editor.putString("str_second_year_projection_repayment", str_second_year_projection_repayment);
//                        editor.putString("str_first_year_projection_repayment", str_first_year_projection_repayment);
//                        editor.putString("str_next_year_repayment", str_next_year_repayment);
//                        editor.putString("str_business_nature_association", str_business_nature_association);
//                        editor.putString("str_business_registration_act", str_business_registration_act);
//                        editor.putString("str_social_category", str_social_category);
//                        editor.putString("str_business_minority", str_business_minority);
//
//                        editor.commit();











                    kycBank_Details2_Submit();

//                }
            }
        });
        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Banking_details_2.this,Banking_details_1.class);
                startActivity(intent);
                Banking_details_2.this.finish();
            }
        });
    }

    private void kycBank_Details2_Submit() {
        networkCall.NetworkAPICall(ApiURL.kycBankDetails2Submit, true);
    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent(Banking_details_2.this,Banking_details_1.class);
        startActivity(intent);
        Banking_details_2.this.finish();
    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.kycBankDetails2Submit:
                ion = (Builders.Any.B) Ion.with(Banking_details_2.this)
                        .load("POST", ApiURL.kycBankDetails2Submit)
                        .setHeader("token", ApiURL.token)
                        .setBodyParameter("user_id", userid)
                        .setBodyParameter("projected", str_projected_cash)
                        .setBodyParameter("sales", str_sales_cash)
                        .setBodyParameter("workingcycle", str_working_cash)
                        .setBodyParameter("inventory", str_inventory_cash)
                        .setBodyParameter("otherassets", str_other_current_cash)
                        .setBodyParameter("promoter/Contribution", str_promotor_cash)
                        .setBodyParameter("machine/equipment", str_type_machine)
                        .setBodyParameter("purpose/required", str_purpose_required)
                        .setBodyParameter("imported/indigenous", str_whether_imported)
                        .setBodyParameter("supplier", str_name_supplier)
                        .setBodyParameter("cost", str_enter_cost)
                        .setBodyParameter("contribution/promoter", str_contribute_promoters)
                        .setBodyParameter("loan", str_loan_required)
                        .setBodyParameter("guarantorName", str_name_of_guarantor)
                        .setBodyParameter("father/spouseName", str_father_spouse_name)
                        .setBodyParameter("residentialAddress", str_residential_address_banking)
                        .setBodyParameter("telephone", str_telephone_no_banking)
                        .setBodyParameter("mobile", str_mobile_no)
                        .setBodyParameter("panno", str_pan_no_banking)
                        .setBodyParameter("adharno", str_aadhar_no_banking)
                        .setBodyParameter("lakh", str_net_worth)
                        .setBodyParameter("collateralName", str_name_of_owner)
                        .setBodyParameter("Securitynature", str_Collateral_security)
                        .setBodyParameter("Securitydetails", str_Collateral_security_details)
                        .setBodyParameter("Security/lakh", str_Collateral_security_value)
                        .setBodyParameter("pastYear1", str_past_year)
                        .setBodyParameter("pastYear2", str_past_year_two)
                        .setBodyParameter("presentYear", str_present_year)
                        .setBodyParameter("nextYear", str_next_year)
                        .setBodyParameter("firstYear", str_first_year_projection)
                        .setBodyParameter("secondYear", str_second_year_projection)
                        .setBodyParameter("thirdYear", str_third_year_projection)
                        .setBodyParameter("fourthYear", str_fourth_year_projection)
                        .setBodyParameter("fifthYear", str_fifth_year_projection)
                        .setBodyParameter("sixthYear", str_six_year_projection)
                        .setBodyParameter("seventhYear", str_seventh_year_projection)
                        .setBodyParameter("installed", str_installed_capacity)
                        .setBodyParameter("utilized", str_utilized_capacity)
                        .setBodyParameter("pastYear12", str_past_year_net)
                        .setBodyParameter("pastYear22", str_past_year_two_net)
                        .setBodyParameter("presentYear2", str_present_year_net)
                        .setBodyParameter("nextYear2", str_next_year_net)
                        .setBodyParameter("firstYear2", str_first_year_projection_net)
                        .setBodyParameter("secondYear2", str_second_year_projection_net)
                        .setBodyParameter("thirdYear2", str_third_year_projection_net)
                        .setBodyParameter("fourthYear2", str_fourth_year_projection_net)
                        .setBodyParameter("fifthYear2", str_fifth_year_projection_net)
                        .setBodyParameter("sixthYear2", str_six_year_projection_net)
                        .setBodyParameter("seventhYear2", str_seventh_year_projection_net)
                        .setBodyParameter("installed2", str_installed_capacity_net)
                        .setBodyParameter("utilized2", str_utilized_capacity_net)
                        .setBodyParameter("pastYear13", str_past_year_capital)
                        .setBodyParameter("pastYear23", str_past_year_two_capital)
                        .setBodyParameter("presentYear3", str_present_year_capital)
                        .setBodyParameter("nextYear3", str_next_year_capital)
                        .setBodyParameter("firstYear3", str_first_year_projection_capital)
                        .setBodyParameter("secondYear3", str_second_year_projection_capital)
                        .setBodyParameter("thirdYear3", str_third_year_projection_capital)
                        .setBodyParameter("fourthYear3", str_fourth_year_projection_capital)
                        .setBodyParameter("fifthYear3", str_fifth_year_projection_capital)
                        .setBodyParameter("sixthYear3", str_six_year_projection_capital)
                        .setBodyParameter("seventhYear3", str_seventh_year_projection_capital)
                        .setBodyParameter("installed3", str_installed_capacity_capital)
                        .setBodyParameter("utilized3", str_utilized_capacity_capital)
                        .setBodyParameter("pastYear14", str_past_year_repayment)
                        .setBodyParameter("pastYear24", str_past_year_two_repayment)
                        .setBodyParameter("presentYear4", str_present_year_repayment)
                        .setBodyParameter("nextYear4", str_next_year_repayment)
                        .setBodyParameter("firstYear4", str_first_year_projection_repayment)
                        .setBodyParameter("secondYear4", str_second_year_projection_repayment)
                        .setBodyParameter("thirdYear4", str_third_year_projection_repayment)
                        .setBodyParameter("fourthYear4", str_fourth_year_projection_repayment)
                        .setBodyParameter("fifthYear4", str_fifth_year_projection_repayment)
                        .setBodyParameter("sixthYear4", str_six_year_projection_repayment)
                        .setBodyParameter("seventhYear4", str_seventh_year_projection_repayment)
                        .setBodyParameter("installed4", str_installed_capacity_repayment)
                        .setBodyParameter("utilized4", str_utilized_capacity_repayment);


                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.kycBankDetails2Submit:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String status = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");
                    if (status.equals("true")) {
                        Intent intent=new Intent(Banking_details_2.this,Status_regarding_statutory.class);

                        startActivity(intent);
                        Banking_details_2.this.finish();
//                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
//                        str_business_exit=jsonObject1.getString("existing");
//                        str_business_since=jsonObject1.getString("since");
//                        str_business_proposed=jsonObject1.getString("propose");
//                        str_business_automobile=jsonObject1.getString("automobile_service");
//                        str_business_date=jsonObject1.getString("commencementDate");
//                        str_check_box=jsonObject1.getString("ZedRate");
//                        str_business_registration_no= jsonObject1.getString("registerNo");
//                        str_business_office_address=jsonObject1.getString("registerAddress");
//                        str_business_address_associate=jsonObject1.getString("associateName");
//                        str_business_presently_banking=jsonObject1.getString("presentlyBanking");
//                        str_business_nature_association=jsonObject1.getString("natureAssociation");
//                        str_business_registration_act=jsonObject1.getString("registrationAct");
//                        str_social_category=jsonObject1.getString("socialCategory");
//                        str_business_minority=jsonObject1.getString("minority");
//                        str_business_radio=jsonObject1.getString("selectDirectorbank");

//
//                        business_exit.setText(str_business_exit);
//                        business_since.setText(str_business_since);
//                        business_proposed.setText(str_business_proposed);
//                        business_automobile.setText(str_business_automobile);
//                        business_date.setText(str_business_date);
//                        business_registration_no.setText(str_business_registration_no);
//                        business_office_address.setText(str_business_office_address);
//                        business_address_associate.setText(str_business_address_associate);
//                        business_presently_banking.setText(str_business_presently_banking);
//                        business_nature_association.setText(str_business_nature_association);
//                        business_registration_act.setText(str_business_registration_act);
//                        social_category.setText(str_social_category);
//                        business_minority.setText(str_business_minority);


//                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//                        SharedPreferences.Editor editor = prefs.edit();
//                        editor.putString("str_business_exit", "str_business_exit");
////                        editor.putString("str_business_since", str_business_since);
////                        editor.putString("str_business_proposed", str_business_proposed);
////                        editor.putString("str_business_automobile", str_business_automobile);
////                        editor.putString("str_business_date", str_business_date);
////                        editor.putString("str_business_registration_no", str_business_registration_no);
////                        editor.putString("str_business_office_address", str_business_office_address);
////                        editor.putString("str_business_address_associate", str_business_address_associate);
////                        editor.putString("str_business_presently_banking", str_business_presently_banking);
////                        editor.putString("str_business_nature_association", str_business_nature_association);
////                        editor.putString("str_business_registration_act", str_business_registration_act);
////                        editor.putString("str_social_category", str_social_category);
////                        editor.putString("str_business_minority", str_business_minority);
//
//                        editor.commit();
//                        Intent intent=new Intent(Banking_details_1.this,Banking_details_2.class);
//                        startActivity(intent);
//                        finish();

                    } else {


                        String fail_status = jsonObject.getString("success");

                        if (fail_status.equals("false")) {


                            Toast.makeText(Banking_details_2.this, msg, Toast.LENGTH_SHORT).show();
                        }
                        progress.dismiss();
                    }
                } catch (JSONException e1) {


                    Toast.makeText(Banking_details_2.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        Toast.makeText(Banking_details_2.this, jsonstring, Toast.LENGTH_SHORT).show();

    }
}