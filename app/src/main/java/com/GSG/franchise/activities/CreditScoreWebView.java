package com.gsg.franchise.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.gsg.franchise.R;

public class CreditScoreWebView extends AppCompatActivity {
    WebView browser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_score_web_view);
        browser = (WebView) findViewById(R.id.webview);
        WebSettings webSettings = browser.getSettings();
        webSettings.setJavaScriptEnabled(true);
        browser.loadUrl("https://creditreport.paisabazaar.com/");
    }

    @Override
    public void onBackPressed() {
        if(browser.canGoBack()){
            onBackPressed();
        }else{

            Intent intent = new Intent(CreditScoreWebView.this,CreditScore.class);
            startActivity(intent);
            CreditScoreWebView.this.finish();
        }
    }
}