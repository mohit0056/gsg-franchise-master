package com.gsg.franchise.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.chaos.view.PinView;
import com.gsg.franchise.R;
import com.gsg.franchise.utill.ApiURL;
import com.gsg.franchise.utill.NetworkCall;
import com.gsg.franchise.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONException;
import org.json.JSONObject;

public class OTP extends AppCompatActivity implements NetworkCall.MyNetworkCallBack{

    Progress progress;
    NetworkCall networkCall;
    SharedPreferences mSharedPreference;
    static final String pref_name = "GSG";
    String userid,OTP,otp_matcher,myMobile="";
    TextView resendTime,txtResend;
    PinView myOTP;

    long timerDuration = 60000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        progress = new Progress(OTP.this);
        networkCall = new NetworkCall(OTP.this, OTP.this);

        mSharedPreference = getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        userid = (mSharedPreference.getString("id", ""));
        OTP = (mSharedPreference.getString("otp", ""));
        myMobile = getIntent().getStringExtra("myMobile");
        myOTP = findViewById(R.id.otp_edit);
        resendTime = findViewById(R.id.resendTime);
        txtResend = findViewById(R.id.txtResend);
//        sendOTP();
        startCountDown();
        txtResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // sendOTP();
                startCountDown();
            }
        });
    }

//    private void sendOTP() {
//        networkCall.NetworkAPICall(ApiURL.send_otp, true);
//
//    }
    void startCountDown() {
        new CountDownTimer(timerDuration, 1000) {
            @SuppressLint("SetTextI18n")
            @Override
            public void onTick(long l) {
                long sec = (l / 1000);
                if (sec >= 10) {
                    resendTime.setText("00:" + sec);
                } else {
                    resendTime.setText("00:0" + sec);
                }
            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onFinish() {
                resendTime.setText("00:00");
            }
        }.start();

        new Handler().postDelayed(() -> {
        }, timerDuration);
    }

    public void back(View view) {
        super.onBackPressed();
    }

    public void newPass(View view) {
        otp_matcher = myOTP.getText().toString().trim();
        if (otp_matcher.equals(OTP)){
            myOTP.setLineColor(Color.BLACK);
            Toast.makeText(this, "OTP verified", Toast.LENGTH_SHORT).show();
            Intent matched = new Intent(OTP.this,NewPassword.class);
            startActivity(matched);
            OTP.this.finish();
        }else{
            myOTP.setLineColor(Color.RED);
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.send_otp:
                ion = (Builders.Any.B) Ion.with(OTP.this)
                        .load("POST", ApiURL.send_otp)
                        .setHeader("token", ApiURL.token)
                        .setBodyParameter("id", userid)
                        .setBodyParameter("mobile", myMobile);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.send_otp:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");

                    if (succes.equals("true")) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        OTP = jsonObject1.getString("otp");
                        Toast.makeText(this, ""+OTP, Toast.LENGTH_SHORT).show();
                    } else {

                        String status_fail = jsonObject.getString("success");
                        if (status_fail.equals("false")) {
                            Toast.makeText(OTP.this, msg.toString(), Toast.LENGTH_SHORT).show();
                            progress.dismiss();
                        }

                    }
                } catch (JSONException e1) {


                    Toast.makeText(OTP.this, jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();
                }


                break;
        }

    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }

    public void newPassa(View view) {
        otp_matcher = myOTP.getText().toString().trim();
        if (otp_matcher.equals(OTP)){
           // myOTP.setLineColor(Color.BLACK);
            Toast.makeText(this, "OTP verified", Toast.LENGTH_SHORT).show();
            Intent matched = new Intent(OTP.this,NewPassword.class);
            startActivity(matched);
            OTP.this.finish();
        }else{
            myOTP.setLineColor(Color.RED);
            Toast.makeText(this, "OTP is not correct", Toast.LENGTH_SHORT).show();
        }
    }
}