package com.gsg.franchise.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.loader.content.CursorLoader;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gsg.franchise.fragments.Franchise;
import com.gsg.franchise.utill.ApiURL;
import com.gsg.franchise.utill.NetworkCall;
import com.gsg.franchise.utill.Progress;
import com.gsg.franchise.R;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class CreditScore extends AppCompatActivity implements NetworkCall.MyNetworkCallBack {

    private static final int IMAGE_PICKER_SELECT = 1;
    File File_data1;
    private static final int PICK_FROM_GALLERY = 1;
    String filePath1 = "";
    ImageView  imgCredsmall, imgcred;
    RelativeLayout r1, Submit;
    final int RESULT_LOAD_IMG = 1000;
    int clickImage;
    SharedPreferences mSharedPreference;
    Progress progress;
    static final String pref_name = "GSG";
    NetworkCall networkCall;
    Uri contentURI;
    String userid, creditScore;
    TextView txtCred;
    EditText CreditScore;
    Boolean cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_score);


        progress = new Progress(CreditScore.this);
        networkCall = new NetworkCall(CreditScore.this, CreditScore.this);

        mSharedPreference = getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        userid = (mSharedPreference.getString("id", ""));


        imgcred = findViewById(R.id.imgcred);

        txtCred = findViewById(R.id.txtCred);

        imgCredsmall = findViewById(R.id.imgCredsmall);

        CreditScore = findViewById(R.id.creditEDT);

        r1 = findViewById(R.id.relatedCredit);
        r1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(CreditScore.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(CreditScore.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                } else {
                    clickImage = 1;
                    loadImagefromGallery();
                }
            }
        });

        Submit = findViewById(R.id.submitCred);
        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                creditScore = CreditScore.getText().toString().trim();
                CreditScore.setError(null);

                if (TextUtils.isEmpty(creditScore)) {
                    CreditScore.setError("This field is Required");
                    cancel = true;
                } else {

                    submit();


                }

            }
        });


    }

    private void submit() {

        networkCall.NetworkAPICall(ApiURL.creditScore, true);

    }

    private void loadImagefromGallery() {

        if (clickImage == 1) {

            Intent galleryIntent1 = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent1, RESULT_LOAD_IMG);


        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (clickImage) {
            case 1:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            imgcred.setImageURI(selectedImage);
                            imgCredsmall.setVisibility(View.INVISIBLE);
                            txtCred.setVisibility(View.INVISIBLE);
                            filePath1 = getRealPathFromURI(selectedImage);
                            File_data1 = new File(filePath1);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private String getRealPathFromURI(Uri uri) {

        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(getApplicationContext(), uri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }




    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.creditScore:
                ion = (Builders.Any.B) Ion.with(CreditScore.this)
                        .load("POST", ApiURL.creditScore)
                        .setHeader("token", ApiURL.token)
                        .setMultipartParameter("user_id", userid)
                        .setMultipartParameter("creditscore", creditScore)
                        .setMultipartFile("screenshot", new File(String.valueOf(filePath1)));
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.creditScore:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");

                    if (succes.equals("true")) {

                        getSupportFragmentManager().beginTransaction()
                                .add(android.R.id.content, new Franchise()).commit();

                    } else {

                        String status_fail = jsonObject.getString("success");
                        if (status_fail.equals("false")) {
                            Toast.makeText(CreditScore.this, msg.toString(), Toast.LENGTH_SHORT).show();
                            progress.dismiss();
                        }

                    }
                } catch (JSONException e1) {


                    Toast.makeText(CreditScore.this, jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();
                }


                break;
        }

    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }

    public void back(View view) {

        getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, new Franchise()).commit();

    }
}