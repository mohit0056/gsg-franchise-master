package com.gsg.franchise.activities;

import static com.av.smoothviewpager.utils.Smoolider_Utils.stop_autoplay_ViewPager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Toast;

import com.av.smoothviewpager.Smoolider.SmoothViewpager;
import com.google.gson.Gson;
import com.gsg.franchise.MoreDetails;
import com.gsg.franchise.R;
import com.gsg.franchise.adapter.DynamicFranchiseAdapter;
import com.gsg.franchise.models.DynmicFranchiseModel;
import com.gsg.franchise.utill.ApiURL;
import com.gsg.franchise.utill.NetworkCall;
import com.gsg.franchise.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class DynamicFranchise extends AppCompatActivity implements NetworkCall.MyNetworkCallBack, DynamicFranchiseAdapter.pageonClick {
    ViewPager viewPager;
    Progress progress;
    NetworkCall networkCall;
    Timer timer;
    SmoothViewpager viewPagerBanners;
    ArrayList<DynmicFranchiseModel> arrDynmicFranchiseModel = new ArrayList<>();
    DynamicFranchiseAdapter dynamicFranchiseAdapter;
    DynmicFranchiseModel dynmicFranchiseModel;
    String prevStarted = "yes";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_franchise);

        progress = new Progress(DynamicFranchise.this);
        networkCall = new NetworkCall(DynamicFranchise.this, DynamicFranchise.this);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (!prefs.getBoolean("firstTime", false)) {
            // <---- run your one time code here
            timer = new Timer();
            timer.scheduleAtFixedRate(new MyTimerTask(), 1000, 500);
            // mark first time has ran.
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("firstTime", true);
            editor.commit();
        }


        getData();

    }

    public class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            DynamicFranchise.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (viewPagerBanners.getCurrentItem() == 0) {
                        viewPagerBanners.setCurrentItem(1);
                    } else if (viewPagerBanners.getCurrentItem() == 1) {
                        viewPagerBanners.setCurrentItem(0);
                        timer.cancel();

                    }

                }
            });
        }
    }

    private void getData() {

        networkCall.NetworkAPICall(ApiURL.Dynamic_Franchise, true);

    }

    public void back(View view) {
        super.onBackPressed();
    }

    public void newmore(View view) {
        Intent intent = new Intent(DynamicFranchise.this, MoreDetails.class);
        startActivity(intent);
    }


    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.Dynamic_Franchise:
                ion = (Builders.Any.B) Ion.with(DynamicFranchise.this)
                        .load("POST", ApiURL.Dynamic_Franchise)
                        .setHeader("token", ApiURL.token)
                        .setBodyParameter("type", "dynamic")

                ;
                break;


        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.Dynamic_Franchise:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String success = jsonObject.getString("success");
                    if (success.equals("true")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            dynmicFranchiseModel = new Gson().fromJson(jsonArray.optJSONObject(i).toString(), DynmicFranchiseModel.class);
                            arrDynmicFranchiseModel.add(dynmicFranchiseModel);
                        }

                        viewPagerBanners = (SmoothViewpager) findViewById(R.id.viewpager);
                        // viewPagerBanners(viewPager,feedItemList.size());

                        viewPagerBanners.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                            @Override
                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                                for (int i = 0; i < arrDynmicFranchiseModel.size(); i++) {

                                }

                            }

                            @Override
                            public void onPageSelected(int position) {

                            }

                            @Override
                            public void onPageScrollStateChanged(int state) {

                            }
                        });

                        DynamicFranchiseAdapter dynamicFranchiseAdapter = new DynamicFranchiseAdapter(this, arrDynmicFranchiseModel, this, "offer");
                        viewPagerBanners.setAdapter(dynamicFranchiseAdapter);
                        dynamicFranchiseAdapter.notifyDataSetChanged();

                    } else {

                        progress.dismiss();
                    }
                } catch (JSONException e1) {
                    Toast.makeText(DynamicFranchise.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;


        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }

    @Override
    public void viewPageImageClick(String page, int position) {

    }
}