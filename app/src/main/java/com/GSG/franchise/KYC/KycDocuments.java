package com.gsg.franchise.KYC;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.loader.content.CursorLoader;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;

import android.view.View;

import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gsg.franchise.R;

import com.gsg.franchise.utill.ApiURL;
import com.gsg.franchise.utill.NetworkCall;
import com.gsg.franchise.utill.Progress;
import com.gocashfree.cashfreesdk.CFPaymentService;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_APP_ID;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_EMAIL;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_NAME;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_PHONE;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_AMOUNT;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_ID;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_NOTE;


public class KycDocuments extends AppCompatActivity implements NetworkCall.MyNetworkCallBack {

    File File_data1, File_data2, File_data3, File_data4, File_data5;
    private static final int PICK_FROM_GALLERY = 1;
    String user_id="",filePath1 = "", filePath2 = "", filePath3 = "",filePath4 = "",filePath5 = "";
    ImageView img1, img2, img3, img4, img5, imgsmall1, imgsmall2, imgsmall3, imgsmall4, imgsmall5;
    RelativeLayout r1, r2, r3, r4, r5, submit;
    final int RESULT_LOAD_IMG = 1000;
    int clickImage;
    SharedPreferences mSharedPreference;
    Progress progress;
    static final String pref_name = "GSG";
    NetworkCall networkCall;
    Uri contentURI;
    String txn_id;
    String userid, email, phone,orderid="1",amount="1",total_cost="",myfranchise_cost="",myfranchise_fees="",package_id="";

    TextView txt1, txt2, txt3, txt4, txt5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kyc_documents);
        progress = new Progress(KycDocuments.this);
        networkCall = new NetworkCall(KycDocuments.this, KycDocuments.this);
        progress = new Progress(KycDocuments.this);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        user_id = (mSharedPreference.getString("id", ""));

        total_cost = getIntent().getStringExtra("total_cost");
        package_id = getIntent().getStringExtra("package_id");
        myfranchise_cost = getIntent().getStringExtra("myfranchise_cost");
        myfranchise_fees = getIntent().getStringExtra("myfranchise_fees");

        img1 = findViewById(R.id.imgpan);
        img2 = findViewById(R.id.imgpan1);
        img3 = findViewById(R.id.imgpan2);
        img4 = findViewById(R.id.imgpan3);
        img5 = findViewById(R.id.imgpan4);


        imgsmall1 = findViewById(R.id.imgPANSmall);
        imgsmall2 = findViewById(R.id.imgPANSmall1);
        imgsmall3 = findViewById(R.id.imgPANSmall2);
        imgsmall4 = findViewById(R.id.imgPANSmall3);
        imgsmall5 = findViewById(R.id.imgPANSmall4);

        txt1 = findViewById(R.id.txtpan);
        txt2 = findViewById(R.id.txtpan1);
        txt3 = findViewById(R.id.txtpan2);
        txt4 = findViewById(R.id.txtpan3);
        txt5 = findViewById(R.id.txtpan4);



        r1 = findViewById(R.id.r1);
        r1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(KycDocuments.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(KycDocuments.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                } else {
                    clickImage = 1;
                    loadImagefromGallery();
                }
            }
        });
        r2 = findViewById(R.id.r2);
        r2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(KycDocuments.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(KycDocuments.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                } else {
                    clickImage = 2;
                    loadImagefromGallery();
                }
            }
        });
        r3 = findViewById(R.id.r3);
        r3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(KycDocuments.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(KycDocuments.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                } else {
                    clickImage = 3;
                    loadImagefromGallery();
                }
            }
        });
        r4 = findViewById(R.id.r4);
        r4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(KycDocuments.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(KycDocuments.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                } else {
                    clickImage = 4;
                    loadImagefromGallery();
                }
            }
        });
        r5 = findViewById(R.id.r5);
        r5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(KycDocuments.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(KycDocuments.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                } else {
                    clickImage = 5;
                    loadImagefromGallery();
                }
            }
        });
        submit = findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitData();
//                cashfree_genrate_token(orderid,"70");


            }
        });


    }


    private void loadImagefromGallery() {

        if (clickImage == 1) {

            Intent galleryIntent1 = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent1, RESULT_LOAD_IMG);


        } else if (clickImage == 2) {

            Intent galleryIntent2 = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent2, RESULT_LOAD_IMG);

        } else if (clickImage == 3) {

            Intent galleryIntent3 = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent3, RESULT_LOAD_IMG);

        } else if (clickImage == 4) {

            Intent galleryIntent4 = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent4, RESULT_LOAD_IMG);

        } else if (clickImage == 5) {

            Intent galleryIntent5 = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent5, RESULT_LOAD_IMG);

        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (clickImage) {
            case 1:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            img1.setImageURI(selectedImage);
                            imgsmall1.setVisibility(View.INVISIBLE);
                            txt1.setVisibility(View.INVISIBLE);
                            filePath1 = getRealPathFromURI(selectedImage);
                            File_data1 = new File(filePath1);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;


            case 2:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            img2.setImageURI(selectedImage);
                            imgsmall2.setVisibility(View.INVISIBLE);
                            txt2.setVisibility(View.INVISIBLE);
                            filePath2 = getRealPathFromURI(selectedImage);
                            File_data2 = new File(filePath2);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;

            case 3:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            img3.setImageURI(selectedImage);
                            imgsmall3.setVisibility(View.INVISIBLE);
                            txt3.setVisibility(View.INVISIBLE);
                            filePath3 = getRealPathFromURI(selectedImage);
                            File_data3 = new File(filePath3);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;

            case 4:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            img4.setImageURI(selectedImage);
                            imgsmall4.setVisibility(View.INVISIBLE);
                            txt4.setVisibility(View.INVISIBLE);
                            filePath4 = getRealPathFromURI(selectedImage);
                            File_data4 = new File(filePath4);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;

            case 5:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            img5.setImageURI(selectedImage);
                            imgsmall5.setVisibility(View.INVISIBLE);
                            txt5.setVisibility(View.INVISIBLE);
                            filePath5 = getRealPathFromURI(selectedImage);
                            File_data5 = new File(filePath5);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
            case 6:
                Log.d("", "API Response : ");
                //Prints all extras. Replace with app logic.
                if (data != null) {
                    Bundle bundle = data.getExtras();

                    if (bundle != null)
                        Log.e("suceess_data", bundle.toString());

                    for (String key : bundle.keySet()) {
                        if (bundle.getString(key) != null) {
                            Log.e("TAG", key + " : " + bundle.getString(key));
                        }
                    }

                    try {

                        if (bundle.getString("txStatus").equalsIgnoreCase("Success") || bundle.getString("txMsg").equalsIgnoreCase("Transaction Successful")) {
//                  /
//                        onBackPressed();
//                    }
                            Toast.makeText(this, bundle.getString("txMsg"), Toast.LENGTH_SHORT).show();
                            txn_id=bundle.getString("referenceId");
                            transaction_();



//                                        add_cash_amount(bundle.getString("orderId"), bundle.getString("orderAmount"));
                        } else {
//                    Utility.showToastMessage(AddCashActivity.this, bundle.getString("txMsg"), "2");
                        }
//                Utility.showToastMessage(AddCashActivity.this, bundle.getString("txMsg"), "2");
                    } catch (Exception e) {
//                Utility.showToastMessage(/AddCashActivity.this, "payment cancelled ", "2");
                    }
                }

        }

        super.onActivityResult(requestCode, resultCode, data);

    }

    private String getRealPathFromURI(Uri uri) {

        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(getApplicationContext(), uri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }


    public void back(View view) {
        super.onBackPressed();
    }

    private void submitData() {
        networkCall.NetworkAPICall(ApiURL.Kyc_Form_document, true);

    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.Kyc_Form_document:
                ion = (Builders.Any.B) Ion.with(KycDocuments.this)
                        .load("POST", ApiURL.Kyc_Form_document)
                        .setHeader("token", ApiURL.token)
                        .setMultipartParameter("user_id", userid)
                        .setMultipartFile("pancard", new File(String.valueOf(filePath1)))
                        .setMultipartFile("biodata", new File(String.valueOf(filePath2)))
                        .setMultipartFile("Itreturn", new File(String.valueOf(filePath3)))
                        .setMultipartFile("gst", new File(String.valueOf(filePath4)))
                        .setMultipartFile("Bankstatement", new File(String.valueOf(filePath5)));
                break;

        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.Kyc_Form_document:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");

                    if (succes.equals("true")) {

                        Toast.makeText(KycDocuments.this, msg, Toast.LENGTH_SHORT).show();



//                        Intent intent = new Intent(KycDocuments.this, Cash_activity.class);
//                        intent.putExtra("package_id", package_id);
//                        intent.putExtra("total_cost", total_cost);
//                        intent.putExtra("myfranchise_cost", myfranchise_cost);
//                        intent.putExtra("myfranchise_fees", myfranchise_fees);
//                        startActivity(intent);
//                        finish();



                    } else {

                        String status_fail = jsonObject.getString("success");
                        if (status_fail.equals("false")) {
                            Toast.makeText(KycDocuments.this, msg.toString(), Toast.LENGTH_SHORT).show();
                            progress.dismiss();
                        }

                    }
                } catch (JSONException e1) {


                    Toast.makeText(KycDocuments.this, jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();
                }


                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }
    private void cashfree_genrate_token(final String orderid, final String amount) {
        JsonObject json = new JsonObject();
        json.addProperty("orderId", orderid);
        json.addProperty("orderAmount", amount);
        json.addProperty("orderCurrency", "INR");
//      json.addProperty("user_id", MyProfile.getProfile().getUserId());

        Ion.with(KycDocuments.this)
                .load("POST",ApiURL.cashfreeurl)
                .setHeader("x-client-id", ApiURL.CASHFREE_mid)
                .setHeader("x-client-secret", ApiURL.CASHFREE_SECRETID)
                .setJsonObjectBody(json)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {

                        try {
                            Log.d("cashfreedata", String.valueOf(result.toString()));
                            JSONObject jsonObject = new JSONObject(result.toString());
                            if (jsonObject.optString("status").equalsIgnoreCase("OK")) {
                                doPayment(jsonObject.optString("cftoken"), orderid, amount);
                            } else {
                                Toast.makeText(KycDocuments.this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        } catch (NullPointerException ex) {
                            Toast.makeText(KycDocuments.this, "SERVER ERROR", Toast.LENGTH_SHORT).show();


                            ex.printStackTrace();
                        }
//                        JSONObject jsonObject = new JSONObject(result.)
                        // do stuff with the result or error
                    }
                });
    }
    public void doPayment(String Tokken, String odr_id, String amount) {

        String token = Tokken;
//       String stage = "PROD";
////        String appId = "42503bba18ea3af85ed7e9ec730524";
        String stage = "TEST";
        String appId = ApiURL.CASHFREE_mid;
        Map<String, String> params = new HashMap<>();
        String mobile_number = "";
        String email_id = "";
        params.put(PARAM_APP_ID, appId);
        params.put(PARAM_ORDER_ID, odr_id);
        params.put(PARAM_ORDER_AMOUNT, amount);
        params.put(PARAM_ORDER_NOTE, "");
        params.put(PARAM_CUSTOMER_NAME, "testingandroid");
        params.put(PARAM_CUSTOMER_PHONE, "+910000000000");
        params.put(PARAM_CUSTOMER_EMAIL, "manojchahal93@gmail.com");

        CFPaymentService cfPaymentService = CFPaymentService.getCFPaymentServiceInstance();
        cfPaymentService.setOrientation(0);

        cfPaymentService.doPayment(this, params, token, stage, "#8f3295", "#FFFFFFFF", true);
        clickImage=6;

    }

    private void transaction_() {
        progress.show();
        Ion.with(this)
                .load("POST", ApiURL.transaction)
                .setHeader("token", ApiURL.token)
                .setBodyParameter("user_id",user_id)
                .setBodyParameter("package_id",package_id)
                .setBodyParameter("fee",myfranchise_fees)
                .setBodyParameter("cost",myfranchise_cost)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            String success = jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("true")) {
                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
//                                creditedamount=jsonObject1.getString("amount");
//                                totalamount=jsonObject1.getString("total_amount");
//
//                                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//                                SharedPreferences.Editor editor = prefs.edit();
//                                editor.putString("creditedamount", creditedamount);
//                                editor.putString("totalamount", totalamount);
//                                editor.apply();

                                Intent intent=new Intent(KycDocuments.this, KYCnormalDetails.class);
                                startActivity(intent);
                                finish();

                            } else if (success.equals("false")) {
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                progress.dismiss();
                            }

                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }
                });

    }

}