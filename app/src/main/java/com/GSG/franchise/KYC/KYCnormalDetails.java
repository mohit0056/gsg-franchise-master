package com.gsg.franchise.KYC;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gsg.franchise.R;
import com.gsg.franchise.utill.ApiURL;
import com.gsg.franchise.utill.NetworkCall;
import com.gsg.franchise.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONException;
import org.json.JSONObject;

public class KYCnormalDetails extends AppCompatActivity implements NetworkCall.MyNetworkCallBack {

    EditText adharNum, panNum, enterprize, permanentAddress, qualifications, experienceTime;
    TextView name,gender,phone,email,dob,currentAddress,occupation;
    RelativeLayout submitForm,dob_relative;
    Boolean isLogin;
    Boolean cancel;
    static final String pref_name = "GSG";
    Progress progress;
    NetworkCall networkCall;
    Spinner sp1;
    SharedPreferences mSharedPreference;
    String total_cost="",myfranchise_cost="",myfranchise_fees="",package_id="",myname, myphone, myemail, myaddress, mygender, user_id, myoccupation, mydob, myadharNum, mypanNum, myenterprize, mypermanentAddress, myqualifications,mychooseType, myexperienceTime;
    String[] product;
    String s1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kycnormal_details);

        mSharedPreference = getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        user_id = (mSharedPreference.getString("id", ""));
        myname =(mSharedPreference.getString("name", ""));
        myemail =(mSharedPreference.getString("email", ""));
        myphone =(mSharedPreference.getString("mobile", ""));
        myaddress=(mSharedPreference.getString("address", ""));
        mydob = (mSharedPreference.getString("dob", ""));
        myoccupation=(mSharedPreference.getString("occupation", ""));
        mygender=(mSharedPreference.getString("gender", ""));

        total_cost = getIntent().getStringExtra("total_cost");
        package_id = getIntent().getStringExtra("package_id");
        myfranchise_cost = getIntent().getStringExtra("myfranchise_cost");
        myfranchise_fees = getIntent().getStringExtra("myfranchise_fees");
//        mypanNum=(mSharedPreference.getString("id", ""));
//        myadharNum=(mSharedPreference.getString("id", ""));


        progress = new Progress(KYCnormalDetails.this);
        networkCall = new NetworkCall(KYCnormalDetails.this, KYCnormalDetails.this);
        sp1 = (Spinner) findViewById(R.id.gender_spinner);

        currentAddress =  findViewById(R.id.currentAddress);
        email =  findViewById(R.id.email);
        phone =  findViewById(R.id.phone);
        name = findViewById(R.id.name);
        gender =  findViewById(R.id.gender);
        occupation = findViewById(R.id.occupation);
        dob = findViewById(R.id.dob);
        dob_relative = findViewById(R.id.dob_relative);





        submitForm = findViewById(R.id.submitForm);
        panNum = findViewById(R.id.pancardEDT);
        adharNum = findViewById(R.id.adhaarEDT);
        enterprize = findViewById(R.id.enterpriseNameEDT);
        experienceTime = findViewById(R.id.experienceEDT);
        qualifications = findViewById(R.id.qualificationsEDT);

        permanentAddress = findViewById(R.id.permanentAddressEDT);
        sp1 = (Spinner) findViewById(R.id.chooseType);
        name.setText(myname);
        email.setText(myemail);
        phone.setText(myphone);
        gender.setText(mygender);
        occupation.setText(myoccupation);
        currentAddress.setText(myaddress);
        dob.setText(mydob);

        product = getResources().getStringArray(R.array.enterprize);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, product);
        sp1.setAdapter(adapter);

        sp1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mychooseType = String.valueOf(product[i]);
                if (i == 4) {


                } else {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        submitForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                myadharNum = adharNum.getText().toString().trim();
                mypanNum = panNum.getText().toString().trim();
                myenterprize=enterprize.getText().toString().trim();
                myexperienceTime=experienceTime.getText().toString().trim();
                myqualifications=qualifications.getText().toString().trim();
                mypermanentAddress=permanentAddress.getText().toString().trim();



                adharNum.setError(null);
                panNum.setError(null);
                enterprize.setError(null);
                experienceTime.setError(null);
                qualifications.setError(null);
                permanentAddress.setError(null);

                if (TextUtils.isEmpty(myenterprize)) {
                    enterprize.setError("Enterprise name is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(myexperienceTime)) {
                    experienceTime.setError("Experience Time is required");
                    cancel = true;
                }  else if (TextUtils.isEmpty(myqualifications)) {
                    qualifications.setError("Qualification is required");
                    cancel = true;
                }  else if (TextUtils.isEmpty(myadharNum)) {
                    adharNum.setError("Aadhaar Number is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(mypanNum)) {
                    panNum.setError("Pan Number is required");
                    cancel = true;
                }
                else if (TextUtils.isEmpty(mypermanentAddress)) {
                    panNum.setError("Permanent Address is required");
                    cancel = true;
                }
                else {

                    next();

                }
            }
        });



    }

    private void next() {

        networkCall.NetworkAPICall(ApiURL.Kyc_Form, true);

    }

    public void back(View view) {
        super.onBackPressed();
    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.Kyc_Form:
                ion = (Builders.Any.B) Ion.with(KYCnormalDetails.this)
                        .load("POST", ApiURL.Kyc_Form)
                        .setHeader("token", ApiURL.token)
                        .setBodyParameter("Username", myname)
                        .setBodyParameter("Email", myemail)
                        .setBodyParameter("user_id", user_id)
                        .setBodyParameter("mobile", myphone)
                        .setBodyParameter("address", myaddress)
                        .setBodyParameter("gender", mygender)
                        .setBodyParameter("enterprise", myenterprize)
                        .setBodyParameter("dob", mydob)
                        .setBodyParameter("yearExprience", myexperienceTime)
                        .setBodyParameter("chooseType", mychooseType)
                        .setBodyParameter("adharNumber", myadharNum)
                        .setBodyParameter("panNumber", mypanNum)
                        .setBodyParameter("permanantAddress", mypermanentAddress)
                        .setBodyParameter("qualification", myqualifications)
                        .setBodyParameter("occupation", myoccupation);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {

        switch (apitype) {

            case ApiURL.Kyc_Form:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");

                    if (succes.equals("true")) {

                        Toast.makeText(KYCnormalDetails.this, msg, Toast.LENGTH_SHORT).show();
                        Intent uploadDoc = new Intent(KYCnormalDetails.this, KycDocuments.class);
                        uploadDoc.putExtra("package_id", package_id);
                        uploadDoc.putExtra("total_cost", total_cost);
                        uploadDoc.putExtra("myfranchise_cost", myfranchise_cost);
                        uploadDoc.putExtra("myfranchise_fees", myfranchise_fees);
                        startActivity(uploadDoc);

                    } else {

                        String status_fail = jsonObject.getString("success");
                        if (status_fail.equals("false")) {
                            Toast.makeText(KYCnormalDetails.this, msg.toString(), Toast.LENGTH_SHORT).show();
                            progress.dismiss();

                        }

                    }

                } catch (JSONException e1) {

                    Toast.makeText(KYCnormalDetails.this, jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();

                }

                break;

        }

    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }
}