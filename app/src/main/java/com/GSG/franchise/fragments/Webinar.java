package com.gsg.franchise.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.gsg.franchise.R;
import com.gsg.franchise.activities.WebinarForm;
import com.gsg.franchise.activities.Webinar_attended;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Webinar#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Webinar extends Fragment {
RelativeLayout attended,not_attended;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public Webinar() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Webinar.
     */
    // TODO: Rename and change types and number of parameters
    public static Webinar newInstance(String param1, String param2) {
        Webinar fragment = new Webinar();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=  inflater.inflate(R.layout.fragment_webinar, container, false);

        attended = v.findViewById(R.id.attended);
        attended.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent attended = new Intent(getContext(), Webinar_attended.class);
                startActivity(attended);
            }
        });
        not_attended = v.findViewById(R.id.not_attended);
        not_attended.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent not_attended = new Intent(getContext(), WebinarForm.class);
                startActivity(not_attended);
            }
        });

    return v;
    }
}