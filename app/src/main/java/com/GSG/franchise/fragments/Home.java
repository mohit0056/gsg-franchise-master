package com.gsg.franchise.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gsg.franchise.R;
import com.gsg.franchise.activities.Check_List;
import com.gsg.franchise.activities.GSG_Franchise_form;
import com.gsg.franchise.activities.KYC_details_1;
import com.gsg.franchise.activities.Preferred_bank;
import com.gsg.franchise.activities.Status_regarding_statutory;
import com.gsg.franchise.activities.WebinarForm;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Home#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Home extends Fragment {

    String myname;
    TextView name;
    SharedPreferences mSharedPreference;

    RelativeLayout webinar, franchise;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public Home() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Home.
     */
    // TODO: Rename and change types and number of parameters
    public static Home newInstance(String param1, String param2) {
        Home fragment = new Home();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);
       // webinar = v.findViewById(R.id.webinar);

        mSharedPreference= PreferenceManager.getDefaultSharedPreferences(getContext());
        myname =(mSharedPreference.getString("name", ""));
        name = v.findViewById(R.id.name);
        name.setText(myname);


////        webinar.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                Dialog dialogg = new Dialog(getContext(), R.style.PauseDialog);
////                dialogg.setContentView(R.layout.webinar_confirmation_layout);
////                RelativeLayout yes = dialogg.findViewById(R.id.yes);
////                yes.setOnClickListener(new View.OnClickListener() {
////                    @Override
////                    public void onClick(View v) {
////                        Intent form = new Intent(getContext(), WebinarForm.class);
////                        startActivity(form);
////                        dialogg.dismiss();
////                    }
////                });
//
//                RelativeLayout no = dialogg.findViewById(R.id.no);
//                no.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        dialogg.dismiss();
//                    }
//                });
//
//
//                Window window = dialogg.getWindow();
//                WindowManager.LayoutParams wlp = window.getAttributes();
//                wlp.gravity = Gravity.CENTER;
//                dialogg.show();
//            }
//        });


        franchise = v.findViewById(R.id.gsgFranchise);
        franchise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//                Fragment fragment = new Franchise();
//                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                fragmentTransaction.replace(R.id.content, fragment);
//                fragmentTransaction.addToBackStack(null);
//                fragmentTransaction.commit();
//
//                Fragment someFragment = new Franchise();
//                FragmentTransaction transaction = getFragmentManager().beginTransaction();
//                transaction.replace(R.id.container, someFragment ); // give your fragment container id in first parameter
//                transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
//                transaction.commit();
//
//                getSupportFragmentManager().beginTransaction()
//                                .add(android.R.id.content, new Franchise()).commit();

                Fragment fragment = new Franchise();
                replaceFragment(fragment);

            }
        });

        return v;
    }

    public void replaceFragment(Fragment someFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.container, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }



}