package com.gsg.franchise.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.Network;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.gsg.franchise.R;
import com.gsg.franchise.activities.Login;
import com.gsg.franchise.adapter.Transaction_adapter;
import com.gsg.franchise.models.Transaction_model;
import com.gsg.franchise.utill.ApiURL;
import com.gsg.franchise.utill.NetworkCall;
import com.gsg.franchise.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Profile#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Profile extends Fragment implements NetworkCall.MyNetworkCallBack,Transaction_adapter.ReturnView{
    String myname, myphone, myemail, myaddress, mygender, user_id, myoccupation, mydob, myadharNum, mypanNum, myenterprize, mypermanentAddress, myqualifications, mychooseType, myexperienceTime;
    TextView name, gender, email, phone, logout;
    SharedPreferences mSharedPreference;
    RecyclerView recycler_transaction;
    Progress progress;
    NetworkCall networkCall;
    Transaction_model transaction_model;
    ArrayList<Transaction_model>arr_list=new ArrayList<>();
    Transaction_adapter transaction_adapter;
    String str_service="",str_type="",str_franchise_cost="",str_Process_charge="",str_id="";

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public Profile() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Profile.
     */
    // TODO: Rename and change types and number of parameters
    public static Profile newInstance(String param1, String param2) {
        Profile fragment = new Profile();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        progress = new Progress(getContext());
        networkCall = new NetworkCall(Profile.this, getContext());

        View v = inflater.inflate(R.layout.fragment_profile, container, false);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getContext());
        myname = (mSharedPreference.getString("name", ""));
        myemail = (mSharedPreference.getString("email", ""));
        myphone = (mSharedPreference.getString("mobile", ""));
        myaddress = (mSharedPreference.getString("address", ""));
        mydob = (mSharedPreference.getString("dob", ""));
        myoccupation = (mSharedPreference.getString("occupation", ""));
        mygender = (mSharedPreference.getString("gender", ""));
        user_id = (mSharedPreference.getString("id", ""));
        name = v.findViewById(R.id.name);
        gender = v.findViewById(R.id.gender);
        email = v.findViewById(R.id.email);
        phone = v.findViewById(R.id.phone);
        logout = v.findViewById(R.id.logout);
        recycler_transaction = v.findViewById(R.id.recycler_transaction);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Login.class);
                SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();
                editor.clear();
                editor.apply();
                editor.remove("login");//your key
                startActivity(intent);
                getActivity().finish();

            }
        });

        name.setText(myname);
        gender.setText(mygender);
        email.setText(myemail);
        phone.setText(myphone);

        getkyc();

        return v;
    }

    private void getkyc() {
        networkCall.NetworkAPICall(ApiURL.getkyc, true);
    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.getkyc:
                ion = (Builders.Any.B) Ion.with(getContext())
                        .load("POST", ApiURL.getkyc)
                        .setHeader("token", ApiURL.token)
                        .setBodyParameter("user_id", user_id);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.getkyc:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String success = jsonObject.getString("success");
                    String message = jsonObject.getString("message");
                    if (success.equals("true")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject json_object3 = jsonArray.getJSONObject(i);
                            transaction_model = new Gson().fromJson(json_object3.toString(), Transaction_model.class);
                            arr_list.add(transaction_model);
                        }
                        transaction_adapter = new Transaction_adapter(arr_list, getActivity(), R.layout.dynamic_franchise_history, this, 1);
                        recycler_transaction.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL, false));
                        recycler_transaction.setHasFixedSize(true);
                        recycler_transaction.setAdapter(transaction_adapter);
                    } else {
                        progress.dismiss();
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    progress.dismiss();
                    Toast.makeText(getContext(), e1.toString(), Toast.LENGTH_SHORT).show();

                }
                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        Toast.makeText(getContext(), "Data not available at this time...", Toast.LENGTH_SHORT).show();
        Log.d("response",jsonstring);
    }

    @Override
    public void getAdapterView(View view, List objects, int position, int from) {
        str_service=arr_list.get(position).getServicetype();
        str_type=arr_list.get(position).getType();
        str_franchise_cost=arr_list.get(position).getFranchiseCost();
        str_Process_charge=arr_list.get(position).getProcessCharges();
        str_id=arr_list.get(position).getId();

        TextView service=view.findViewById(R.id.service);
//        TextView total_amount=view.findViewById(R.id.total_amount);
        TextView processing_charge=view.findViewById(R.id.processing_charge);
        TextView franchise_cost=view.findViewById(R.id.franchise_cost);
        TextView id_txt=view.findViewById(R.id.id_txt);
        LinearLayout background_liner=view.findViewById(R.id.background_liner);

        service.setText(str_service);
        franchise_cost.setText(str_franchise_cost);
        processing_charge.setText(str_Process_charge);
        id_txt.setText(str_id);

         if (str_type.equals("static")){
             background_liner.setBackgroundResource(R.drawable.trans_static);
         }else {
             background_liner.setBackgroundResource(R.drawable.trans_dynamic);

         }

    }
}