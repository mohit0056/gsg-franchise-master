package com.gsg.franchise.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gsg.franchise.activities.DistributionshipFranchise;
import com.gsg.franchise.R;
import com.gsg.franchise.activities.DynamicFranchise;
import com.gsg.franchise.activities.Static_Franchise;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Franchise#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Franchise extends Fragment {
    LinearLayout dynamicfranchise, staticfranchise,distrifranchise;
    String myname;
    TextView name;
    SharedPreferences mSharedPreference;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public Franchise() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Franchise.
     */
    // TODO: Rename and change types and number of parameters
    public static Franchise newInstance(String param1, String param2) {
        Franchise fragment = new Franchise();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_franchise, container, false);

        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getContext());


        myname = (mSharedPreference.getString("name", ""));


        name = v.findViewById(R.id.name);


        dynamicfranchise = v.findViewById(R.id.dynamic);
        dynamicfranchise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dynamic = new Intent(getContext(), DynamicFranchise.class);
                startActivity(dynamic);
            }
        });

        staticfranchise = v.findViewById(R.id.staticf);
        staticfranchise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dynamic = new Intent(getContext(), Static_Franchise.class);
                startActivity(dynamic);
            }
        });

        distrifranchise = v.findViewById(R.id.distrif);
        distrifranchise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dynamic = new Intent(getContext(), DistributionshipFranchise.class);
                startActivity(dynamic);
            }
        });

        name.setText(myname);

        return v;
    }
}