package com.gsg.franchise.DynamicDetails;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.gsg.franchise.R;
import com.gsg.franchise.activities.Cash_activity;

import java.util.Objects;

public class DynamicDetailsTwo extends AppCompatActivity {
    String total_cost="",myfranchise_cost="",myfranchise_fees="",package_id="";
    TextView img_2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_details_two);
        total_cost = getIntent().getStringExtra("total_cost");
        package_id = getIntent().getStringExtra("package_id");
        myfranchise_cost = getIntent().getStringExtra("myfranchise_cost");
        myfranchise_fees = getIntent().getStringExtra("myfranchise_fees");
        img_2=findViewById(R.id.img_2);
        img_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog error_dialog = new Dialog(DynamicDetailsTwo.this);
                error_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                error_dialog.setCanceledOnTouchOutside(true);
                error_dialog.setCancelable(true);
                Objects.requireNonNull(error_dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                error_dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                error_dialog.setContentView(R.layout.dynamic_img_two);

                error_dialog.show();
            }
        });
    }

    public void back(View view) {
        super.onBackPressed();
    }

    public void kyc(View view) {
        final Dialog error_dialog = new Dialog(this);
        error_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        error_dialog.setCanceledOnTouchOutside(true);
        error_dialog.setCancelable(true);
        Objects.requireNonNull(error_dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        error_dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        error_dialog.setContentView(R.layout.payment_dailog);
        Button ok_dialog;
        ok_dialog = error_dialog.findViewById(R.id.ok_dialog);
        ok_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent uploadDoc = new Intent(DynamicDetailsTwo.this, Cash_activity.class);
                uploadDoc.putExtra("package_id", package_id);
                uploadDoc.putExtra("total_cost", total_cost);
                uploadDoc.putExtra("myfranchise_cost", myfranchise_cost);
                uploadDoc.putExtra("myfranchise_fees", myfranchise_fees);
                startActivity(uploadDoc);
                error_dialog.dismiss();
            }
        });
        error_dialog.show();

    }
}