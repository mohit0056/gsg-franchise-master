package com.gsg.franchise.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gsg.franchise.R;

import java.util.List;

public class Transaction_adapter extends RecyclerView.Adapter<Transaction_adapter.Holder>{
     List list;
     Context context;
     int layout;
   ReturnView returnView;
    int from;

    public interface ReturnView {
        void getAdapterView(View view, List objects, int position, int from);
    }
    public Transaction_adapter(List list1, Context context, int layout, ReturnView returnView, int from) {
        this.list = list1;
        this.context = context;
        this.layout = layout;
        this.returnView = returnView;
        this.from = from;
    }
    @NonNull
    @Override
    public Transaction_adapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dynamic_franchise_history,parent,false);
        return new Transaction_adapter.Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Transaction_adapter.Holder holder, int position) {
        returnView.getAdapterView(holder.itemView, list, position, from);


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        public Holder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
