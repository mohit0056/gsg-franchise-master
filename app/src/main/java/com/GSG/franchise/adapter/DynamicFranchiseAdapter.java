package com.gsg.franchise.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.gsg.franchise.DynamicDetails.DynamicDetailsFive;
import com.gsg.franchise.DynamicDetails.DynamicDetailsFour;
import com.gsg.franchise.DynamicDetails.DynamicDetailsOne;
import com.gsg.franchise.DynamicDetails.DynamicDetailsSeven;
import com.gsg.franchise.DynamicDetails.DynamicDetailsSix;
import com.gsg.franchise.DynamicDetails.DynamicDetailsTwo;
import com.gsg.franchise.R;
import com.gsg.franchise.models.DynmicFranchiseModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class DynamicFranchiseAdapter extends PagerAdapter {
    private final Activity activity;
    private List<DynmicFranchiseModel> arrayList = new ArrayList<>();
    private pageonClick pageonClick;
    private String pageType;
    int cost;
    String myfranchise_cost="",myserviceType="",myprocessing_charge="",myfranchise_fees="",mysubsidy="",mytimeline="",name="",total_cost="",package_id="";

    public interface pageonClick {
        void viewPageImageClick(String page, int position);
    }

    public DynamicFranchiseAdapter(Activity activity, List<DynmicFranchiseModel> arrayList, pageonClick pageonClick, String pageType) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.pageType = pageType;
        this.pageonClick = pageonClick;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    private View view = null;

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        LayoutInflater inflater = (LayoutInflater) activity
                .getSystemService(activity.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.dynamic_franchise_model, container, false);


        TextView franchise_cost = view.findViewById(R.id.franchise_cost);
        TextView processing_charge = view.findViewById(R.id.processing_charge);
        TextView franchise_fees = view.findViewById(R.id.franchise_fees);
        TextView margin_money = view.findViewById(R.id.margin_money);
        TextView serviceType = view.findViewById(R.id.serviceType);
        TextView timeline = view.findViewById(R.id.timeline);


        RelativeLayout submitForm = view.findViewById(R.id.submitForm);
        ImageView imageView = view.findViewById(R.id.imageView);


        DynmicFranchiseModel banners = arrayList.get(position);

         myfranchise_cost = banners.getFranchiseCost();
         myprocessing_charge = banners.getProcessCharges();
         myfranchise_fees = banners.getFranchiseFee();
         mysubsidy = banners.getMarginMoney();
         myserviceType = banners.getServicetype();
         mytimeline = banners.getSubsidy();
        final String pics = banners.getImage();


        Picasso.with(activity.getApplicationContext()).load(pics).fit()
                .placeholder(R.drawable.ic_launcher_background).
                into(imageView);


        ((ViewPager) container).addView(view);


        franchise_cost.setText(myfranchise_cost);
        processing_charge.setText(myprocessing_charge);
        franchise_fees.setText(myfranchise_fees);
        margin_money.setText(mysubsidy);
        timeline.setText(mytimeline);
        serviceType.setText(myserviceType);

        submitForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DynmicFranchiseModel banners = arrayList.get(position);
                if (position == 0) {

                    package_id = banners.getId();
                    myfranchise_cost = banners.getFranchiseCost();
                    myfranchise_fees = banners.getFranchiseFee();
                    String franchise_cost = myfranchise_cost.replaceAll("[^0-9]", "");
                    String franchise_fee = myfranchise_fees.replaceAll("[^0-9]", "");

                    cost = Integer.parseInt(franchise_cost) + Integer.parseInt(franchise_fee);
                    total_cost = String.valueOf(cost);
                    Intent intent = new Intent(activity, DynamicDetailsOne.class);
                    intent.putExtra("total_cost", total_cost);
                    intent.putExtra("package_id", package_id);
                    intent.putExtra("myfranchise_cost", myfranchise_cost);
                    intent.putExtra("myfranchise_fees", myfranchise_fees);
                    activity.startActivity(intent); // p

                } else if (position == 1) {
                    package_id = banners.getId();
                    myfranchise_cost = banners.getFranchiseCost();
                    myfranchise_fees = banners.getFranchiseFee();
                    String franchise_cost = myfranchise_cost.replaceAll("[^0-9]", "");
                    String franchise_fee = myfranchise_fees.replaceAll("[^0-9]", "");

                    cost = Integer.parseInt(franchise_cost) + Integer.parseInt(franchise_fee);
                    total_cost = String.valueOf(cost);

                    Intent intent = new Intent(activity, DynamicDetailsTwo.class);
                    intent.putExtra("total_cost", total_cost);
                    intent.putExtra("package_id", package_id);
                    intent.putExtra("myfranchise_cost", myfranchise_cost);
                    intent.putExtra("myfranchise_fees", myfranchise_fees);
                    activity.startActivity(intent); // p

                } else if (position == 2) {
                    package_id = banners.getId();
                    myfranchise_cost = banners.getFranchiseCost();
                    myfranchise_fees = banners.getFranchiseFee();
                    String franchise_cost = myfranchise_cost.replaceAll("[^0-9]", "");
                    String franchise_fee = myfranchise_fees.replaceAll("[^0-9]", "");

                    cost = Integer.parseInt(franchise_cost) + Integer.parseInt(franchise_fee);
                    total_cost = String.valueOf(cost);

                    Intent intent = new Intent(activity, DynamicDetailsFour.class);
                    intent.putExtra("total_cost", total_cost);
                    intent.putExtra("package_id", package_id);
                    intent.putExtra("myfranchise_cost", myfranchise_cost);
                    intent.putExtra("myfranchise_fees", myfranchise_fees);
                    activity.startActivity(intent); // p

                }else if (position == 3) {
                    package_id = banners.getId();
                    myfranchise_cost = banners.getFranchiseCost();
                    myfranchise_fees = banners.getFranchiseFee();
                    String franchise_cost = myfranchise_cost.replaceAll("[^0-9]", "");
                    String franchise_fee = myfranchise_fees.replaceAll("[^0-9]", "");

                    cost = Integer.parseInt(franchise_cost) + Integer.parseInt(franchise_fee);
                    total_cost = String.valueOf(cost);

                    Intent intent = new Intent(activity, DynamicDetailsFive.class);
                    intent.putExtra("total_cost", total_cost);
                    intent.putExtra("package_id", package_id);
                    intent.putExtra("myfranchise_cost", myfranchise_cost);
                    intent.putExtra("myfranchise_fees", myfranchise_fees);
                    activity.startActivity(intent); // p

                } else if (position == 4) {
                    package_id = banners.getId();
                    myfranchise_cost = banners.getFranchiseCost();
                    myfranchise_fees = banners.getFranchiseFee();
                    String franchise_cost = myfranchise_cost.replaceAll("[^0-9]", "");
                    String franchise_fee = myfranchise_fees.replaceAll("[^0-9]", "");

                    cost = Integer.parseInt(franchise_cost) + Integer.parseInt(franchise_fee);
                    total_cost = String.valueOf(cost);

                    Intent intent = new Intent(activity, DynamicDetailsSix.class);
                    intent.putExtra("total_cost", total_cost);
                    intent.putExtra("package_id", package_id);
                    intent.putExtra("myfranchise_cost", myfranchise_cost);
                    intent.putExtra("myfranchise_fees", myfranchise_fees);
                    activity.startActivity(intent); // p

                } else if (position == 5) {
                    package_id = banners.getId();
                    myfranchise_cost = banners.getFranchiseCost();
                    myfranchise_fees = banners.getFranchiseFee();
                    String franchise_cost = myfranchise_cost.replaceAll("[^0-9]", "");
                    String franchise_fee = myfranchise_fees.replaceAll("[^0-9]", "");

                    cost = Integer.parseInt(franchise_cost) + Integer.parseInt(franchise_fee);
                    total_cost = String.valueOf(cost);

                    Intent intent = new Intent(activity, DynamicDetailsSeven.class);
                    intent.putExtra("total_cost", total_cost);
                    intent.putExtra("package_id", package_id);
                    intent.putExtra("myfranchise_cost", myfranchise_cost);
                    intent.putExtra("myfranchise_fees", myfranchise_fees);
                    activity.startActivity(intent); // p

                }


            }
        });

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

}

