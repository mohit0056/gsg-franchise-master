package com.gsg.franchise.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.gsg.franchise.R;

import com.gsg.franchise.StaticDetails.StaticDetailFour;
import com.gsg.franchise.StaticDetails.StaticDetailSix;
import com.gsg.franchise.StaticDetails.StaticDetailThree;
import com.gsg.franchise.StaticDetails.StaticDetailone;
import com.gsg.franchise.StaticDetails.StaticDetailsFive;
import com.gsg.franchise.StaticDetails.StaticDetailtwo;
import com.gsg.franchise.models.StaticFranchiseModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class StaticFranchiseAdapter extends PagerAdapter {
    private final Activity activity;
    private List<StaticFranchiseModel> arrayList = new ArrayList<>();
    private pageonClick pageonClick;
    private String pageType;
    int cost;
    String myfranchise_cost="",myserviceType="",myprocessing_charge="",myfranchise_fees="",mymargin_money="",mytimeline="",name="",total_cost="",package_id="";
    public interface pageonClick {
        void viewPageImageClick(String page, int position);
    }

    public StaticFranchiseAdapter(Activity activity, ArrayList<StaticFranchiseModel> arrayList, pageonClick pageonClick, String pageType) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.pageType = pageType;
        this.pageonClick = pageonClick;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    private View view = null;

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        LayoutInflater inflater = (LayoutInflater) activity
                .getSystemService(activity.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.static_franchise_model, container, false);


        ImageView imageView = view.findViewById(R.id.image);
        TextView franchise_cost = view.findViewById(R.id.franchise_cost);
        TextView processing_charge = view.findViewById(R.id.processing_charge);
        TextView franchise_fees = view.findViewById(R.id.franchise_fees);
        TextView margin_money = view.findViewById(R.id.margin_money);
        TextView timeline = view.findViewById(R.id.timeline);
        TextView serviceType = view.findViewById(R.id.serviceType);
        TextView view_img = view.findViewById(R.id.view_img);


        StaticFranchiseModel banners = arrayList.get(position);

        myfranchise_cost = banners.getFranchiseCost();
       myprocessing_charge = banners.getProcessCharges();
       myfranchise_fees = banners.getFranchiseFee();
         mymargin_money = banners.getMarginMoney();
        myserviceType = banners.getServicetype();
         mytimeline = banners.getTimeline();
         name = banners.getImage();

        ((ViewPager) container).addView(view);
        Picasso.with(activity.getApplicationContext()).load(name).fit()
                .placeholder(R.drawable.ic_launcher_background).
                into(imageView);

        franchise_cost.setText(myfranchise_cost);
        processing_charge.setText(myprocessing_charge);
        franchise_fees.setText(myfranchise_fees);
        serviceType.setText(myserviceType);
        margin_money.setText(mymargin_money);
        timeline.setText(mytimeline);

        RelativeLayout submitForm = view.findViewById(R.id.submitForm);

        view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StaticFranchiseModel banners = arrayList.get(position);
                if (position == 0) {
                    name = banners.getImage();
                    final Dialog error_dialog = new Dialog(activity);
                    error_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    error_dialog.setCanceledOnTouchOutside(true);
                    error_dialog.setCancelable(true);
                    Objects.requireNonNull(error_dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    error_dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                    error_dialog.setContentView(R.layout.static_imge_dilog);
                     ImageView image1=error_dialog.findViewById(R.id.image1);
                     Picasso.with(activity.getApplicationContext()).load(name).fit()
                            .placeholder(R.drawable.ic_launcher_background).
                            into(image1);

                    error_dialog.show();
                } else if (position == 1) {
                    name = banners.getImage();
                    final Dialog error_dialog = new Dialog(activity);
                    error_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    error_dialog.setCanceledOnTouchOutside(true);
                    error_dialog.setCancelable(true);
                    Objects.requireNonNull(error_dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    error_dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                    error_dialog.setContentView(R.layout.static_imge_dilog);
                    ImageView image1=error_dialog.findViewById(R.id.image1);
                    Picasso.with(activity.getApplicationContext()).load(name).fit()
                            .placeholder(R.drawable.ic_launcher_background).
                            into(image1);

                    error_dialog.show();

                } else if (position == 2) {
                    name = banners.getImage();
                    final Dialog error_dialog = new Dialog(activity);
                    error_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    error_dialog.setCanceledOnTouchOutside(true);
                    error_dialog.setCancelable(true);
                    Objects.requireNonNull(error_dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    error_dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                    error_dialog.setContentView(R.layout.static_imge_dilog);
                    ImageView image1=error_dialog.findViewById(R.id.image1);
                    Picasso.with(activity.getApplicationContext()).load(name).fit()
                            .placeholder(R.drawable.ic_launcher_background).
                            into(image1);

                    error_dialog.show();
                } else if (position == 3) {
                    name = banners.getImage();
                    final Dialog error_dialog = new Dialog(activity);
                    error_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    error_dialog.setCanceledOnTouchOutside(true);
                    error_dialog.setCancelable(true);
                    Objects.requireNonNull(error_dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    error_dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                    error_dialog.setContentView(R.layout.static_imge_dilog);
                    ImageView image1=error_dialog.findViewById(R.id.image1);
                    Picasso.with(activity.getApplicationContext()).load(name).fit()
                            .placeholder(R.drawable.ic_launcher_background).
                            into(image1);

                    error_dialog.show();
                }


            }
        });



        submitForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StaticFranchiseModel banners = arrayList.get(position);
                if (position == 0) {
                     package_id = banners.getId();
                     myfranchise_cost = banners.getFranchiseCost();
                     myfranchise_fees = banners.getFranchiseFee();
                    String franchise_cost = myfranchise_cost.replaceAll("[^0-9]", "");
                    String franchise_fee = myfranchise_fees.replaceAll("[^0-9]", "");

                    cost = Integer.parseInt(franchise_cost) + Integer.parseInt(franchise_fee);
                     total_cost = String.valueOf(cost);

                    Intent intent = new Intent(activity, StaticDetailone.class);
                    intent.putExtra("total_cost", total_cost);
                    intent.putExtra("package_id", package_id);
                    intent.putExtra("myfranchise_cost", myfranchise_cost);
                    intent.putExtra("myfranchise_fees", myfranchise_fees);
                    activity.startActivity(intent); // p
                } else if (position == 1) {
                    package_id = banners.getId();
                    myfranchise_cost = banners.getFranchiseCost();
                    myfranchise_fees = banners.getFranchiseFee();
                    String franchise_cost = myfranchise_cost.replaceAll("[^0-9]", "");
                    String franchise_fee = myfranchise_fees.replaceAll("[^0-9]", "");

                    cost = Integer.parseInt(franchise_cost) + Integer.parseInt(franchise_fee);
                    total_cost = String.valueOf(cost);

                    Intent intent = new Intent(activity, StaticDetailtwo.class);
                    intent.putExtra("total_cost", total_cost);
                    intent.putExtra("package_id", package_id);
                    intent.putExtra("myfranchise_cost", myfranchise_cost);
                    intent.putExtra("myfranchise_fees", myfranchise_fees);
                    activity.startActivity(intent); // p

                } else if (position == 2) {
                    package_id = banners.getId();
                    myfranchise_cost = banners.getFranchiseCost();
                    myfranchise_fees = banners.getFranchiseFee();
                    String franchise_cost = myfranchise_cost.replaceAll("[^0-9]", "");
                    String franchise_fee = myfranchise_fees.replaceAll("[^0-9]", "");

                    cost = Integer.parseInt(franchise_cost) + Integer.parseInt(franchise_fee);
                    total_cost = String.valueOf(cost);

                    Intent intent = new Intent(activity, StaticDetailThree.class);
                    intent.putExtra("total_cost", total_cost);
                    intent.putExtra("package_id", package_id);
                    intent.putExtra("myfranchise_cost", myfranchise_cost);
                    intent.putExtra("myfranchise_fees", myfranchise_fees);
                    activity.startActivity(intent); // p
                } else if (position == 3) {
                    package_id = banners.getId();
                    myfranchise_cost = banners.getFranchiseCost();
                    myfranchise_fees = banners.getFranchiseFee();
                    String franchise_cost = myfranchise_cost.replaceAll("[^0-9]", "");
                    String franchise_fee = myfranchise_fees.replaceAll("[^0-9]", "");

                    cost = Integer.parseInt(franchise_cost) + Integer.parseInt(franchise_fee);
                    total_cost = String.valueOf(cost);

                    Intent intent = new Intent(activity, StaticDetailFour.class);
                    intent.putExtra("total_cost", total_cost);
                    intent.putExtra("package_id", package_id);
                    intent.putExtra("myfranchise_cost", myfranchise_cost);
                    intent.putExtra("myfranchise_fees", myfranchise_fees);
                    activity.startActivity(intent); // p
                }

                else if (position == 4) {
                    package_id = banners.getId();
                    myfranchise_cost = banners.getFranchiseCost();
                    myfranchise_fees = banners.getFranchiseFee();
                    String franchise_cost = myfranchise_cost.replaceAll("[^0-9]", "");
                    String franchise_fee = myfranchise_fees.replaceAll("[^0-9]", "");

                    cost = Integer.parseInt(franchise_cost) + Integer.parseInt(franchise_fee);
                    total_cost = String.valueOf(cost);

                    Intent intent = new Intent(activity, StaticDetailsFive.class);
                    intent.putExtra("total_cost", total_cost);
                    intent.putExtra("package_id", package_id);
                    intent.putExtra("myfranchise_cost", myfranchise_cost);
                    intent.putExtra("myfranchise_fees", myfranchise_fees);
                    activity.startActivity(intent); // p
                }

                else if (position == 5) {
                    package_id = banners.getId();
                    myfranchise_cost = banners.getFranchiseCost();
                    myfranchise_fees = banners.getFranchiseFee();
                    String franchise_cost = myfranchise_cost.replaceAll("[^0-9]", "");
                    String franchise_fee = myfranchise_fees.replaceAll("[^0-9]", "");

                    cost = Integer.parseInt(franchise_cost) + Integer.parseInt(franchise_fee);
                    total_cost = String.valueOf(cost);

                    Intent intent = new Intent(activity, StaticDetailSix.class);
                    intent.putExtra("total_cost", total_cost);
                    intent.putExtra("package_id", package_id);
                    intent.putExtra("myfranchise_cost", myfranchise_cost);
                    intent.putExtra("myfranchise_fees", myfranchise_fees);
                    activity.startActivity(intent); // p
                }
            }
        });


        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

}

